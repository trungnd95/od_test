#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "src/GPUBased/ServicesManager.h"
#include "src/GPUBased/services/helper/display/VideoRender.h"
#include "logging/LogApis.h"

void signalCallbackHandler(int signum)
{
    closeLogFile();
    exit(signum);
}

int main(int argc, char *argv[])
{
    signal(SIGINT, signalCallbackHandler);
    struct sigaction action;
    memset(&action, 0, sizeof(struct sigaction));
    action.sa_flags = SA_SIGINFO;
    action.sa_handler = signalCallbackHandler;
    sigaction(SIGSEGV, &action, NULL);

    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);

    QGuiApplication app(argc, argv);
    app.setOrganizationName("UAV-HKVT");
    app.setOrganizationDomain("UAV-HKVT");

    QQmlApplicationEngine engine;
    qmlRegisterSingletonType(QUrl(QLatin1String("qrc:/views/components/UIConstants.qml")),
                                                                "UIConstants", 1, 0, "UIConstants");
    qmlRegisterType<VideoRender>("io.uav.hkvt.dev", 1, 0, "VideoRender");
    qmlRegisterType<ServicesManager>("io.uav.hkvt.dev", 1, 0, "ServicesManager");

    engine.addImportPath("qrc:/");
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
