/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Module: Layout
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 13/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//----------------------Include QT Libs ------------------------
import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

//----------------------Include custom views -------------------
import "views/windows"
import "views/components"
import "views/partials"
import "./assets/javascripts/Helper.js" as Helper
import UIConstants 1.0
import io.uav.hkvt.dev 1.0


//================= Main layout application ===================
ApplicationWindow {
    id: rootItem
    property bool isVideoOn: false
    property bool odActive: true
    visible: true
    //------------------- Set Dimension
    width: 1440
    height: 1200
    //------------------- Set font family
    font.family: UIConstants.customFont

    //------------------- Set background
    color: UIConstants.sidebarConfigBg
    Overlay { color: UIConstants.bgColorOverlay }

    ServicesManager {
        id: servicesManager
        onVideoStartRunning: {
            isVideoOn = true;
            customSlider.endTime = servicesManager.getTime("TOTAL") / 1000000000;
            countingVideo.start();
        }

        onDecodeSourceIsSet: {
            fileName.text = t_fileName;
        }
    }
    //------------------- Load fonts
    FontsLoader {}

    //========= Windows Page Navigation ================
    //------- Header --------
    Header {
        id: header
        anchors { top: parent.top; left: parent.left; }
        navbarVisible: true
        Behavior on anchors.topMargin {
            NumberAnimation {
                duration: 500
                easing.type: Easing.InCubic
            }
        }
        FlatButton {
            id: odFeatureControl
            width: 100
            height: 50
            anchors { right: parent.right; rightMargin: 10;
                bottom: parent.bottom; bottomMargin: 5 }
            btnText: odActive ? "Turnoff OD" : "Turnon OD"
            btnBgColor: odActive ? UIConstants.borderGreen : "#57606f"
            btnTextColor: odActive ? UIConstants.textColor : UIConstants.cateOverlayBg
            opacity: odActive ? 1 : 0.6
            radius: 5
            onClicked: {
                odActive = !odActive;
                servicesManager.toggleOD();
            }
        }

    }

    //------- Page Content -------
    Rectangle {
        id: videopane
        anchors { top: header.bottom; left: parent.left; bottom: footer.top; right: parent.right }
        //---Content
        ColumnLayout {
            anchors.fill: parent

            //----- Video Output
            Rectangle {
                Layout.preferredWidth: parent.width
                Layout.preferredHeight: parent.height * 9 / 10
                anchors.top: parent.top
                color: UIConstants.cfProcessingOverlayBg

                Rectangle {
                    anchors.fill: parent
                    color: "black"
                    border.color: UIConstants.grayColor
                    border.width: 1
                    radius: 3
                }

                Label {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: rootItem.width > UIConstants.sRect * 8?"NO VIDEO":"NO\nVIDEO"
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    color: UIConstants.textColor
                    font.family: UIConstants.customFont
                    font.bold: true
                    font.pixelSize: UIConstants.fontSize * 2
                    visible: !rootItem.isVideoOn
                }

                VideoRender {
                    id: videoOutput
                    anchors.fill: parent
                }
            }

            //----- Control pane
            Rectangle {
                id: controlPane
                Layout.preferredWidth: parent.width
                Layout.preferredHeight: parent.height / 10
                color: UIConstants.bgColorOverlay
                focus: true
                // --- Slider
                PlaybackSlider {
                    id: customSlider
                    width: parent.width
                    height: 7
                    anchors { left: parent.left; top: parent.top; topMargin: 5 }
                    startTime: 0
                    endTime: 60
                    stepSize: 10 // in seconds
                    onClicked: {
                        isVideoOn = false;
                        countingVideo.stop();
                        servicesManager.goToPosition(percent);
                    }

                    Timer {
                        id: countingVideo
                        repeat: true
                        running: false
                        interval: 1000
                        onTriggered: {
                            customSlider.increase(1);
                        }
                    }
                }

                // Pause/Playing + Speed + Filename
                Rectangle {
                    id: controlPaneAdjust
                    anchors {top: customSlider.bottom; left: parent.left; bottom: parent.bottom}
                    width: parent.width
                    color: UIConstants.transparentColor

                    Row {
                        id: controlLeft
                        width: parent.width / 3
                        height: parent.height
                        x: 15
                        spacing: 20
                        // Pause/ Playing
                        Row {
                            height: parent.height
                            width: childrenRect.width
                            spacing: 7

                            Rectangle {
                                id: decreaseBtn
                                height: parent.height / 3
                                width: height
                                anchors.verticalCenter: parent.verticalCenter
                                border.width: 1
                                border.color: UIConstants.dropshadowColor
                                radius: 2
                                color: UIConstants.transparentColor
                                Text {
                                    text: UIConstants.iChevronLeft
                                    font.pixelSize: 12
                                    font.family: "FontAwesome"
                                    color: UIConstants.textFooterColor
                                    anchors.centerIn: parent

                                }
                                MouseArea {
                                    anchors.fill: parent
                                    visible: customSlider.endTime > 0
                                    onPressed: {
                                        parent.opacity = 0.6;
                                        isVideoOn = false;
                                        countingVideo.stop();
                                        controlPane.focus = true;
                                        customSlider.decrease(customSlider.stepSize);
                                        servicesManager.goToPosition(
                                                   (customSlider.value - customSlider.stepSize)
                                                    /
                                                    customSlider.endTime );
                                    }
                                    onReleased: parent.opacity = 1
                                }
                            }

                            Rectangle {
                                height: parent.height / 3
                                width: height
                                anchors.verticalCenter: parent.verticalCenter
                                border.width: 1
                                border.color: UIConstants.dropshadowColor
                                radius: 2
                                color: UIConstants.transparentColor
                                Text {
                                    id: pausePlaying
                                    anchors.left: parent.left
                                    text: isVideoOn ? UIConstants.iPlayState : UIConstants.iStopState
                                    font.pixelSize: 12
                                    color: UIConstants.textFooterColor
                                    anchors.centerIn: parent
                                    MouseArea {
                                        anchors.fill: parent
                                        enabled: true
                                        hoverEnabled: true
                                        onClicked: {
                                            parent.text = (parent.text == UIConstants.iPlayState)
                                                        ? UIConstants.iStopState : UIConstants.iPlayState;
                                            if (parent.text == UIConstants.iStopState)
                                            {
                                                servicesManager.pauseDecoder(true);
                                                countingVideo.stop();
                                            }
                                            else {
                                                servicesManager.pauseDecoder(false);
                                            }
                                        }
                                    }
                                }
                            }

                            Rectangle {
                                id: increaseBtn
                                height: parent.height / 3
                                width: height
                                anchors.verticalCenter: parent.verticalCenter
                                border.width: 1
                                border.color: UIConstants.dropshadowColor
                                radius: 2
                                color: UIConstants.transparentColor
                                Text {
                                    text: UIConstants.iChevronRight
                                    font.pixelSize: 12
                                    font.family: "FontAwesome"
                                    color: UIConstants.textFooterColor
                                    anchors.centerIn: parent

                                }
                                MouseArea {
                                    anchors.fill: parent
                                    visible: customSlider.endTime > 0
                                    onPressed: {
                                        parent.opacity = 0.6;
                                        isVideoOn = false;
                                        countingVideo.stop();
                                        controlPane.focus = true;
                                        customSlider.increase(customSlider.stepSize);
                                        servicesManager.goToPosition(
                                                   (customSlider.value + customSlider.stepSize)
                                                    /
                                                    customSlider.endTime);
                                    }
                                    onReleased: parent.opacity = 1
                                }
                            }
                        }

                        // Speed adjusting
                        CustomComboBox {
                            id: speedAdjusting
                            width: 50
                            height: parent.height / 3
                            anchors.verticalCenter: parent.verticalCenter
                            model_: ["0.1x", "0.5x", "1.0x", "1.5x", "2x"]
                            activeIndex: 2 // It means default value is 1.0x
                            popupBackground: UIConstants.bgColorOverlay
                            onActiveValueChanged: {
                                var value = speedAdjusting.model_[activeValueIndex];
                                value = parseFloat(value.substring(0, value.length - 1));
                                servicesManager.setSpeed(value);
                                isVideoOn = false;
                                countingVideo.stop();
                                countingVideo.interval = 1000 / value;
                            }
                        }

                    }

                    // FileName
                    Rectangle {
                        anchors.left: controlLeft.right
                        width: parent.width / 3
                        height: parent.height
                        color: UIConstants.transparentColor
                        Text {
                            id: previousFile
                            text: UIConstants.iDoubleLeft
                            font.pixelSize: 15
                            font.family: "FontAwesome"
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.right: fileName.left
                            anchors.rightMargin: 10
                            color: UIConstants.textFooterColor
                            visible: fileName.text != "N/A"
                            MouseArea {
                                anchors.fill: parent
                                onPressed: parent.opacity = 0.6
                                onReleased: parent.opacity = 1
                            }
                        }

                        Text {
                            id: fileName
                            text: "N/A"
                            color: UIConstants.textFooterColor
                            font { family: UIConstants.customFont; bold: true; pixelSize: 14 }
                            anchors.centerIn: parent
                        }

                        Text {
                            id: nextFile
                            text: UIConstants.iDoubleRight
                            font.pixelSize: 15
                            font.family: "FontAwesome"
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.left: fileName.right
                            anchors.leftMargin: 10
                            color: UIConstants.textFooterColor
                            visible: fileName.text != "N/A"
                            MouseArea {
                                anchors.fill: parent
                                onPressed: parent.opacity = 0.6
                                onReleased: parent.opacity = 1
                            }
                        }
                    }

                    Rectangle {
                        id: controlRight
                        anchors.right: parent.right
                        width: parent.width / 3
                        height: parent.height
                        color: UIConstants.transparentColor
                        Row {
                            height: parent.height
                            width: childrenRect.width
                            anchors.right: parent.right
                            anchors.rightMargin: 10
                            spacing: 10

                            Rectangle {
                                id: videoExport
                                height: parent.height / 3
                                width: height
                                anchors.verticalCenter: parent.verticalCenter
                                border.width: 1
                                border.color: UIConstants.dropshadowColor
                                radius: 2
                                color: UIConstants.transparentColor
                                Text {
                                    text: UIConstants.iFileDownload
                                    font.pixelSize: 12
                                    font.family: "FontAwesome"
                                    color: UIConstants.textFooterColor
                                    anchors.centerIn: parent

                                }
                            }

                            Rectangle {
                                id: fullScreen
                                height: parent.height / 3
                                width: height
                                anchors.verticalCenter: parent.verticalCenter
                                border.width: 1
                                border.color: UIConstants.dropshadowColor
                                radius: 2
                                color: UIConstants.transparentColor
                                Text {
                                    text: UIConstants.iOpenFullScreen
                                    font.pixelSize: 12
                                    font.family: "FontAwesome"
                                    color: UIConstants.textFooterColor
                                    anchors.centerIn: parent

                                }
                            }

                            Rectangle {
                                height: parent.height / 3
                                width: height
                                anchors.verticalCenter: parent.verticalCenter
                                border.width: 1
                                border.color: UIConstants.dropshadowColor
                                radius: 2
                                color: UIConstants.transparentColor
                                Text {
                                    id: voice
                                    anchors.left: parent.left
                                    text: UIConstants.iVoiceControl
                                    font.pixelSize: 12
                                    color: UIConstants.textFooterColor
                                    anchors.centerIn: parent
                                }
                                MouseArea {
                                    anchors.fill: parent
                                    onPressed: parent.opacity = 0.6;
                                    onReleased: parent.opacity = 1;
                                }
                            }
                        }
                    }
                }

                Keys.onPressed:  {
                    if (event.key == Qt.Key_Left) {
                        decreaseBtn.opacity = 0.6;
                        controlPane.focus = true;
                        customSlider.decrease(customSlider.stepSize);
                        isVideoOn = false;
                        countingVideo.stop();
                        servicesManager.goToPosition(
                                   (customSlider.value - customSlider.stepSize)
                                    /
                                    customSlider.endTime );
                    }

                    if (event.key == Qt.Key_Right) {
                        increaseBtn.opacity = 0.6;
                        controlPane.focus = true;
                        customSlider.increase(customSlider.stepSize);
                        isVideoOn = false;
                        countingVideo.stop();
                        servicesManager.goToPosition(
                                   (customSlider.value + customSlider.stepSize)
                                    /
                                    customSlider.endTime );
                    }
                }

                Keys.onReleased: {
                        decreaseBtn.opacity = 1;
                        increaseBtn.opacity = 1;
                }
            }
        }
        Component.onCompleted: {
            servicesManager.setDisplayWorker(videoOutput);
        }
    }

    //------- Footer --------
    Footer {
        id: footer
        anchors { bottom: parent.bottom; left: parent.left }
    }

    Component.onCompleted: {
        servicesManager.setVideo("filesrc location=/home/pgcs-05/Videos/CheyenneVAhospital.mpeg4 ! decodebin");
        servicesManager.start();
    }
}
