/**
 *==================================================================================================
 * Project: Treron-Pgcs
 * Module: Pgcs image process manager
 * Module Short Description: Manage all image process features like image decoding, object detection,
 *                           object searching, image displaying, image saving, ....
 * Author: Trung-Ng (trungnd233@viettel.com.vn)
 * Date: 11/06/2020 09:58
 * Organization: Viettel Aerospace Institude - Viettel Group
 *
 * (c) VTX 2020. All Rights reserved.
 * =================================================================================================
 */

#include "ServicesManager.h"
#include "services/helper/FrameMetaPackaged.h"
#include "helper/TimeHelper.h"

// TreronVDisplayAdapter
ServicesManager::ServicesManager(QObject* parent)
    : QObject(parent)
{
    qRegisterMetaType<FrameMetaPackaged>();

    //--- Initialiazing services
    m_frameDecoder   = new FrameDecoder();
    m_frameProcessor = new FrameProcessor();
    m_frameDisplayer = new FrameDisplayer();
    m_frameSaver     = new FrameSaver(QString::fromStdString(Time::getDateTimeStr() + ".mp4"));

    //--- Connecting events
    connect(m_frameDecoder, &FrameDecoder::newImageDecoded,
                                             this, &ServicesManager::handleNewImageDecoded);
    connect(m_frameDecoder, SIGNAL(sourceFileIsSet(QString)),
                                             this, SIGNAL(decodeSourceIsSet(QString)));
    connect(m_frameProcessor, &FrameProcessor::processed,
                                             this, &ServicesManager::handleNewFrameProcessed);

    //connect(m_frameDisplayer, &FrameDisplayer::readyDrawOnRenderID,
    //                                         this, &ServicesManager::drawOnRenderID);
}

// ~TreronVDisplayAdapter
ServicesManager::~ServicesManager()
{
    delete m_frameDecoder;
    delete m_frameProcessor;
    delete m_frameDisplayer;
    delete m_frameSaver;
}

// handleNewImageDecoded
static bool vidRan = false;
void ServicesManager::handleNewImageDecoded(FrameMetaPackaged t_decodedFrameMeta)
{
    if (m_frameDecoder->decodeUriType() == FrameDecoder::URI::FILE && !vidRan)
    {
        vidRan = true;
        Q_EMIT videoStartRunning();
    }

    m_frameProcessor->processNewFrame(t_decodedFrameMeta);
}

// handleNewFrameProcessed
void ServicesManager::handleNewFrameProcessed(FrameMetaPackaged t_processedFrame)
{
    //--- Op1: Add data to FrameDisplayer service
    m_frameDisplayer->displayFrame(t_processedFrame);

    //--- Op2: Add data to FrameSaver service
    m_frameSaver->saveNewFrame(t_processedFrame);

    //--- Op3: Add data to FrameRtspMaker service
    //m_frameRtspMaker->streamNewFrame(t_processedFrame);
}

// start
void ServicesManager::start()
{
    m_frameDecoder->start();
    m_frameProcessor->start();
    m_frameDisplayer->start();
    m_frameSaver->start();
    //m_frameRtspMaker->start();
}

// toggleOD
void ServicesManager::toggleOD()
{
    m_frameProcessor->toggleOD();
}

// setVideo
void ServicesManager::setVideo(QString t_ip, int t_port)
{
    printf("%s - %s\r\n", __func__, t_ip.toStdString().c_str());
    //    if(t_ip.contains("filesrc"))
    //    {
    //        Q_EMIT sourceLinkChanged(true);
    //    }else{
    //        Q_EMIT sourceLinkChanged(false);
    //    }
    m_frameDecoder->setSource(t_ip.toStdString(), t_port);
}

// pause
void ServicesManager::pauseDecoder(bool pause)
{
    vidRan = false;
    m_frameDecoder->pause(pause);
}

// goToPosition
void ServicesManager::goToPosition(float percent)
{
    vidRan = false;
    m_frameDecoder->goToPosition(percent);
}

// setSpeed
void ServicesManager::setSpeed(float speed)
{
    vidRan = false;
    m_frameDecoder->setSpeed(speed);
}

// getTime
qint64 ServicesManager::getTime(QString type)
{
    if(type == "TOTAL")
    {
        return m_frameDecoder->getTotalTime();
    }
    else if(type == "CURRENT")
    {
        return m_frameDecoder->getPosCurrent();
    }
    return 0;
}

// setDisplayWorker
void ServicesManager::setDisplayWorker(VideoRender* t_renderWorker)
{
    m_frameDisplayer->setDisplayWorker(t_renderWorker);
}
