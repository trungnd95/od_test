/**
 *==================================================================================================
 * Project: Treron-Pgcs
 * Module: FrameDecoder
 * Module Short Description: Process image before it is displayed on screen. Some works like
 *                           extract barcode id, object detection, ...
 *
 * Author: Trung-Ng (trungnd233@viettel.com.vn)
 * Date: 11/06/2020 09:58
 * Organization: Viettel Aerospace Institude - Viettel Group
 *
 * (c) VTX 2020. All Rights reserved.
 * =================================================================================================
 */

#include "FrameProcessor.h"
#include "helper/OdLabelHelper.h"
#include "cuda_runtime_api.h"

// Constructor
FrameProcessor::FrameProcessor(QObject* parent)
    : QThread(parent)
{
    // Init yolo detect worker.
    std::string names_file   = "../src/GPUBased/od/yolo-setup/visdrone2019.names";
    readLabelNamesToMap(names_file);
    std::string cfg_file     = "../src/GPUBased/od/yolo-setup/yolov3-tiny_3l.cfg";
    std::string weights_file = "../src/GPUBased/od/yolo-setup/yolov3-tiny_3l_last.weights";
    m_detector               = new YoloDetector(cfg_file, weights_file);
}

// Destructor
FrameProcessor::~FrameProcessor()
{
    delete m_detector;
}

// processNewFrame
void FrameProcessor::processNewFrame(FrameMetaPackaged t_newFrame)
{
    if (t_newFrame.data)
        m_processorFramesBuffer.push(t_newFrame);
}

// run
void FrameProcessor::run()
{
    m_fpsProfiling.start();
    while (true)
    {
        FrameMetaPackaged processItem = m_processorFramesBuffer.pull();
        // 1. Extract frame id.
        //int frameId = extractBarcode(processItem);
        //if (frameId != -1)
        //{
        //    processItem.id = static_cast<uint32_t>(frameId);
        //}

        // 2. Detect objects if enable
        if (m_doObjDetect)
        {
            std::vector<bbox_t> detectedResult = doDetectObjs(processItem);
            if (detectedResult.size() > 0)
            {
                processItem.odBoxes = detectedResult;
            }

            if (m_fpsProfiling.isTimeUp())
            {
                logInfo(FRAME_PROCESSOR_LOG, "Detect object fps: %d", m_fpsProfiling.fps());
            }
        }

        Q_EMIT processed(processItem);
    }
}

// detectInitTractRect
cv::Rect FrameProcessor::detectInitTrackRect(void* t_clickedImg,
                                             uint32_t t_realImgW, uint32_t t_realImgH,
                                             float t_xPosOnQmlVidWindow, float t_yPosOnQmlVidWindow,
                                             float t_qmlVidWindowWidth, float t_qmlVidWindowHeight)
{
    int trackSize = 200;
    /**
     * Step 1. Detect objects in a small roi around clicked point.
     */
    // Calculate position x, y in actual prossing image.
    float realPosX = t_xPosOnQmlVidWindow * t_realImgW / static_cast<float>(t_qmlVidWindowWidth);
    float realPosY = t_yPosOnQmlVidWindow * t_realImgH / static_cast<float>(t_qmlVidWindowHeight);

    // Create an area with maximum size of [512x512] or
    // 4x m_trackSize expanded from click point for OD
    cv::Rect g_roi;
    g_roi.x       = ((static_cast<int>(realPosX) - trackSize) < 0) ? 0 :
                                                       (static_cast<int>(realPosX) - trackSize);
    g_roi.y       = ((static_cast<int>(realPosY) - trackSize) < 0) ? 0 :
                                                       (static_cast<int>(realPosY) - trackSize);

    g_roi.width   = ((g_roi.x + trackSize * 2) >= static_cast<int>(t_realImgW))
                    ? (static_cast<int>(t_realImgW ) - g_roi.x) : (trackSize * 2);

    g_roi.height  = ((g_roi.y + trackSize * 2) >= static_cast<int>(t_realImgH))
                    ? (static_cast<int>(t_realImgH) - g_roi.y) : (trackSize * 2);

    if(g_roi.width > MAX_SIZE)
    {
        g_roi.x += (g_roi.width - MAX_SIZE) / 2;
        g_roi.width = MAX_SIZE;
    }

    if(g_roi.height > MAX_SIZE)
    {
        g_roi.y += (g_roi.height-MAX_SIZE) / 2;
        g_roi.height = MAX_SIZE;
    }

    image_t i420Img;
    i420Img.c = 1;
    i420Img.h = static_cast<int>(t_realImgH) * 3/2;
    i420Img.w = static_cast<int>(t_realImgW);
    i420Img.data = static_cast<float *>(t_clickedImg);
    std::vector<bbox_t> detectedBoxes = m_detector->gpu_detect_roi_I420(i420Img, g_roi);

    /**
     * Step 2. Choose the object which has center point nearest the clicked point.
     */
    if(detectedBoxes.size() > 0)
    {
        float minDist = 1920.f;
        int minIdx = -1;
        for(size_t i = 0; i < detectedBoxes.size(); i++)
        {
            if(detectedBoxes[i].obj_id != 0 && detectedBoxes[i].obj_id != 11)
            {
                double dist = std::pow(detectedBoxes[i].x + detectedBoxes[i].w/2 - realPosX, 2) +
                             std::pow(detectedBoxes[i].y + detectedBoxes[i].h/2 - realPosY, 2);
                if(static_cast<float>(dist) < minDist)
                {
                    minDist = static_cast<float>(dist);
                    minIdx = int(i);
                }
            }
        }
        // If there is no valid object detected
        if(minIdx != -1)
        {   // Track mode
            return cv::Rect(int(detectedBoxes[minIdx].x), int(detectedBoxes[minIdx].y),
                            int(detectedBoxes[minIdx].w), int(detectedBoxes[minIdx].h));

        }
    }

    // if no object detected -> change mode to scene steering at click point.
    g_roi.width = t_realImgH / 2;
    g_roi.height = t_realImgH / 2;
    return g_roi;
}


// toggleOD
void FrameProcessor::toggleOD()
{
    m_doObjDetect = !m_doObjDetect;
}

// extractBarcode
int FrameProcessor::extractBarcode(const FrameMetaPackaged& t_frame)
{
//    cv::Mat frameInGray(static_cast<int>(t_frame.height), static_cast<int>(t_frame.width),
//                        CV_8UC1, t_frame.data);
//    int res = -1;
//    std::vector<decodedObject> decodedObjects;
//    int zbarWidth = 130;
//    int zbarHeight = 65;
//    cv::Rect rectZBar(0, 0, zbarWidth, zbarHeight);
//    cv::Mat zbarMat = frameInGray(rectZBar);
//    ZbarLibs::decode(zbarMat, decodedObjects);

//    if (decodedObjects.size() > 0) {
//        std::string frameID =
//            decodedObjects[0].data.substr(0, decodedObjects[0].data.length() - 1);
//        res = std::atoi(frameID.c_str());
//    }
//    return res;
    return -1;
}

// doDetectObjs
std::vector<bbox_t> FrameProcessor::doDetectObjs(const FrameMetaPackaged& t_frame)
{
    image_t input;
    input.c = 1;
    input.h = static_cast<int>(t_frame.height) * 3 / 2;
    input.w = static_cast<int>(t_frame.width);
    input.data = static_cast<float *>(t_frame.data);
    cudaDeviceSynchronize();
    return m_detector->gpu_detect_I420(input, static_cast<int>(t_frame.width),
                                                  static_cast<int>(t_frame.height));
}

