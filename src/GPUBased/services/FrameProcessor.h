/**
 *==================================================================================================
 * Project: Treron-Pgcs
 * Module: FrameProcessor
 * Module Short Description: Process image before it is displayed on screen. Some works like
 *                           extract barcode id, object detection, ...
 *
 * Author: Trung-Ng (trungnd233@viettel.com.vn)
 * Date: 11/06/2020 09:58
 * Organization: Viettel Aerospace Institude - Viettel Group
 *
 * (c) VTX 2020. All Rights reserved.
 * =================================================================================================
 */

#ifndef FRAMEPROCESSOR_H
#define FRAMEPROCESSOR_H

//--- From C++, Linux
#include <iostream>
#include <cmath>

//--- From Qt
#include <QObject>
#include <QThread>

//--- From Opencv
#include <opencv2/opencv.hpp>

//--- From Utils
#include "inmemory-storage/RingBuffer.h"
#include "profiling/FpsProfiling.h"
#include "logging/LogApis.h"
using namespace Utils;

//--- From this project
#include "helper/FrameMetaPackaged.h"
#include "../od/yolo_v3_class.hpp"
//#include "../../Zbar/ZbarLibs.h"

#define MAX_SIZE    640
#define FRAME_PROCESSOR_LOG "[FrameProcessor]"
class FrameProcessor : public QThread
{
    Q_OBJECT
    public:
        explicit FrameProcessor(QObject* parent = 0);
        ~FrameProcessor() override;

        /**
         * @brief processNewFrame: Add new packaged frame to waiting processor buffer.
         * @param t_newFrame: New frame image just decoded from VFrameDecoder module.
         */
        void processNewFrame(FrameMetaPackaged t_newFrame);

        /**
         * @brief run: Pull frame in waiting buffer to do process.
         */
        void run() override;

        /**
         * @brief detectInitTrackRect: Detect exactly the size and position of object
         *                             which is intented clicking by user.
         *
         * @param t_clickedImg: The image when user click.
         * @param t_realImgW: The actual image width
         * @param t_realImgH: The actual image height
         * @param t_xPosOnQmlVidWindow: The mouse postion in x axis when user click on qml HMI.
         * @param t_yPosOnQmlVidWindow: The mouse postion in y axis when user click on qml HMI.
         * @param t_qmlVidWindowWidth: The width of window where display video on qml HMI.
         * @param t_qmlVidWindowHeight: The height of window where display video on qml HMI.
         *
         * @return Detail rect of target object.
         *
         * @note the x, y, width, height input params is get when user interact with qml HMI,
         *       so before doing any algorithm logic, it need to be converted into real image size.
         */
        cv::Rect detectInitTrackRect(void* t_clickedImg, uint32_t t_realImgW, uint32_t t_realImgH,
                                     float t_xPosOnQmlVidWindow, float t_yPosOnQmlVidWindow,
                                     float t_qmlVidWindowWidth, float t_qmlVidWindowHeight);

        /**
         * @brief toggleOD: Turn on/off object detection job.
         */
        void toggleOD();

    Q_SIGNALS:
        /**
         * @brief processed: Fire signal when image is done processing.
         * @param t_frame
         */
        void processed(FrameMetaPackaged t_frame);

    protected:
        /**
         * @brief extractBarcode: Extract id is embeded in barcode.
         * @param t_frame
         * @return
         */
        int extractBarcode(const FrameMetaPackaged& t_frame);

        /**
         * @brief doDetectObjs: Detect objects in entire image.
         * @param t_frame
         * @return
         */
        std::vector<bbox_t> doDetectObjs(const FrameMetaPackaged& t_frame);

    private:
        Storage::RingBuffer<FrameMetaPackaged> m_processorFramesBuffer{2};
        YoloDetector*                          m_detector;
        FpsProfiling                           m_fpsProfiling;
        bool                                   m_doObjDetect{true};
};

#endif // FRAMEPROCESSOR_H
