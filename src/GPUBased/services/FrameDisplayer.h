/**
 *==================================================================================================
 * Project: Treron-Pgcs
 * Module: FrameDisplayer
 * Module Short Description: Do all preworks before display image to qml.
 *
 * Author: Trung-Ng (trungnd233@viettel.com.vn)
 * Date: 11/06/2020 09:58
 * Organization: Viettel Aerospace Institude - Viettel Group
 *
 * (c) VTX 2020. All Rights reserved.
 * =================================================================================================
 */
#ifndef FRAMEDISPLAYER_H
#define FRAMEDISPLAYER_H

//--- From C++, Linux
#include <iostream>
#include <cmath>

//--- From Qt
#include <QObject>
#include <QThread>

//--- From Opencv
#include <opencv2/opencv.hpp>

//--- From Utils
#include "inmemory-storage/RingBuffer.h"
using namespace Utils;

//--- From this project
#include "helper/FrameMetaPackaged.h"
#include "helper/display/VideoRender.h"

#define FRAME_DISPLAYER_LOG "[FrameDisplayer]"
class FrameDisplayer : public QThread
{
    Q_OBJECT
    public:
        explicit FrameDisplayer(QObject* parent = nullptr);
        ~FrameDisplayer() override;

        /**
         * @brief displayFrame: Add new ready frame to waiting displayer buffer.
         * @param t_frame
         */
        void displayFrame(const FrameMetaPackaged& t_frame);

        /**
         * @brief run: Get frame from buffer, do prework and fire signal ready display image to
         *             render worker.
         */
        void run() override;

        /**
         * @brief setDisplayWorker
         */
        void setDisplayWorker(VideoRender* t_renderWorker);

    public Q_SLOTS:
        void doGlRender(unsigned char *t_data, int t_width, int t_height,
                                 float* t_warpMatrix = nullptr, unsigned char* t_dataOut = nullptr);
    Q_SIGNALS:
        void readyToRender(unsigned char *t_data, int t_width, int t_height,
                                 float* t_warpMatrix = nullptr, unsigned char* t_dataOut = nullptr);
    protected:
        /**
         * @brief drawBoxes: Draw detected object box to image.
         * @param t_frame
         */
        void drawBoxes(FrameMetaPackaged& t_frame);

    private:
        Storage::RingBuffer<FrameMetaPackaged> m_displayerFramesBuffer{2};
        FrameMetaPackaged                      m_displayingFrame;
        VideoRender*                           m_vidRender{nullptr};
};

#endif // FRAMEDISPLAYER_H
