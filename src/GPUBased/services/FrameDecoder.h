/**
 *==================================================================================================
 * Project: Treron-Pgcs
 * Module: FrameDecoder
 * Module Short Description: Decode image from from uri source like udp, file.
 * Author: Trung-Ng (trungnd233@viettel.com.vn)
 * Date: 11/06/2020 09:58
 * Organization: Viettel Aerospace Institude - Viettel Group
 *
 * (c) VTX 2020. All Rights reserved.
 * =================================================================================================
 */

#ifndef FRAMEDECODER_H
#define FRAMEDECODER_H

//--- From C++, Linux
#include <iostream>

//--- From Qt
#include <QObject>
#include <QThread>
#include <QRegularExpression>

//--- From gstreamer
#include <gst/app/gstappsink.h>
#include <gst/app/gstappsrc.h>
#include <gst/gst.h>
#include <gst/gstutils.h>
#include <gst/gstsegment.h>

//--- From Utils
#include "inmemory-storage/RingBuffer.h"
using namespace Utils;

//--- From this project
#include "helper/FrameMetaPackaged.h"
#include "helper/GpuMemoryBuffer.h"

/**
 * @brief The FrameDecoder class: Decode image from udp/file.
 */
#define FRAME_DECODER "[FrameDecoder]"
class FrameDecoder : public QThread
{
    Q_OBJECT
    public:
        enum class URI {
            FILE,
            RTP,
            RTSP
        };

        FrameDecoder(QObject* parent = 0);
        ~FrameDecoder() override;

        void pause(bool t_pause);

        gint64 getTotalTime();

        gint64 getPosCurrent();

        void setSpeed(float t_speed);

        void goToPosition(float t_percent);

        void stopPipeline();

        void run() override;

        bool stop();

        void restartPipeline();

        void setSource(std::string t_ip, int t_port);

        URI decodeUriType();

    Q_SIGNALS:
        void newImageDecoded(FrameMetaPackaged t_decodedFrameMeta);
        void sourceFileIsSet(QString t_fileName);

     protected:
        bool initPipeline();

        static GstFlowReturn wrapperOnNewSample(GstAppSink *t_vsink, gpointer t_uData);

        static void wrapperOnEOS(_GstAppSink *t_sink, void *t_uData);

        static GstFlowReturn wrapperOnNewPreroll(_GstAppSink *t_sink, void *t_uData);

        static gboolean wrapperOnBusCall(GstBus *t_bus, GstMessage *t_msg,
                                         gpointer t_uData);

        static GstPadProbeReturn wrapperPadDataMod(GstPad *t_pad, GstPadProbeInfo *t_info, gpointer t_uData);

        static void wrapperRun(void *t_pointer);

        static gboolean wrapNeedKlv(void* t_userPointer);

        static void wrapStartFeedKlv(GstElement * t_pipeline, guint t_size, void* t_userPointer);

        GstFlowReturn onNewSample(GstAppSink *t_vsink, gpointer t_userData);

        void onEOS(_GstAppSink *t_sink, void *t_userData);

        GstFlowReturn onNewPreroll(_GstAppSink *t_sink, void *t_userData);

        gboolean onBusCall(GstBus *bus, GstMessage *t_msg, gpointer t_data);

        GstPadProbeReturn padDataMod(GstPad *t_pad, GstPadProbeInfo *t_info,
                                     gpointer t_uData);
        gboolean needKlv(void* t_userPointer);


    private:
        float               m_speed = 1;
        GMainLoop*          m_loop{nullptr};
        GstPipeline*        m_pipeline{nullptr};
        std::string         m_pipelineStr;
        GstBus*             m_bus{nullptr};
        GstAppSrc*          m_klvAppSrc{nullptr};
        GError*             m_err{nullptr};
        guint               m_busWatchID;
        GstAppSink*         m_appSink{nullptr};
        std::string         m_ip;
        uint16_t            m_port;
        gint64              m_totalTime{1800000000000};
        bool                m_stop{false};
        uint32_t            m_currID{0};
        int                 m_metaID{-1};
        int                 m_metaPerSecond{5};
        URI                 m_uriType{URI::RTP};

};

#endif // FRAMEDECODER_H
