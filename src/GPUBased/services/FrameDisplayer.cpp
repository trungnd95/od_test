/**
 *==================================================================================================
 * Project: Treron-Pgcs
 * Module: FrameDisplayer
 * Module Short Description: Do all preworks before display image to qml.
 *
 * Author: Trung-Ng (trungnd233@viettel.com.vn)
 * Date: 11/06/2020 09:58
 * Organization: Viettel Aerospace Institude - Viettel Group
 *
 * (c) VTX 2020. All Rights reserved.
 * =================================================================================================
 */

#include "FrameDisplayer.h"
#include "helper/OdLabelHelper.h"

// Constructor
FrameDisplayer::FrameDisplayer(QObject* parent)
    : QThread (parent)
{
    connect(this, &FrameDisplayer::readyToRender, this, &FrameDisplayer::doGlRender);
}

// Destructor
FrameDisplayer::~FrameDisplayer()
{}

// displayFrame
void FrameDisplayer::displayFrame(const FrameMetaPackaged& t_frame)
{
    if (t_frame.data)
        m_displayerFramesBuffer.push(t_frame);
}

// run
void FrameDisplayer::run()
{
    while (true)
    {
        FrameMetaPackaged packagedFrame = m_displayerFramesBuffer.pull();
        if (packagedFrame.odBoxes.size() > 0)
        {
            drawBoxes(packagedFrame);
        }
        Q_EMIT readyToRender(static_cast<unsigned char*>(packagedFrame.data),
                     static_cast<int>(packagedFrame.width), static_cast<int>(packagedFrame.height));
    }
}

// setDisplayWorker
void FrameDisplayer::setDisplayWorker(VideoRender* t_renderWorker)
{
    m_vidRender = t_renderWorker;
}


// glRender
void FrameDisplayer::doGlRender(unsigned char *t_data, int t_width, int t_height,
                              float* t_warpMatrix, unsigned char* t_dataOut)
{
    if (m_vidRender)
        m_vidRender->handleNewFrame(t_data, t_width, t_height, t_warpMatrix, t_dataOut);
}

// drawBoxes
void FrameDisplayer::drawBoxes(FrameMetaPackaged& t_frame)
{
    uint32_t limitW = 30;
    uint32_t limitH = 60;
    for (auto box : t_frame.odBoxes)
    {
        cv::Rect rectObject = cv::Rect(
                    static_cast<int>(box.x),
                    static_cast<int>(box.y),
                    static_cast<int>(box.w),
                    static_cast<int>(box.h)
                    );
        if((box.w * box.h) < (limitW * limitH) )
        {
            continue;
        }
        cv::Mat img(t_frame.height * 3 / 2, static_cast<int>(t_frame.width), CV_8UC1, t_frame.data);
        cv::rectangle(img, rectObject, cv::Scalar(255, 0, 0,255), 2);
        cv::putText(img, odLabelMap[int(box.obj_id)], cv::Point2f(int(box.x + box.w / 4), int(box.y - 5)),
                              cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, cv::Scalar(255, 255, 255, 0), 1);
    }
}
