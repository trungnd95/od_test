/**
 *==================================================================================================
 * Project: Treron-Pgcs
 * Module: FrameSaver
 * Module Short Description: Gcs video recording worker.
 *
 * Author: Trung-Ng (trungnd233@viettel.com.vn)
 * Date: 11/06/2020 09:58
 * Organization: Viettel Aerospace Institude - Viettel Group
 *
 * (c) VTX 2020. All Rights reserved.
 * =================================================================================================
 */

#include "FrameSaver.h"
#include "helper/FileHelper.h"

// FrameSaver
static std::string outFile = "";
FrameSaver::FrameSaver(QString t_fileName, QObject* parent)
    : QThread(parent)
{

    createSavingFolder();
    outFile = "video-record/" + t_fileName.toStdString();
    m_saverParams.outFilename = outFile.c_str();
    chooseCodingType();
}

FrameSaver::FrameSaver(QString t_fileName, uint32_t t_width, uint32_t t_height, QObject* parent)
    : QThread(parent)
{
    createSavingFolder();
    std::string outFile = "video-record/" + t_fileName.toStdString();
    m_saverParams.outFilename = outFile.c_str();
    m_saverParams.streamW = t_width;
    m_saverParams.streamH = t_height;
    chooseCodingType();
}

FrameSaver::FrameSaver(QString t_fileName, uint32_t t_fps, QObject* parent)
    : QThread(parent)
{
    createSavingFolder();
    std::string outFile = "video-record/" + t_fileName.toStdString();
    m_saverParams.outFilename = outFile.c_str();
    m_saverParams.streamFps   = t_fps;
    chooseCodingType();
}

FrameSaver::FrameSaver(QString t_fileName, uint32_t t_width, uint32_t t_height, uint32_t t_fps,
                       QObject* parent)
    : QThread(parent)
{
    createSavingFolder();
    std::string outFile = "video-record/" + t_fileName.toStdString();
    m_saverParams.outFilename = outFile.c_str();
    m_saverParams.streamW = t_width;
    m_saverParams.streamH = t_height;
    m_saverParams.streamFps = t_fps;
    chooseCodingType();
}

// ~FrameSaver
FrameSaver::~FrameSaver()
{
    //delete m_saverWorker;
}

// saveNewFrame
void FrameSaver::saveNewFrame(const FrameMetaPackaged& t_frame)
{
    if (t_frame.data)
    {
        if (!m_workerCreated)
        {
            m_saverParams.capInput.m_width = t_frame.width;
            m_saverParams.capInput.m_height = t_frame.height;
            m_saverParams.capInput.m_framerate = 25;
            m_saverParams.streamW = 640;
            m_saverParams.streamH = 480;
            m_saverParams.streamFps = 10;
            m_saverWorker = GstEncoder::create(m_saverParams);
            m_workerCreated = true;
        }

        if (m_saverParams.capInput.m_width * m_saverParams.capInput.m_height * 3 /  2 != t_frame.size)
        {
            printf("[FrameSaver] -- Image is invalid dimension or format");
            return;
        }
        m_saverFramesBuffer.push(t_frame);
    }
}

// run
void FrameSaver::run()
{
    while(true)
    {
        if (m_saverWorker != nullptr)
        {
            FrameMetaPackaged framePackaged = m_saverFramesBuffer.pull();
            if (framePackaged.data)
                m_saverWorker->encodeFrame(framePackaged.data, framePackaged.size);
        }
        else
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }
}

// createSavingFolder
void FrameSaver::createSavingFolder()
{
    if (!File::directoryExist("video-record"))
    {
        File::createFolder("video-record");
    }
}

// chooseCodingType
void FrameSaver::chooseCodingType()
{
    if (File::fileExtension(std::string(m_saverParams.outFilename)) == "avi")
    {
        m_saverParams.codeType = Gst::Codec::NV_H264;
    }
    else
    {
        m_saverParams.codeType = Gst::Codec::NV_H265;
    }
}
