/**
 *==================================================================================================
 * Project: Treron-Pgcs
 * Module: FrameSaver
 * Module Short Description: Gcs video recording worker.
 *
 * Author: Trung-Ng (trungnd233@viettel.com.vn)
 * Date: 11/06/2020 09:58
 * Organization: Viettel Aerospace Institude - Viettel Group
 *
 * (c) VTX 2020. All Rights reserved.
 * =================================================================================================
 */

#ifndef FRAMESAVER_H
#define FRAMESAVER_H

//--- From C++, Linux
#include <iostream>
#include <cmath>

//--- From Qt
#include <QObject>
#include <QThread>

//--- From Opencv
#include <opencv2/opencv.hpp>

//--- From Utils
#include "inmemory-storage/RingBuffer.h"
#include "gst/gst-encoder/GstEncoder.h"
using namespace Utils;

//--- From this project
#include "helper/FrameMetaPackaged.h"

class FrameSaver : public QThread
{
    Q_OBJECT
    public:
        /**
         * @brief FrameSaver: Recording video with dimension equal default input stream.
         * @param t_fileName
         * @param parent
         */
        FrameSaver(QString t_fileName, QObject* parent = 0);

        /**
         * @brief FrameSaver
         * @param t_fileName: Recording video with custom width,height dimension
         * @param t_width
         * @param t_height
         * @param parent
         */
        FrameSaver(QString t_fileName, uint32_t t_width, uint32_t t_height, QObject* parent = 0);

        /**
         * @brief FrameSaver
         * @param t_fileName: Recording video with custom fps
         * @param t_fps
         * @param parent
         */
        FrameSaver(QString t_fileName, uint32_t t_fps, QObject* parent = 0);

        /**
         * @brief FrameSaver
         * @param t_fileName: Recording video with custom dimension and fps.
         * @param t_width
         * @param t_height
         * @param t_fps
         * @param parent
         */
        FrameSaver(QString t_fileName, uint32_t t_width, uint32_t t_height, uint32_t t_fps,
                                                                              QObject* parent = 0);
        ~FrameSaver() override;

        /**
         * @brief saveNewFrame: Add new frame to waiting buffer.
         * @param t_frame
         */
        void saveNewFrame(const FrameMetaPackaged& t_frame);

        /**
         * @brief run: Read frame from waiting buffer and encode.
         */
        void run() override;

    protected:
        void createSavingFolder();
        void chooseCodingType();
    private:
        Storage::RingBuffer<FrameMetaPackaged>  m_saverFramesBuffer{2};
        GstEncoder::Option                      m_saverParams;
        GstEncoder*                             m_saverWorker{nullptr};
        bool                                    m_run;
        bool                                    m_workerCreated{false};
};
#endif // FRAMESAVER_H
