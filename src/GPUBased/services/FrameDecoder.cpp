#include "FrameDecoder.h"
#include "helper/FileHelper.h"

// FFmpeg
extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/avutil.h>
#include <libavutil/pixdesc.h>
#include <libswscale/swscale.h>
}

// FrameDecoder
FrameDecoder::FrameDecoder(QObject* parent)
    : QThread(parent)
{}

// ~FrameDecoder
FrameDecoder::~FrameDecoder()
{
    delete m_appSink;
    delete m_bus;
    delete m_pipeline;
    delete  m_err;
}

// run
void FrameDecoder::run()
{
    gst_init(0, NULL);
    m_loop = g_main_loop_new(NULL, FALSE);
    while(!m_stop)
    {
        initPipeline();
        GstStateChangeReturn result =
                gst_element_set_state(GST_ELEMENT(m_pipeline), GST_STATE_PLAYING);

        if (result != GST_STATE_CHANGE_SUCCESS)
        {
            logInfo(FRAME_DECODER, "ReadingCam-gstreamer failed to set pipeline state to PLAYING "
                   "(error %u)\n",
                   result);
        }
        g_main_loop_run(m_loop);
        gst_element_set_state(GST_ELEMENT(m_pipeline), GST_STATE_NULL);
        gst_object_unref(m_pipeline);
    }
}

// setSource
void FrameDecoder::setSource(std::string t_ip, int t_port)
{
    QString qSource = QString::fromStdString(t_ip);
    if(qSource.contains("filesrc"))
    {
        QString fileName;
        QRegularExpression re("[ ]*filesrc[ ]+location=(.*)[ ]+");
        QRegularExpressionMatch match = re.match(qSource.split("!")[0]);
        if (match.hasMatch())
        {
            fileName = match.captured(1);
        }
        logInfo(FRAME_DECODER, "Source file decoded: %s", fileName.toStdString().c_str());
        if(Utils::File::fileExists(fileName.toStdString().c_str()))
        {
            av_register_all();
            av_log_set_level(AV_LOG_QUIET);
            AVFormatContext* inctx = nullptr;
            int ret = avformat_open_input(&inctx, fileName.toStdString().c_str(), nullptr, nullptr);
            ret = avformat_find_stream_info(inctx, nullptr);
            m_totalTime = inctx->duration*1000;
            avformat_close_input(&inctx);
            Q_EMIT sourceFileIsSet(fileName);
            m_uriType = URI::FILE;
        }

    }
    m_ip = t_ip;
    m_port = static_cast<uint16_t>(t_port);
    g_main_loop_quit(m_loop);
}

FrameDecoder::URI FrameDecoder::decodeUriType()
{
    return m_uriType;
}

// stop
bool FrameDecoder::stop()
{
    if (m_loop != NULL && g_main_loop_is_running(m_loop) == TRUE)
    {
        g_main_loop_quit(m_loop);
        m_stop = true;
        return true;
    }
    return false;
}

// wrapperRun
void FrameDecoder::wrapperRun(void *t_pointer)
{
    FrameDecoder *itseft = (FrameDecoder *)t_pointer;
    return itseft->run();
}

// wrapperOnNewSample
GstFlowReturn FrameDecoder::wrapperOnNewSample(GstAppSink *t_vsink,
                                                gpointer t_uData)
{
    FrameDecoder *itseft = (FrameDecoder *) t_uData;
    return itseft->onNewSample(t_vsink, t_uData);
}

// wrapperOnEOS
void FrameDecoder::wrapperOnEOS(_GstAppSink *t_sink, void *t_uData)
{
    FrameDecoder *itseft = (FrameDecoder *)t_uData;
    return itseft->onEOS(t_sink, t_uData);
}

// wrapperOnBusCall
gboolean FrameDecoder::wrapperOnBusCall(GstBus *t_bus, GstMessage *t_msg,
                                         gpointer t_uData)
{
    FrameDecoder *itseft = (FrameDecoder *)t_uData;
    return itseft->onBusCall(t_bus, t_msg, t_uData);
}

// wrapperOnNewPreroll
GstFlowReturn FrameDecoder::wrapperOnNewPreroll(_GstAppSink *t_sink,
                                                 void *t_uData)
{
    FrameDecoder *itseft = (FrameDecoder *)t_uData;
    return itseft->onNewPreroll(t_sink, t_uData);
}

// wrapperPadDataMod
GstPadProbeReturn FrameDecoder::wrapperPadDataMod(GstPad *t_pad,
                                                   GstPadProbeInfo *t_info,
                                                   gpointer t_uData)
{
    FrameDecoder *itseft = (FrameDecoder *)t_uData;
    return itseft->padDataMod(t_pad, t_info, t_uData);
}
// wrapNeedKlv
gboolean FrameDecoder::wrapNeedKlv(void* t_userPointer)
{
    FrameDecoder *itseft = (FrameDecoder *)t_userPointer;
    return itseft->needKlv(t_userPointer);
}

// wrapStartFeedKlv
void FrameDecoder::wrapStartFeedKlv(GstElement * t_pipeline, guint t_size, void* t_userPointer)
{
    FrameDecoder *itseft = (FrameDecoder *)t_userPointer;
    itseft->needKlv(t_userPointer);
}

// needKlv
gboolean FrameDecoder::needKlv(void* t_userPointer)
{
    Q_UNUSED(t_userPointer);
    return true;
}

// onNewSample
GstFlowReturn FrameDecoder::onNewSample(GstAppSink *t_vsink, gpointer t_uData)
{
    if (!t_vsink)
        return GST_FLOW_ERROR;

    GstSample* gstSample = gst_app_sink_pull_sample((GstAppSink *) t_vsink);
    if (!gstSample)
    {
        //printf(GST_READER_LOG "gst_app_sink_pull_sample return NULL\n");
        return GST_FLOW_ERROR;
    }

    GstBuffer* gstBuffer = gst_sample_get_buffer(gstSample);
    if (!gstBuffer)
    {
        //printf(GST_READER_LOG "gst_sample_get_buffer return NULL\n");
        return GST_FLOW_ERROR;
    }

    // retrieve
    GstMapInfo map;
    if (!gst_buffer_map(gstBuffer, &map, GST_MAP_READ))
    {
        //printf(GST_READER_LOG " gst_buffer_map failed...\n");
        return GST_FLOW_ERROR;
    }

    //gst_util_dump_mem(map.data, map.size);

    void* gstData = map.data; //GST_BUFFER_DATA(gstBuffer);
    const uint32_t gstSize = map.size; //GST_BUFFER_SIZE(gstBuffer);

    if (!gstData)
    {
        //printf(GST_READER_LOG "gst_buffer had NULL data pointer...\n");
        gst_sample_unref(gstSample);
        return GST_FLOW_ERROR;
    }

    static GpuMemoryBuffer s_framesMemBuffer{gstSize};
    void* frameMemPtr = s_framesMemBuffer.next();
    memcpy(frameMemPtr, gstData, gstSize);

    // retrieve caps
    GstCaps* gstCaps = gst_sample_get_caps(gstSample);

    if (!gstCaps)
    {
        //printf(GST_READER_LOG "gst_buffer had NULL caps...\n");
        gst_sample_unref(gstSample);
        return GST_FLOW_ERROR;
    }

    GstStructure* gstCapsStruct = gst_caps_get_structure(gstCaps, 0);

    if (!gstCapsStruct)
    {
        //printf(GST_READER_LOG "gst_caps had NULL structure...\n");
        gst_sample_unref(gstSample);
        return GST_FLOW_ERROR;
    }

    // get width & height of the buffer
    int width  = 0;
    int height = 0;

    if( !gst_structure_get_int(gstCapsStruct, "width", &width) ||
        !gst_structure_get_int(gstCapsStruct, "height", &height) )
    {
        //printf(GST_READER_LOG "gst_caps missing width/height...\n");
        gst_sample_unref(gstSample);
        return GST_FLOW_ERROR;
    }

    if( width < 1 || height < 1 )
    {
        gst_sample_unref(gstSample);
        return GST_FLOW_ERROR;
    }

    FrameMetaPackaged newFramePackaged;
    newFramePackaged.id     = m_currID++;
    newFramePackaged.data   = frameMemPtr;
    newFramePackaged.width  = static_cast<uint32_t>(width);
    newFramePackaged.height = static_cast<uint32_t>(height);
    newFramePackaged.size   = gstSize;
    Q_EMIT newImageDecoded(newFramePackaged);
    gst_buffer_unmap(gstBuffer, &map);
    gst_sample_unref(gstSample);
    return GST_FLOW_OK;
}

// onEOS
void FrameDecoder::onEOS(_GstAppSink *t_sink, void *t_uData)
{
    Q_UNUSED(t_sink);
    Q_UNUSED(t_uData);
}

// onBusCall
gboolean FrameDecoder::onBusCall(GstBus *t_bus, GstMessage *t_msg,
                                  gpointer t_uData)
{
    GMainLoop *loop = (GMainLoop *)t_uData;

    switch (GST_MESSAGE_TYPE(t_msg))
    {
        case GST_MESSAGE_EOS:
        {
            g_print("\nEnd of stream");
            //        g_main_loop_quit(loop);
            g_signal_stop_emission_by_name(m_klvAppSrc, "need-data");
            break;
        }

        case GST_MESSAGE_ERROR:
        {
            gchar *debug;
            GError *error;
            gst_message_parse_error(t_msg, &error, &debug);
            g_free(debug);
            g_printerr("\nError: %s", error->message);
            g_error_free(error);
            break;
        }

        default: break;
    }
    return TRUE;
}


//  onNewPreroll
GstFlowReturn FrameDecoder::onNewPreroll(_GstAppSink *t_sink, void *t_uData)
{
    Q_UNUSED(t_sink);
    Q_UNUSED(t_uData);
    logInfo(FRAME_DECODER, "\ngstreamer decoder onPreroll");
    return GST_FLOW_OK;
}

// padDataMod
GstPadProbeReturn FrameDecoder::padDataMod(GstPad *t_pad, GstPadProbeInfo *t_info,
                                            gpointer t_uData)
{
    Q_UNUSED(t_pad);
    Q_UNUSED(t_info);
    Q_UNUSED(t_uData);
    return GST_PAD_PROBE_OK;
}

// getTotalTime
gint64 FrameDecoder::getTotalTime()
{
    return m_totalTime;
}

// getPosCurrent
gint64 FrameDecoder::getPosCurrent()
{
    gint64 pos;
    gst_element_query_position(GST_ELEMENT(m_pipeline), GST_FORMAT_TIME, &pos);
    return pos;
}

// restartPipeline
void FrameDecoder::restartPipeline()
{
    GstStateChangeReturn result =
            gst_element_set_state(GST_ELEMENT(m_pipeline), GST_STATE_PLAYING);

    if (result != GST_STATE_CHANGE_SUCCESS) {
        logInfo(FRAME_DECODER, "ReadingCam-gstreamer failed to restart pipeline"
               "(error %u)", result);
    }
}

// setSpeed
void FrameDecoder::setSpeed(float t_speed)
{
    if(m_pipeline == NULL)
        return;

    printf("Change speed to %f\r\n", t_speed);

    gint64 posCurrent = getPosCurrent();
    printf("%ld = posCurrent\r\n",posCurrent);

    m_speed = t_speed;
    pause(true);
    gst_element_seek(GST_ELEMENT(m_pipeline),t_speed,
                     GST_FORMAT_TIME,
                     GstSeekFlags(GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_ACCURATE),
                     GST_SEEK_TYPE_SET,(gint64)(posCurrent),
                     GST_SEEK_TYPE_SET,GST_CLOCK_TIME_NONE);
    pause(false);
}

// pause
void FrameDecoder::pause(bool t_pause)
{
    if(m_pipeline == NULL) return;
    if(t_pause)
    {
        gst_element_set_state(GST_ELEMENT(m_pipeline), GST_STATE_PAUSED);
    }else
    {
        gst_element_set_state(GST_ELEMENT(m_pipeline), GST_STATE_PLAYING);
    }
}

// goToPosition
void FrameDecoder::goToPosition(float t_percent)
{
    printf("goToPosition %f%\r\n",t_percent);

    if(m_pipeline == NULL) return;

    gint64 posNext = (gint64)((double)m_totalTime * (double)t_percent);
    //    printf("%ld = (gint64)(percent*100*GST_SECOND)\r\n",(gint64)(percent*100*GST_SECOND));
    //    printf("%ld = posNext\r\n",posNext);
    //    printf("%f = m_speed\r\n",m_speed);
    gst_element_seek(GST_ELEMENT(m_pipeline),m_speed,
                     GST_FORMAT_TIME,
                     GstSeekFlags(GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_ACCURATE),
                     GST_SEEK_TYPE_SET,(gint64)(posNext),
                     GST_SEEK_TYPE_SET,GST_CLOCK_TIME_NONE);
}

// initPipeline
bool FrameDecoder::initPipeline()
{
    std::string m_pipelineStr = m_ip + std::string(" ! appsink name=mysink sync=")+
        (QString::fromStdString(m_ip).contains("filesrc")?std::string("true"):std::string("false"))+""
//        " t. ! queue ! mpegtsmux name=mux mux. ! filesink location="+m_filename+".mp4 "
//        " appsrc name=klvsrc ! mux. "
            ;
    logInfo(FRAME_DECODER, "Frame decode pipeline: %s\r", m_pipelineStr.data());
    m_pipeline = GST_PIPELINE(gst_parse_launch(m_pipelineStr.data(), &m_err));

    if ((m_err != NULL) || (!m_pipeline))
    {
        logWarning(FRAME_DECODER, "gstreamer decoder failed to create pipeline\n");
        g_error_free(m_err);
        return false;
    }
    else
    {
        logSuccess(FRAME_DECODER, "gstreamer decoder create pipeline success\n");
    }
    // check end of stream
    m_bus = gst_pipeline_get_bus(GST_PIPELINE(m_pipeline));
    m_busWatchID = gst_bus_add_watch(m_bus, wrapperOnBusCall, (void *)this);
    gst_object_unref(m_bus);

    GstAppSink *m_appsink = (GstAppSink *)gst_bin_get_by_name((GstBin *)m_pipeline, "mysink");

    if (m_appsink == nullptr)
    {
        printf("Fail to get element \n");
    }
    else
    {
        gst_app_sink_set_drop(m_appsink, true);
        g_object_set(m_appsink, "emit-signals", TRUE, NULL);
        GstAppSinkCallbacks cbs;
        memset(&cbs, 0, sizeof(GstAppSinkCallbacks));
        cbs.new_sample = wrapperOnNewSample;
        cbs.eos = wrapperOnEOS;
        cbs.new_preroll = wrapperOnNewPreroll;
        gst_app_sink_set_callbacks(m_appsink, &cbs, (void *)this, NULL);
    }

    // add call back save meta to file
    m_klvAppSrc = nullptr;
    m_klvAppSrc = (GstAppSrc *)gst_bin_get_by_name((GstBin *)m_pipeline, "klvsrc");
    if (m_klvAppSrc == nullptr) {
        g_print("Fail to get klvsrc \n");
    }else{
        g_signal_connect (m_klvAppSrc, "need-data", G_CALLBACK (wrapStartFeedKlv), (void *)this);
        /* set the caps on the source */
        GstCaps *caps = gst_caps_new_simple ("meta/x-klv",
                                             "parsed", G_TYPE_BOOLEAN, TRUE,
                                             nullptr);
        gst_app_src_set_caps(GST_APP_SRC(m_klvAppSrc), caps);
        g_object_set(GST_APP_SRC(m_klvAppSrc), "format", GST_FORMAT_TIME, nullptr);
    }

    return true;
}

// stopPipeline
void FrameDecoder::stopPipeline()
{
    if (m_loop != nullptr &&  g_main_loop_is_running(m_loop) == TRUE)
    {
        g_main_loop_quit(m_loop);
    }
    m_stop = true;
}
