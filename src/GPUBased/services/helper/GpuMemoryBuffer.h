/**
 *==================================================================================================
 * Project: Utils
 * Module: GpuMemoryBuffer.h
 * Module Short Description: Allocate package image memory (locked-memory) that shared between
 *                           CPU and GPU or direct memory access (DMA) only on GPU.
 * Author: Trung-Ng (trungnd233@viettel.com.vn)
 * Date: 15/07/2020 15:22
 * Organization: Viettel Aerospace Institude - Viettel Group
 *
 * (c) 2020 VTX. All Rights reserved.
 * =================================================================================================
 */

#ifndef GPUMEMORYBUFFER_H
#define GPUMEMORYBUFFER_H

//---- From C++, Linux libs
#include <iostream>

//---- From Utils
#include "cuda/CudaMappedMemory.h"
#include "logging/LogApis.h"
using namespace Utils;

//---- From this project

#define CUDA_MEM_LOG "[MappedMemoryBuffer]"
#define NUM_IMGS_BUFFER 30
/**
 * @brief The MappedMemoryBuffer class: The buffer contains list shared CPU/GPU memories of images.
 */
class GpuMemoryBuffer
{
    public:
        /**
         * @brief MappedMemoryBuffer: Init buffer with fixed buffer size == NUM_IMGS_BUFFER and
         *                           customizable each element size.
         * @param t_imgSize - Size of each image in buffer.
         * @param t_dma: if true, allocated dma memory only reside in gpu, otherwise create pinned
         *               memory.
         */
        explicit GpuMemoryBuffer(size_t t_imgSize, bool t_dma = false)
            : m_allocated(false),
              m_lastestRetrieved(0),
              m_bufferSize(NUM_IMGS_BUFFER),
              m_eachItemSize(t_imgSize),
              m_sharedBuffer(NULL),
              m_dma(t_dma)
        {
            if (m_sharedBuffer == NULL)
            {
                //Allocate memrory for list imgs buffer.
                const size_t bufferListSize = m_bufferSize * sizeof(void*);
                m_sharedBuffer = (void**) malloc(bufferListSize);
                memset(m_sharedBuffer, 0, bufferListSize);

                //Allocate image shared memory between CPU and GPU.
                for (uint32_t n = 0; n < m_bufferSize; n++)
                {
                    //m_sharedBuffer[n] = NULL;
                    if (t_dma)
                    {
                        if (CUDA_FAILED(cudaMalloc(&m_sharedBuffer[n], m_eachItemSize)))
                        {
                            //logDanger(CUDA_MEM_LOG, "Device mem -- failed to allocate dma memory"
                            //                             "buffer of %zu bytes\n", m_eachItemSize);
                            m_allocated = false;
                            break;
                        }
                    }
                    else
                    {
                        if (!cudaAllocMapped(&m_sharedBuffer[n], m_eachItemSize))
                        {
                            //logDanger(CUDA_MEM_LOG, "Device mem -- failed to allocate zero-copy"
                            //                             "buffer of %zu bytes\n", m_eachItemSize);
                            m_allocated = false;
                            break;
                        }
                    }

                    m_allocated = true;
                }
            }
        }

        /** Destructor */
        ~GpuMemoryBuffer()
        {
            //logInfo(IMAGE_MAPPED_MEM_LOG,
            //        "Clean image device buffer memory- [Length - Item size] : [%d - %d]",
            //                                                NUM_IMGS_BUFFER, (int)m_eachItemSize);
            printf("Clean image mapped buffer memory- [Length - Item size] : [%d - %d]\n",
                                                               NUM_IMGS_BUFFER, (int)m_eachItemSize);
            freeSpace();
        }

        /**
         * @brief next: Get the pointer to the next memory in buffer from lastest call.
         * @note: This buffer is wrapping buffer, so if will turn around to the head
         *        if it reachs to the end of buffer.
         * @return return next memory address in buffer.
         */
        void* next()
        {
            if (!m_allocated)
            {
                return NULL;
            }
            return m_sharedBuffer[m_lastestRetrieved++ % m_bufferSize];
        }

    protected:
        /**
         * @brief freeSpace: Clean memory space.
         */
        void freeSpace()
        {
            if (!m_sharedBuffer || m_bufferSize == 0)
                return;
            for (uint32_t n = 0; n < m_bufferSize; n++)
            {
                if (m_sharedBuffer[n] != NULL)
                {
                    if (m_dma)
                    {
                        CUDA(cudaFree(m_sharedBuffer[n]));
                    }
                    else
                    {
                        CUDA(cudaFreeHost(m_sharedBuffer[n]));
                    }
                    m_sharedBuffer[n] = NULL;
                }
            }

            if (m_sharedBuffer != NULL)
                free(m_sharedBuffer);
                m_sharedBuffer = NULL;
        }

    private:
        //------ Properties
        bool        m_allocated;         /**< Make sure this memory area is allocated successfully*/
        uint32_t    m_lastestRetrieved;  /**< The lastest called memory area */
        size_t      m_bufferSize;        /**< Size of the buffer */
        size_t      m_eachItemSize;      /**< Size of each item in buffer */
        void**      m_sharedBuffer;      /**< The buffer */
        bool        m_dma;               /**< Device Direct access memory or pinned memory */
};

#endif // MappedMemoryBuffer

