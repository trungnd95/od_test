/**
 *==================================================================================================
 * Project: Treron-Pgcs
 * Module: FrameMetaPackaged
 * Module Short Description: Packaged image with all related data like id, width, height, ...
 * Author: Trung-Ng (trungnd233@viettel.com.vn)
 * Date: 11/06/2020 09:58
 * Organization: Viettel Aerospace Institude - Viettel Group
 *
 * (c) VTX 2020. All Rights reserved.
 * =================================================================================================
 */

#ifndef FRAMEMETAPACKAGED_H
#define FRAMEMETAPACKAGED_H

#include <iostream>
#include <QObject>
#include "../../od/yolo_v3_class.hpp"

struct FrameMetaPackaged
{
    void*               data;
    uint32_t            id;
    uint32_t            width;
    uint32_t            height;
    uint32_t            size;
    std::vector<bbox_t> odBoxes;

    FrameMetaPackaged()
        : data(nullptr), id(0), width(0), height(0), size(0), odBoxes{}
    {}
};
Q_DECLARE_METATYPE(FrameMetaPackaged)
#endif // FRAMEMETAPACKAGED_H
