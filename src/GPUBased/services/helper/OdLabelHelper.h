/**
 *==================================================================================================
 * Project: Utils
 * Module: OdLabelHelper.h
 * Module Short Description: Helper that help convert label object id to string
 * Author: Trung-Ng (trungnd233@viettel.com.vn)
 * Date: 15/07/2020 15:22
 * Organization: Viettel Aerospace Institude - Viettel Group
 *
 * (c) 2020 VTX. All Rights reserved.
 * =================================================================================================
 */

#ifndef ODLABELHELPER_H
#define ODLABELHELPER_H

#include <iostream>
#include <map>
#include <fstream>

#include "helper/FileHelper.h"

extern std::map<int, std::string> odLabelMap;

inline void readLabelNamesToMap(const std::string& t_filePath)
{
    if (!Utils::File::fileExists(t_filePath.c_str()))
        return;

    //std::string fileContents = Utils::File::fileContent(t_filePath);
    std::ifstream fread;
    fread.open(t_filePath.c_str());
    if (fread.is_open())
    {
        std::string line;
        int i = 0;
        while (std::getline(fread, line))
        {
            odLabelMap[i++] = line;
        }
        fread.close();
    }
}

#endif // ODLABELHELPER_H
