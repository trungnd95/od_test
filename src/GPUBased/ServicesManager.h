/**
 *==================================================================================================
 * Project: Treron-Pgcs
 * Module: Pgcs image process manager
 * Module Short Description: Manage all image process features like image decoding, object detection,
 *                           object searching, image displaying, image saving, ....
 * Author: Trung-Ng (trungnd233@viettel.com.vn)
 * Date: 11/06/2020 09:58
 * Organization: Viettel Aerospace Institude - Viettel Group
 *
 * (c) VTX 2020. All Rights reserved.
 * =================================================================================================
 */

#ifndef SERVICESMANAGER_H
#define SERVICESMANAGER_H

//--- From C++, Linux
#include <iostream>

//--- From Qt
#include <QObject>

//--- From this project
#include "services/FrameDecoder.h"
#include "services/FrameProcessor.h"
#include "services/FrameDisplayer.h"
#include "services/FrameSaver.h"

/**
 * @brief The TreronVisplayAdapter class: The class manage and control all image process threads and
 *                                        behaviors.
 */
#define SERVICES_MANAGER_LOG "[ServicesManager]"

class ServicesManager : public QObject
{
    Q_OBJECT
    public:
        explicit ServicesManager(QObject* parent = 0);
        ~ServicesManager();

        /**
         * @brief start: Starting services
         */
        Q_INVOKABLE void start();

        /**
         * @brief toggleOD: Toggle object detection in FrameProcessor module
         */
        Q_INVOKABLE void toggleOD();

        /**
         * @brief setVideo: Set source input for decoder.
         * @param t_ip
         * @param t_port
         */
        Q_INVOKABLE void setVideo(QString t_ip, int t_port = 0);

        /**
         * @brief pause: Stop decode, process frame. (camera part)
         * @param pause
         */
        Q_INVOKABLE void pauseDecoder(bool pause);

        /**
         * @brief goToPosition: Go to specific position of video in case source input decoder is
         *                      file video.
         * @param percent
         */
        Q_INVOKABLE void goToPosition(float percent);

        /**
         * @brief setSpeed: Set video playback speed.
         * @param speed
         */
        Q_INVOKABLE void setSpeed(float speed);

        /**
         * @brief getTime: Get time in case of decode file.
         * @param type: TOTAL or CURRENT
         * @return
         */
        Q_INVOKABLE qint64 getTime(QString type);

        /**
         * @brief setShare: Set sharing pcs video to UC module state.
         * @param enable
         */
        Q_INVOKABLE void setShare(bool enable) { Q_UNUSED(enable); }

        /**
         * @brief setDisplayWorker: Set display worker is existing on qml. It will be done by
         *                          FrameDisplayer service.
         * @param t_renderWorker
         */
        Q_INVOKABLE void setDisplayWorker(VideoRender* t_renderWorker);

    public Q_SLOTS:
        /**
         * @brief handleNewImageDecoded: This slot is connected with signal that emitted from
         *                               FrameDecoder module.
         * @param t_decodedFrameMeta
         */
        void handleNewImageDecoded(FrameMetaPackaged t_decodedFrameMeta);

        /**
         * @brief handleNewFrameProcessed: This slot is connected with signal that emitted from
         *                                 FrameProcessor service. The result will be used to
         *                                 display to qml interface, save and put to rtsp server.
         *
         * @param t_processedFrame
         */
        void handleNewFrameProcessed(FrameMetaPackaged t_processedFrame);

    Q_SIGNALS:
        void videoStartRunning();
        void decodeSourceIsSet(QString t_fileName);

    protected:

    private:
        FrameDecoder*       m_frameDecoder;
        FrameProcessor*     m_frameProcessor;
        FrameDisplayer*     m_frameDisplayer;
        FrameSaver*         m_frameSaver;
};
#endif // SERVICESMANAGER_H
