QT += qml quick multimedia network positioning sensors core gui serialport charts widgets
QT += dbus
CONFIG += c++11 no_keywords console
CONFIG += use_video_gpu

DEFINES += QT_DEPRECATED_WARNINGS
RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH += \
    $$PWD \
    $$PWD/qml

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

include(c-utils/Utils.pri)
HOME = $$system(echo $HOME)

# project build directories
DESTDIR = $$system(pwd/build)
OBJECTS_DIR = $$DESTDIR/objs

## FFMPEG
INCLUDEPATH += /usr/include/x86_64-linux-gnu/
DEPENDPATH += /usr/include/x86_64-linux-gnu/
LIBS +=  \
    -lavformat \
    -lavcodec \
    -lavutil \
    -lswscale \
    -lswresample

# GStreamer
unix:!macx: DEPENDPATH += /usr/local/include
unix:!macx: INCLUDEPATH += /usr/include/gstreamer-1.0
#unix:!macx: INCLUDEPATH += /usr/local/include/gstreamer-1.0
unix:!macx: INCLUDEPATH += /usr/lib/x86_64-linux-gnu/gstreamer-1.0/include
unix:!macx: INCLUDEPATH += /usr/include/glib-2.0
unix:!macx: INCLUDEPATH += /usr/lib/x86_64-linux-gnu/glib-2.0/include

unix:!macx: LIBS += -LD:\usr\lib\x86_64-linux-gnu\
    -lglib-2.0 \
    -lgstreamer-1.0 \
    -lgstapp-1.0 \
    -lgstrtsp-1.0 \
    -lgstrtspserver-1.0 \
    -lgobject-2.0 \
    -lgstvideo-1.0 \
    -lgstaudio-1.0 \
    -lgstpbutils-1.0
unix:!macx: INCLUDEPATH += /usr/local/include
unix:!macx: DEPENDPATH += /usr/local/include

HEADERS += \
    src/GPUBased/OD/yolo_v3_class.hpp \
    src/GPUBased/services/FrameDecoder.h \
    src/GPUBased/services/FrameDisplayer.h \
    src/GPUBased/services/FrameProcessor.h \
    src/GPUBased/services/FrameSaver.h \
    src/GPUBased/services/helper/FrameMetaPackaged.h \
    src/GPUBased/services/helper/GpuMemoryBuffer.h \
    src/GPUBased/services/helper/OdLabelHelper.h \
    src/GPUBased/ServicesManager.h \
    src/GPUBased/services/helper/display/I420Render.h \
    src/GPUBased/services/helper/display/ImageItem.h \
    src/GPUBased/services/helper/display/VideoRender.h

SOURCES += \
    main.cpp \
    src/GPUBased/ServicesManager.cpp \
    src/GPUBased/services/FrameProcessor.cpp \
    src/GPUBased/services/FrameDisplayer.cpp \
    src/GPUBased/services/FrameSaver.cpp \
    src/GPUBased/services/helper/OdLabelHelper.cpp \
    src/GPUBased/services/helper/display/I420Render.cpp \
    src/GPUBased/services/helper/display/ImageItem.cpp \
    src/GPUBased/services/helper/display/VideoRender.cpp \
    src/GPUBased/services/FrameDecoder.cpp

use_video_gpu {
    DEFINES += USE_VIDEO_GPU
    # project build directories
    DESTDIR     = $$system(pwd)/build
    OBJECTS_DIR = $$DESTDIR
    # C++ flags
    QMAKE_CXXFLAGS_RELEASE =-O3
    # Cuda sources
    CUDA_SOURCES += src/GPUBased/cuda-helper/ipcuda_image.cu

    # Path to cuda toolkit install
    CUDA_DIR      = /usr/local/cuda-10.1
    # Path to header and libs files
    INCLUDEPATH  += $$CUDA_DIR/include
    QMAKE_LIBDIR += $$CUDA_DIR/lib64     # Note I'm using a 64 bits Operating system
    # libs used in your code
    LIBS += -lcudart -lcuda -lpthread
    # GPU architecture
    CUDA_ARCH     = sm_61                # Yeah! I've a new device. Adjust with your compute capability
    # Here are some NVCC flags I've always used by default.
    NVCCFLAGS     = --compiler-options -fno-strict-aliasing -use_fast_math --ptxas-options=-v


    # Prepare the extra compiler configuration (taken from the nvidia forum - i'm not an expert in this part)
    CUDA_INC = $$join(INCLUDEPATH,' -I','-I',' ')

    cuda.commands = $$CUDA_DIR/bin/nvcc -m64 -O3 -arch=$$CUDA_ARCH -c $$NVCCFLAGS \
                    $$CUDA_INC $$LIBS  ${QMAKE_FILE_NAME} -o ${QMAKE_FILE_OUT} \
                    2>&1 | sed -r \"s/\\(([0-9]+)\\)/:\\1/g\" 1>&2
    # nvcc error printout format ever so slightly different from gcc
    # http://forums.nvidia.com/index.php?showtopic=171651

    cuda.dependency_type = TYPE_C # there was a typo here. Thanks workmate!
    cuda.depend_command = $$CUDA_DIR/bin/nvcc -O3 -M $$CUDA_INC $$NVCCFLAGS ${QMAKE_FILE_NAME} | sed \"s/^.*: //\"

    cuda.input = CUDA_SOURCES
    cuda.output = ${OBJECTS_DIR}${QMAKE_FILE_BASE}_cuda.o
    # Tell Qt that we want add more stuff to the Makefile
    QMAKE_EXTRA_COMPILERS += cuda

    INCLUDEPATH += /usr/local/cuda-10.1/include
    INCLUDEPATH += /usr/local/cuda-10.1/targets/x86_64-linux/include

    # TensorFlow r1.14
    #include(tensorflow_dependency.pri)

    INCLUDEPATH += $$HOME/install/tensorflow/tensorflow-1.14.0
    INCLUDEPATH += $$HOME/install/tensorflow/tensorflow-1.14.0/tensorflow
    INCLUDEPATH += $$HOME/install/tensorflow/tensorflow-1.14.0/bazel-tensorflow-1.14.0/external/eigen_archive
    INCLUDEPATH += $$HOME/install/tensorflow/tensorflow-1.14.0/bazel-tensorflow-1.14.0/external/protobuf_archive/src
    INCLUDEPATH += $$HOME/install/tensorflow/tensorflow-1.14.0/bazel-genfiles

    LIBS += -L$$HOME/install/tensorflow/tensorflow-1.14.0/bazel-bin/tensorflow \
            -ltensorflow_cc \
            -ltensorflow_framework

    LIBS += `pkg-config --libs opencv`
    # End TensorFlow
    LIBS += -L/usr/local/lib -ldatdarknet
    #LIBS += /usr/local/lib/libdatdarknet.so
    message($$LIBS)

    DEFINES += GPU
    DEFINES += OPENCV
    DEFINES += DAT
    # lib zbar
    CONFIG+=link_pkgconfig
    PKGCONFIG+=zbar

    DISTFILES += \
        src/Camera/GPUBased/od/yolo-setup/yolov3-tiny_3l_last.weights \
        src/Camera/GPUBased/od/yolo-setup/yolov3-tiny_best.weights \
        src/Camera/GPUBased/od/libdatdarknet.so \
        src/Camera/GPUBased/od/yolo-setup/visdrone2019.names \
        src/Camera/GPUBased/od/yolo-setup/yolov3-tiny.cfg \
        src/Camera/GPUBased/od/yolo-setup/yolov3-tiny_3l.cfg
}
