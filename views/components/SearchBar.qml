/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component: Search Bar
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 27/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//---------------- Include custom libs ----------------------------------------
import "../components"
import "../partials"
import UIConstants 1.0


Item {
    TextField {
        id: searchField
        anchors.fill: parent
        color: UIConstants.textFooterColor
        font.pixelSize: 13
        placeholderText: qsTr("Gõ tên tệp (Định dạng: Y-M-D-H-M-S)")
        background: Rectangle {
            anchors.fill: parent
            color: searchField.enabled ? "transparent" : UIConstants.textFooterColor
            RectBorder {
                type: "bottom"
                thick: 3
            }
            Text {
                id: iconSearch
                text: UIConstants.iSearch
                anchors { right: parent.right; rightMargin: 15; verticalCenter: parent.verticalCenter }
                font.pixelSize: 15
                color: UIConstants.sidebarBorderColor
            }
        }
        Keys.onReleased: {
            if( searchField.text.length == 0 )
            {
                iconSearch.color = UIConstants.sidebarBorderColor;
            }else {
                iconSearch.color = UIConstants.textFooterColor
            }
        }
    }
}
