/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Module: UIConstants
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 13/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

pragma Singleton
import QtQuick 2.0

QtObject {
    //------------------ Notification type messages color --------------
    readonly property color success:               "#1dd1a1"
    readonly property color error:                 "#ff7675"
    readonly property color warning:               "#e67e22"
    readonly property color info:                  "#0984e3"

    //------------------ Icons notification -------------------------
    readonly property string iChecked:              "\uf00c"
    readonly property string iInfoCircle:           "\uf05a"
    readonly property string iLoading:              "\uf110"
    readonly property string iArrow:                "\uf124"
    readonly property string iError:                "\uf3ed"
    readonly property string iWarning:              "\uf071"
    readonly property string iInfo:                 "\uf05a"
    readonly property string iSuccess:              "\uf058"
    //readonly property string iError:                "\uf3ed"

    //------------------- Nav / Categrory Icons --------------------
    readonly property string iHomepage:             "\uf015"
    readonly property string iLiveView:             "\uf03d"
    readonly property string iPlayback:             "\uf144"
    readonly property string iDeviceMgmt:           "\uf109"
    readonly property string iGeneralConfigs:       "\uf085"

    //------------------- Sidebar Icons ---------------------------
    readonly property string iList:                 "\uf0ca"
    readonly property string iDevice:               "\uf109"
    readonly property string iCaretDown:            "\uf0d7"
    readonly property string iCaretLeft:            "\uf0d9"
    readonly property string iCaretRight:           "\uf0da"
    readonly property string iLeftHand:             "\uf0a5"
    readonly property string iRightHand:            "\uf0a4"
    readonly property string iSearch:               "\uf00e"
    readonly property string iSignal:               "\uf012"
    readonly property string iEdit:                 "\uf044"


    //------------------- Live View icons -------------------------
    readonly property string iSetting:              "\uf013"
    readonly property string iUpload:               "\uf093"
    readonly property string iVoiceControl:         "\uf028"
    readonly property string iX:                    "\uf00d"
    readonly property string iRocketWarning:        "\uf071"
    readonly property string iBandwidth:            "\uf362"
    readonly property string iResolution:           "\uf26c"
    readonly property string iWindowGrid:           "\uf17a"
    readonly property string iRedo:                 "\uf01e"
    readonly property string iRestart:              "\uf011"
    readonly property string iReturnScreen:         "\uf78c"
    readonly property string iOpenFullScreen:       "\uf066"

    //------------------- Playback icons -------------------------
    readonly property string iFiles:                "\uf0c5"
    readonly property string iDoubleLeft:           "\uf100"
    readonly property string iDoubleRight:          "\uf101"
    readonly property string iPlayState:            "\uf7a5"
    readonly property string iStopState:            "\uf04b"
    readonly property string iChoosedRight:         "\uf061"
    readonly property string iChevronLeft:          "\uf053"
    readonly property string iChevronRight:         "\uf054"
    readonly property string iArrowLeft:            "\uf060"
    readonly property string iArrowRight:           "\uf061"
    readonly property string iExport:               "\uf56e"
    readonly property string iFileDownload:         "\uf019"

    //-------------------General configs icons ------------------
    readonly property string iStorage:              "\uf0a0"
    readonly property string iDetectParams:          "\uf11c"
    readonly property string iSequenceTasks:         "\uf0ae"
    readonly property string iUsers:                 "\uf0c0"

    //------------------- Device Mgmt Icons ----------------------
    readonly property string iSave:                 "\uf0c7"

    //------------------ Usual Color -------------------------------
    //-------Text
    readonly property color textColor:             "#fff"
    readonly property color textFooterColor:       "#b2bec3"
    readonly property color cateCirTextColor:      "#778ca3"
    readonly property color cateDescTextColor:     "#CAD3C8"
    readonly property color textBlueColor:         "#0a3d62"
    readonly property color textSidebarColor:      "#ADACAA"
    readonly property color cfWarningColor:        "#ffd32a"
    readonly property color tableHeaderColor:      "#3c6382"
    readonly property color blackColor:            "#2f3640"
    readonly property color textSideActive:        "#23B99A"
    readonly property color iConOff:               "#576574"

    //------- Button
    readonly property color btnCateColor:          "#CAD3C8"
    //------- Shadow
    readonly property color dropshadowColor:       "#57606f"
    readonly property color headerTxtShadowColor:  "#3c6382"
    //------- Border
    readonly property color sidebarBorderColor:    "#636e72"
    readonly property color borderCircleBtnColor:  "#718093"
    readonly property color sidebarHeaderBorderColor: "#c8d6e5"
    readonly property color liveViewGridHighlight:  "#487eb0"
    readonly property color borderGreen:            "#0984e3"
    //------- Background
    readonly property color bgAppColor:            "#34495e"
    readonly property color bgColorOverlay:        "#222f3e"
    readonly property color sidebarBgColor:        "#34495e"
    readonly property color flatButtonColor:       "#3c6382"
    readonly property color categoryEleBgColor:    "#4b6584"
    readonly property color categoryCirColor:      "#4b6584"
    readonly property color cateOverlayBg:         "#808e9b"
    readonly property color sidebarBgItemLevel1:   "#2B3E52"
    readonly property color sidebarBgItemLevel2:   "#1D2833"
    readonly property color cfProcessingOverlayBg: "#2d3436"
    readonly property color sidebarConfigBg:       "#2D3C4B"
    readonly property color sidebarActiveBg:       "#283441"
    //------- Button
    readonly property color savingBtn:             "#487eb0"
    //------- Others
    readonly property color grayColor:             "#dfe4ea"
    readonly property color activeNav:             "#222f3e"
    readonly property color transparentColor:      "transparent"

    //------------------ Fonts -----------------------------------
    readonly property string fontComicSans:         "Comic Sans MS"
    readonly property string fontMonospace:         "Monospace"
    readonly property string fontNunito:            "Nunito, sans-serif;"
    readonly property string customFont:            fontNunito
}
