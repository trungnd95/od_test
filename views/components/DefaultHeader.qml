/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component: Custom Table
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 04/03/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

import QtQuick 2.0
import QtQuick.Controls 2.0
import UIConstants 1.0

Label {
    property DataColumn column
    opacity: 1
    text: column.label || column.field
    color: UIConstants.tableHeaderColor
    font.bold: true
}
