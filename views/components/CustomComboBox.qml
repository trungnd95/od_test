/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Module: Playback Page View
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 13/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//---------------- Include custom libs ----------------------------------------
import UIConstants 1.0

Item {
    id: rootItem

    //--- Properties
    property alias model_: control.model
    property alias background: backGround.color
    property alias popupBackground: popupBackground.color
    property alias textColor_: textColor.color
    property alias labelTextColor: titleText.color
    property alias titleTxt: titleText.text
    property alias icon: titleIcon.text
    property alias activeIndex: control.currentIndex

    //--- Signals
    signal activeValueChanged(real activeValueIndex);

    //--- Title
    Rectangle {
        id: title
        width: parent.width
        height: parent.height / 3
        color: UIConstants.transparentColor
        visible: icon ? true : false
        Text {
            id: titleIcon
            visible: true
            anchors.verticalCenter: parent.verticalCenter
            font { pixelSize: 18; bold: true; family: "FontAwesome" }
            color: titleText.color
            opacity: .7
        }

        Text {
            id: titleText
            text: "Form element title"
            anchors { left: titleIcon.right; verticalCenter: parent.verticalCenter; leftMargin: 20 }
            font { family: UIConstants.customFont; pixelSize: 13; bold: true }
            color: textColor_
            opacity: .7
        }
    }

    //--- Combo Box content
    ComboBox {
        id: control
        anchors { top: icon ? title.bottom : parent.top; left: parent.left;
                right: parent.right; bottom: parent.bottom;
        }
        model: ["HD", "VGA", "Mini-VGA"]
        delegate: ItemDelegate {
            width: control.width
            contentItem: Text {
                text: modelData
                color: textColor_
                font.family: UIConstants.customFont
                verticalAlignment: Text.AlignVCenter
                opacity: .4
                font.pixelSize: 13
            }
        }

        indicator: Canvas {
            id: canvas
            x: control.width - width - 3
            y: control.topPadding + (control.availableHeight - height) / 2
            width: 8
            height: 5
            contextType: "2d"

            Connections {
                target: control
                onPressedChanged: canvas.requestPaint()
            }

            onPaint: {
                context.reset();
                context.moveTo(0, 0);
                context.lineTo(width, 0);
                context.lineTo(width / 2, height);
                context.closePath();
                context.fillStyle = UIConstants.textFooterColor;
                context.fill();
            }
        }
        contentItem: Text {
            id: textColor
            leftPadding: 10
            text: control.displayText
            font.family: UIConstants.customFont
            color: UIConstants.textFooterColor
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 13
            //opacity: .6
        }
        background: Rectangle {
            id: backGround
            anchors.fill: parent
            color: UIConstants.transparentColor
            border { width: 1; color: UIConstants.dropshadowColor }
            radius: 2
            //opacity: .3
        }
        popup: Popup {
            y: control.height - 1
            //x: -7
            width: control.width
            implicitHeight: contentItem.implicitHeight
            padding: 5
            contentItem: ListView {
                clip: true
                implicitHeight: contentHeight
                width: control.width
                model: control.popup.visible ? control.delegateModel : null
                currentIndex: control.highlightedIndex
                ScrollIndicator.vertical: ScrollIndicator { }
            }
            background: Rectangle {
                id: popupBackground
                color: UIConstants.transparentColor
                border { color: UIConstants.sidebarBorderColor; width: 1 }
                radius: 1
                opacity: .5
            }
        }

        currentIndex: 0
        onCurrentIndexChanged: {
            rootItem.activeValueChanged(currentIndex);
        }
    }
}
