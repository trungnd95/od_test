/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Partial:
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 13/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//---------------- Include custom libs ----------------------------------------
import UIConstants 1.0

Item {
    id: rootItem
    Rectangle {
        width: parent.width
        height: parent.height * 9 / 10
        anchors.top: parent.top
        color: UIConstants.cfProcessingOverlayBg
        opacity: .7
        Text {
            id: noData
            text: "\uf5b3"
            font{ pixelSize: 35; bold: true; family: UIConstants.customFont }
            anchors.centerIn: parent
            color: UIConstants.textFooterColor
        }

        Text {
            id: noDataTxt
            text: "Trang không tồn tại"
            anchors { top: noData.bottom; topMargin: 10; horizontalCenter: parent.horizontalCenter }
            font{ pixelSize: 14; family: UIConstants.customFont }
            color: UIConstants.textFooterColor
        }
    }
}
