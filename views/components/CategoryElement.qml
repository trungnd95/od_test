/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component: Category element in homepage navigator
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 16/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import UIConstants 1.0

//------------------ Component content ---------------------------------------
Rectangle {
    id: categoryEle
    //------------ Set element decoration
    width: parent.width / 5
    height: parent.height / 2
    radius: 10
    anchors.verticalCenter: parent.verticalCenter
    color: UIConstants.categoryEleBgColor
    opacity: 0
    clip: true
    signal choosed(real _id)

    //------------ Element property
    property alias iconTxt: iconCircle.text
    property alias cateTitle: titleTxt.text
    property alias cateDesc: cateDescTxt.text
    property int cateId: 0
    property int loadingDuration: 500

    //------------ Icon circle
    Rectangle {
        id: circleTop
        anchors.top: parent.top
        anchors.topMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width / 3
        height: parent.height / 4
        radius: width
        color: UIConstants.categoryCirColor
        Text {
            id: iconCircle
            text: UIConstants.iLiveView
            anchors.centerIn: parent
            font.family: UIConstants.customFont
            font.pixelSize: 30
            color: UIConstants.cateCirTextColor
        }
    }

    //------------ Title
    Rectangle {
        id: titleCate
        anchors { top: circleTop.bottom; left: parent.left; right: parent.right }
        anchors.topMargin: 20
        height: parent.height / 8
        color: "transparent"
        Text {
            id: titleTxt
            text: "Title"
            anchors.centerIn: parent
            font.bold: true
            font.capitalization: Font.AllUppercase
            font.pixelSize: 18
            font.family: UIConstants.customFont
            color: UIConstants.textFooterColor
        }
        RectBorder {
            anchors.fill: parent
            type: "top"
            color: UIConstants.sidebarBorderColor
            thick: 2
        }

    }

    //------------ Description for category
    Rectangle {
        id: cateDesc
        anchors { top: titleCate.bottom; left: parent.left; right: parent.right; bottom: parent.bottom }
        anchors.topMargin: 10
        color: "transparent"
        Text {
            id: cateDescTxt
            anchors { fill: parent; left: parent.left; top: parent.top; leftMargin: 20; topMargin: 30 }
            font { pixelSize: 13; family: UIConstants.customFont }
            text: "Description"
            wrapMode: Text.Wrap
            color: UIConstants.grayColor
            opacity: .5
        }
        RectBorder {
            type: "top"
            color: UIConstants.sidebarBorderColor
            thick: 1
        }
    }
    //------------ Gradient
    gradient: Gradient {
        GradientStop { position: 0.0; color: UIConstants.dropshadowColor }
        GradientStop { position: 0.8; color: UIConstants.categoryEleBgColor }
        GradientStop { position: 1.0; color: UIConstants.categoryEleBgColor }
    }

    //------------ Hover effect
    Rectangle {
        id: overlayHoverEffect
        width: cateDesc.width
        height: cateDesc.height
        anchors.top: cateDesc.top
        anchors.topMargin: cateDesc.height
        color: UIConstants.cateOverlayBg
        opacity: .7
        z: 10
        FlatButton {
            anchors { bottom: parent.bottom; right: parent.right; bottomMargin: 20; rightMargin: 20;
                      horizontalCenter: parent.horizontalCenter }
            width: 100
            height:40
            btnText: "Truy cập"
            onClicked: {
                categoryEle.choosed(cateId);
            }
        }
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            enabled: overlayHoverEffect.enabled
            cursorShape: Qt.OpenHandCursor
            onEntered: { categoryEle.state = "hoverIn" }
            onExited: { categoryEle.state = "hoverOut" }
        }
    }

    //------------ Create animations effect
    states: [
        State {
            name: "hoverIn"
        },
        State {
            name: "hoverOut"
        }
    ]
    transitions:[
        Transition {
            from: "*"
            to: "hoverIn"
            NumberAnimation {
                target: overlayHoverEffect
                properties: "anchors.topMargin"
                to: 0
                duration: 300
                easing.type: Easing.Linear
            }
        },
        Transition {
            from: "hoverIn"
            to: "hoverOut"
            NumberAnimation {
                target: overlayHoverEffect
                properties: "anchors.topMargin"
                to: cateDesc.height
                duration: 250
                easing.type: Easing.InOutCubic
            }
        }
    ]

    //------------- MouseArea and event
    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        enabled: categoryEle.enabled
        cursorShape: Qt.OpenHandCursor
        onEntered: { categoryEle.state = "hoverIn" }
        onExited: { categoryEle.state = "hoverOut" }
    }

    //------------ Element Lazy loading
    NumberAnimation on opacity {
        to: 1
        duration: categoryEle.loadingDuration
        running: true
        easing.type: Easing.InExpo
    }
}
