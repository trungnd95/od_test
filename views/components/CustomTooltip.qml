/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component: Ip Address Form
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 02/03/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import UIConstants 1.0

ToolTip {
    id: control

    //--- Properties
    property alias contentText: control.text
    property alias iCon: icon.text
    property alias iColor: icon.color
    //--- Signals
    signal stopCheck()

    Text {
        id: icon
        text: UIConstants.iError
        font.pointSize: 18
        font.family: "FontAwesome"
        anchors.left: parent.left
        anchors.leftMargin: 10
        color: UIConstants.cfWarningColor
        anchors.verticalCenter: parent.verticalCenter
    }

    text: qsTr("Custom text.!")

    contentItem: Text {
        text: control.text
        font: control.font
        font.family: UIConstants.customFont
        color: UIConstants.textFooterColor
        leftPadding: 40
    }

    background: Rectangle {
        color: UIConstants.cfProcessingOverlayBg
        opacity: .6
    }
    Text {
        id: close
        text: "\uf057"
        font.pointSize: 18
        font.family: "FontAwesome"
        color: UIConstants.sidebarBorderColor
        anchors { top: parent.top; right: parent.right; topMargin: -15; rightMargin: -13 }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                control.close();
                control.stopCheck();
            }
        }
    }
}

