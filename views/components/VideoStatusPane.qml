/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component: Video Status Pane
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 13/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//---------------- Include custom libs ----------------------------------------
import UIConstants 1.0

Item {
    id: root
    property alias paneTitle: paneTitle.text
    property alias bandWidth: bandwdith.text
    property bool streamingRunning: true
    property int activeStreamingResol: -1

    //--- Signals
    signal removeStream();
    signal remoteRestartServer();
    signal toggleRunPauseStream(bool runningState);
    signal changeResol(int resol);

    anchors { top: parent.top; left: parent.left; right: parent.right }
    height: parent.height / 10
    opacity: .5


    //--- Left signals pane
    RowLayout  {
        id: leftSignalsPane
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width / 2
        spacing: 5
        // --------- Resolution
        ComboBox {
            id: control
            anchors { left: parent.left; leftMargin: 10; top: parent.top; bottom: parent.bottom }
            property var listResols: ["HD", "VGA", "Mini-VGA"]
            currentIndex: activeStreamingResol
            model: listResols
            delegate: ItemDelegate {
                width: control.width
                contentItem: Text {
                    text: modelData
                    color: UIConstants.grayColor
                    font.family: UIConstants.customFont
                    verticalAlignment: Text.AlignVCenter
                    opacity: .4
                    font.pixelSize: 13
                }
            }
            indicator: Text {
                text: UIConstants.iSetting
                anchors.verticalCenter: parent.verticalCenter
                color: UIConstants.grayColor
                font.pixelSize: 13
                font.family: "FontAwesome"
            }
            contentItem: Text {
                leftPadding: 20
                text: control.displayText
                font.family: UIConstants.customFont
                color: UIConstants.grayColor
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 13
            }
            background: Rectangle {
                anchors.fill: parent
                color: UIConstants.transparentColor
            }
            popup: Popup {
                y: control.height - 1
                x: -7
                width: control.width
                implicitHeight: contentItem.implicitHeight
                padding: 5
                contentItem: ListView {
                    clip: true
                    implicitHeight: contentHeight
                    width: control.width
                    model: control.popup.visible ? control.delegateModel : null
                    currentIndex: control.highlightedIndex
                    ScrollIndicator.vertical: ScrollIndicator { }
                }
                background: Rectangle {
                    color: UIConstants.transparentColor
                    border { color: UIConstants.sidebarBorderColor; width: 1 }
                    radius: 1
                    opacity: .5
                }
            }
            onCurrentIndexChanged: {
                root.changeResol(currentIndex + 2);
            }
        }

        //--------- Bandwidth
        Rectangle {
            id: bandwidthStatus
            anchors { left: control.right; top: parent.top; bottom: parent.bottom }
            implicitWidth: childrenRect.width
            color: UIConstants.transparentColor
            Text {
                id: iconDataExchange
                text: UIConstants.iUpload
                anchors.verticalCenter: parent.verticalCenter
                color: UIConstants.grayColor
                font.pixelSize: 13
                font.family: UIConstants.customFont
            }

            Text {
                id: bandwdith
                anchors { left: iconDataExchange.right; leftMargin: 5; verticalCenter: parent.verticalCenter }
                color: UIConstants.grayColor
                text: "512 Kb/s"
                font.pixelSize: 13
                font.family: UIConstants.customFont
            }
        }

        //---------- Connection status
        //Rectangle {
        //    anchors { left: bandwidthStatus.right; leftMargin: 10; top: parent.top; bottom: parent.bottom }
        //    color: UIConstants.transparentColor
        //    implicitWidth: connectionIcon.width
        //    Text {
        //        id: connectionIcon
        //        anchors.centerIn: parent
        //        text: UIConstants.iSignal
        //        font.family: "FontAwesome"
        //        font.pixelSize: 13
        //        color: UIConstants.grayColor
        //       Canvas {
        //            anchors.fill: parent
        //            onPaint: {
        //                var ctx = getContext("2d");
        //                ctx.strokeStyle = UIConstants.grayColor;
        //                ctx.lineWidth = 2;
        //                ctx.beginPath();
        //                ctx.moveTo(0, 5);
        //                ctx.lineTo(width, height - 2);
        //                ctx.closePath();
        //                ctx.stroke();
        //           }
        //        }
        //    }
        //}
    }

    //--- Center title
    Rectangle {
        id: paneTitleWrapper
        width: parent.width / 4
        height: parent.height
        anchors.left: leftSignalsPane.right
        color: UIConstants.transparentColor
        Text {
            id: paneTitle
            anchors.verticalCenter: parent.verticalCenter
            //anchors.horizontalCenter: parent.horizontalCenter
            text: ""
            color: UIConstants.grayColor
            font.pixelSize: 14
            font.family: UIConstants.customFont
            font.bold: true
        }
    }

    //--- Right control pane
    Row {
        anchors { verticalCenter: parent.verticalCenter; left: paneTitleWrapper.right; right: parent.right; rightMargin: parent.width / 15 }
        width: parent.width / 4
        layoutDirection: Qt.RightToLeft
        spacing: 8
        opacity: .5

        Text {
            id: iconRemoveFromList
            text: UIConstants.iX
            font { pixelSize: 13; family: "FontAwesome" }
            color: UIConstants.grayColor
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    root.removeStream();
                }
            }
        }

        Text {
            id: iconRemoteReset
            text: UIConstants.iRestart
            font { pixelSize: 13; family: "FontAwesome" }
            color: UIConstants.grayColor
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    root.remoteRestartServer();
                }
            }
        }

        Text {
            id: iconStopStream
            text: root.streamingRunning ? UIConstants.iPlayState : UIConstants.iStopState
            font { pixelSize: 11; family: "FontAwesome" }
            color: UIConstants.grayColor

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    root.streamingRunning = !root.streamingRunning;
                    console.log(root.streamingRunning);
                    root.toggleRunPauseStream(root.streamingRunning);
                }
            }
        }

    }

    //---------- Background
    Rectangle {
        anchors.fill: parent
        color: UIConstants.bgColorOverlay
        z: -2
    }
}
