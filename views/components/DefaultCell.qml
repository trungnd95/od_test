/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component: Custom Table
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 04/03/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

import QtQuick 2.0
import QtQuick.Controls 2.0
import UIConstants 1.0

Label {
    property DataColumn column
    property var row
    property var context
    property var value
    opacity: 0.4
    //anchors.verticalCenter: parent.verticalCenter
    //anchors.horizontalCenter: parent.horizontalCenter
    text: column.format ? column.format(value, row) : value
    color: "#b2bec3"
}

