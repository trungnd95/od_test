/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component: Device Edit Modal
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 05/03/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//----------------- Include Custom views --------------------------------------
import UIConstants 1.0

Item {
    id: rootItem

    //--- Properties
    property var dynamicValidator
    property alias icon: titleIcon.text
    property alias titleTxt: titleText.text
    property alias inputColor: textField.color
    property alias labelColor: titleText.color
    property alias textFieldPlaceholder: textField.placeholderText
    property alias background: textFieldBackground.color

    //--- Title
    Rectangle {
        id: title
        width: parent.width
        height: parent.height / 3
        color: UIConstants.transparentColor
        Text {
            id: titleIcon
            visible: true
            text: UIConstants.iError
            anchors.verticalCenter: parent.verticalCenter
            font{ pixelSize: 18; bold: true }
            color: titleText.color
            opacity: .7
        }

        Text {
            id: titleText
            text: "Form element title"
            anchors { left: titleIcon.right; verticalCenter: parent.verticalCenter; leftMargin: 20 }
            font { family: UIConstants.customFont; pixelSize: 13; bold: true }
            color: inputColor
            opacity: .7
        }
    }
    //--- Input Field
    TextField {
        id: textField
        anchors { top: title.bottom; left: parent.left; right: parent.right;
                bottom: parent.bottom; topMargin: 10 }
        color: UIConstants.textFooterColor
        opacity: .6
        placeholderText: qsTr("20")
        background: Rectangle {
            id: textFieldBackground
            anchors.fill: parent
            color: UIConstants.transparentColor
            border.color: UIConstants.sidebarBorderColor
        }
        validator: dynamicValidator
//        onPressed: {
//            textFieldBackground.border.color = UIConstants.borderGreen;
//        }
//        onEditingFinished: {
//            textFieldBackground.border.color = UIConstants.sidebarBorderColor;
//        }
    }
}
