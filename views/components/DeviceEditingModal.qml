/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component: Device Edit Modal
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 05/03/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//----------------- Include Custom views --------------------------------------
import UIConstants 1.0

//-----------------------------------------------------------------------------
Item {
    id: rootItem

    //----------- Modals content
    Rectangle {
        id: modalContent
        anchors.fill: parent
        color: UIConstants.cfProcessingOverlayBg
        opacity: .7

        //--- Modal wrapper
        Rectangle {
            id: modalWrapper
            anchors{ top: parent.top; topMargin: - parent.height; horizontalCenter: parent.horizontalCenter }
            width: parent.width / 2.5
            //------ Tabbar
            TabBar {
                id: bar
                width: parent.width
                Repeater {
                    model: ["Thiết bị", "Lưu trữ", "Streaming & kết nối", "Hình ảnh"]

                    TabButton {
                        text: modelData
                        width: bar.width / 4
                    }
                }
            }

            //------ Tabbar content
            StackLayout {
                id: stackForms
                anchors{ top :bar.bottom; left: parent.left; right: parent.right }
                currentIndex: bar.currentIndex
                //--- First tab content
                Item {
                    id: firstTab
                    width: parent.width
                    implicitHeight: childrenRect.height
                    Rectangle {
                        id: firstTabChild
                        width: parent.width
                        height: childrenRect.height + 30 + childrenRect.y
                        //--- IP address
                        CustomTextField {
                            id: textField1
                            y: 50
                            x: 10
                            width: parent.width - 30
                            height: 60
                            dynamicValidator: ipAddressValidate
                            titleTxt: "IP address"
                            inputColor: UIConstants.blackColor
                            textFieldPlaceholder: "192.168.1.123"
                            anchors.horizontalCenter: parent.horizontalCenter
                            icon: "\uf6ff"
                        }
                        //--- Subnet Mask
                        CustomTextField {
                            id: textField2
                            anchors{ top: textField1.bottom; topMargin: 20 }
                            x: 10
                            width: parent.width - 30
                            height: 60
                            dynamicValidator: ipAddressValidate
                            titleTxt: "Subnet Mask"
                            inputColor: UIConstants.blackColor
                            textFieldPlaceholder: "255.255.255.255"
                            anchors.horizontalCenter: parent.horizontalCenter
                            icon: "\uf6ff"
                        }
                        //--- Gateway
                        CustomTextField {
                            id: textField3
                            anchors{ top: textField2.bottom; topMargin: 20 }
                            x: 10
                            width: parent.width - 30
                            height: 60
                            dynamicValidator: ipAddressValidate
                            titleTxt: "Gate way"
                            inputColor: UIConstants.blackColor
                            textFieldPlaceholder: "192.168.1.1"
                            anchors.horizontalCenter: parent.horizontalCenter
                            icon: "\uf6ff"
                        }
                    }
                }

                //--- Second tab
                Item {
                    id: secondTab
                    width: parent.width
                    implicitHeight: childrenRect.height
                    Rectangle {
                        width: parent.width
                        height: 30
                        Text {
                            text: "Cấu hình lưu trữ"
                            font { family: UIConstants.customFont; pixelSize: 12; italic: true }
                        }
                    }
                }

                //--- Third tab
                Item {
                    id: thirdTab
                    width: parent.width
                    implicitHeight: childrenRect.height
                    Rectangle {
                        id: thirdTabChild
                        width: parent.width
                        implicitHeight: childrenRect.height + 30 + childrenRect.y
                        GridLayout {
                            width: parent.width - 30
                            height: childrenRect.height
                            anchors.horizontalCenter: parent.horizontalCenter
                            y: 50
                            rows: 2
                            columns: 3
                            rowSpacing: 40

                            //--- Streaming Host
                            CustomTextField {
                                id: streamingHost
                                Layout.rowSpan: 1
                                Layout.columnSpan: 2
                                Layout.preferredHeight: 60
                                Layout.preferredWidth: parent.width * 2 / 3
                                dynamicValidator: ipAddressValidate
                                titleTxt: "Địa chỉ streaming"
                                inputColor: UIConstants.blackColor
                                textFieldPlaceholder: "224.10.10.10"
                                icon: "\uf6ff"
                            }

                            //--- Streaming Port
                            CustomTextField {
                                id: streamingPort
                                Layout.rowSpan: 1
                                Layout.columnSpan: 1
                                Layout.preferredHeight: 60
                                Layout.preferredWidth: parent.width / 3
                                dynamicValidator: ipAddressValidate
                                titleTxt: "Cổng streaming"
                                inputColor: UIConstants.blackColor
                                textFieldPlaceholder: "10444"
                                icon: "\uf6ff"
                            }

                            //--- Fps
                            CustomComboBox {
                                Layout.rowSpan: 1
                                Layout.columnSpan: 1
                                Layout.preferredHeight: 60
                                Layout.preferredWidth: parent.width / 3 - 5
                                titleTxt: "Tốc độ hình"
                                textColor_: UIConstants.blackColor
                                icon: "\uf6ff"
                                popupBackground: UIConstants.grayColor
                            }

                            //--- Resolution
                            CustomComboBox {
                                Layout.rowSpan: 1
                                Layout.columnSpan: 1
                                Layout.preferredHeight: 60
                                Layout.preferredWidth: parent.width / 3 - 5
                                titleTxt: "Độ phân giải"
                                textColor_: UIConstants.blackColor
                                icon: "\uf6ff"
                                popupBackground: UIConstants.grayColor
                            }

                            //--- Bandwidth
                            CustomComboBox {
                                Layout.rowSpan: 1
                                Layout.columnSpan: 1
                                Layout.preferredHeight: 60
                                Layout.preferredWidth: parent.width / 3 - 5
                                titleTxt: "Băng thông"
                                textColor_: UIConstants.blackColor
                                icon: "\uf6ff"
                                popupBackground: UIConstants.grayColor
                            }
                        }
                    }
                }

                //--- Fouth tab
                Item {
                    id: fouthTab
                    width: parent.width
                    implicitHeight: childrenRect.height
                    Rectangle {
                        id: fouthTabChild
                        width: parent.width
                        implicitHeight: childrenRect.height + 30 + childrenRect.y
                        //--- Video Output
                        Rectangle {
                            id: videoOutput
                            width: parent.width - 30
                            height: 320
                            y: 40
                            color: UIConstants.cfProcessingOverlayBg
                            anchors.horizontalCenter: parent.horizontalCenter
                            Text {
                                id: noVideo
                                text: "\uf4e2"
                                font{ pixelSize: 35; bold: true; family: UIConstants.customFont }
                                anchors.centerIn: parent
                                color: UIConstants.textFooterColor
                            }
                        }
                        //--- Divider / Horizontal line
                        Rectangle {
                            anchors { top: videoOutput.bottom; topMargin: 20; }
                            height: 1;
                            width: parent.width;
                            color: UIConstants.sidebarBorderColor
                        }

                        //--- Image Adjusting zone
                        GridLayout {
                            id: imageParamsAdjusting
                            rows: 3
                            columns: 2
                            rowSpacing: 20
                            width: parent.width - 30
                            height: childrenRect.height
                            anchors{ horizontalCenter: parent.horizontalCenter;
                                     top:videoOutput.bottom; topMargin: 40 }
//                            CustomSlider {
//                                id: customSlider
//                                Layout.rowSpan: 1
//                                Layout.columnSpan: 1
//                                Layout.preferredWidth: parent.width / 2 - 5
//                                Layout.preferredHeight: 30
//                                height: 30
//                                textColor: UIConstants.blackColor
//                                titleTxt: "Tham số 1: "
//                                icon: "\uf6ff"
//                            }

//                            CustomSlider {
//                                id: customSlider_
//                                Layout.rowSpan: 1
//                                Layout.columnSpan: 1
//                                Layout.preferredWidth: parent.width / 2 - 5
//                                Layout.preferredHeight: 30
//                                height: 30
//                                textColor: UIConstants.blackColor
//                                titleTxt: "Tham số 2: "
//                                icon: "\uf6ff"
//                            }
                        }
                    }
                }

                onCurrentIndexChanged: {
                    switch( currentIndex )
                    {
                    case 0:
                        modalBottom.y = firstTab.implicitHeight + bar.height;
                        break;
                    case 1:
                        modalBottom.y = secondTab.implicitHeight + bar.height;
                        break;
                    case 2:
                        modalBottom.y = thirdTab.implicitHeight + bar.height;
                        break;
                    case 3:
                        modalBottom.y = fouthTab.implicitHeight + bar.height;
                        break;

                    }
                }
            }

            //------------- Tabbar Bottom
            Rectangle {
                id: modalBottom
                anchors { left: parent.left; right: parent.right }
                height: 70
                RectBorder {
                    thick: 3
                    color: UIConstants.flatButtonColor
                }
                RowLayout {
                    width: parent.width / 3
                    height: parent.height
                    spacing: 10
                    anchors { right: parent.right; rightMargin: 15 }
                    FlatButton {
                        Layout.preferredWidth: parent.width / 2 - 10
                        Layout.preferredHeight: parent.height / 2
                        anchors { verticalCenter: parent.verticalCenter }
                        btnText: "Lưu"
                        btnTextColor: UIConstants.textColor
                        btnBgColor: UIConstants.savingBtn
                        radius: 5
                        iconVisible: true
                        icon: UIConstants.iSave
                    }
                    FlatButton {
                        Layout.preferredWidth: parent.width / 2 - 10
                        Layout.preferredHeight: parent.height / 2
                        anchors { verticalCenter: parent.verticalCenter }
                        btnText: "Đóng"
                        btnTextColor: UIConstants.textColor
                        btnBgColor: UIConstants.borderGreen
                        radius: 5
                        iconVisible: true
                        onClicked: {
                            setModalDisappear();
                            rootItem.opacity = 0;
                        }
                    }
                }
            }

            Behavior on anchors.topMargin {
                NumberAnimation {
                    duration: 400
                    easing.type: Easing.InCubic
                }
            }
        }
    }

    //--------- Input form validator
    RegExpValidator {
        id: ipAddressValidate
        regExp: /^(([0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){0,3}([0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$/
    }

    //--------- Js functions

    function setModalAppear()
    {
        modalWrapper.anchors.topMargin = modalContent.height / 9;
        modalBottom.y = firstTab.implicitHeight + bar.height;
    }

    function setModalDisappear()
    {
        modalWrapper.anchors.topMargin = - modalContent.height
    }
}
