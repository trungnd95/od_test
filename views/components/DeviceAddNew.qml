/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component: Device Edit Modal
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 05/03/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//----------------- Include Custom views --------------------------------------
import UIConstants 1.0

//-----------------------------------------------------------------------------
Item {
    id: rootItem
    //----------- Background
    Rectangle {
        anchors.fill: parent
        color: UIConstants.cfProcessingOverlayBg
        opacity: .7
    }

    //----------- Modals content
    Rectangle {
        id: modalWrapper
        anchors{ top: parent.top; topMargin: - parent.height; horizontalCenter: parent.horizontalCenter }
        width: parent.width / 2.5
        height: children.height
        //------ Tabbar
//        TabBar {
//            id: bar
//            width: parent.width
//            Repeater {
//                model: ["Thiết bị", "Lưu trữ", "Streaming & kết nối", "Hình ảnh"]

//                TabButton {
//                    text: modelData
//                    width: bar.width / 4
//                }
//            }
//        }

//        //------ Tabbar content
//        Rectangle {
//            id: firstTabChild
//            width: parent.width
//            height: childrenRect.height + 30 + childrenRect.y
//            //--- IP address
//            CustomTextField {
//                id: textField1
//                y: 50
//                x: 10
//                width: parent.width - 30
//                height: 60
//                dynamicValidator: ipAddressValidate
//                titleTxt: "IP address"
//                inputColor: UIConstants.blackColor
//                textFieldPlaceholder: "192.168.1.123"
//                anchors.horizontalCenter: parent.horizontalCenter
//                icon: "\uf6ff"
//            }
//            //--- Subnet Mask
//            CustomTextField {
//                id: textField2
//                anchors{ top: textField1.bottom; topMargin: 20 }
//                x: 10
//                width: parent.width - 30
//                height: 60
//                dynamicValidator: ipAddressValidate
//                titleTxt: "Subnet Mask"
//                inputColor: UIConstants.blackColor
//                textFieldPlaceholder: "255.255.255.255"
//                anchors.horizontalCenter: parent.horizontalCenter
//                icon: "\uf6ff"
//            }
//            //--- Gateway
//            CustomTextField {
//                id: textField3
//                anchors{ top: textField2.bottom; topMargin: 20 }
//                x: 10
//                width: parent.width - 30
//                height: 60
//                dynamicValidator: ipAddressValidate
//                titleTxt: "Gate way"
//                inputColor: UIConstants.blackColor
//                textFieldPlaceholder: "192.168.1.1"
//                anchors.horizontalCenter: parent.horizontalCenter
//                icon: "\uf6ff"
//            }
//        }

//        //------------- Tabbar Bottom
//        Rectangle {
//            id: modalBottom
//            anchors { left: parent.left; right: parent.right }
//            height: 70
//            RectBorder {
//                thick: 3
//                color: UIConstants.flatButtonColor
//            }
//            RowLayout {
//                width: parent.width / 3
//                height: parent.height
//                spacing: 10
//                anchors { right: parent.right; rightMargin: 15 }
//                FlatButton {
//                    Layout.preferredWidth: parent.width / 2 - 10
//                    Layout.preferredHeight: parent.height / 2
//                    anchors { verticalCenter: parent.verticalCenter }
//                    btnText: "Lưu"
//                    btnTextColor: UIConstants.textColor
//                    btnBgColor: UIConstants.savingBtn
//                    radius: 5
//                    iconVisible: true
//                    icon: UIConstants.iSave
//                }
//                FlatButton {
//                    Layout.preferredWidth: parent.width / 2 - 10
//                    Layout.preferredHeight: parent.height / 2
//                    anchors { verticalCenter: parent.verticalCenter }
//                    btnText: "Đóng"
//                    btnTextColor: UIConstants.textColor
//                    btnBgColor: UIConstants.borderGreen
//                    radius: 5
//                    iconVisible: true
//                    onClicked: {
//                        setModalDisappear();
//                        rootItem.opacity = 0;
//                    }
//                }
//            }
//        }

        Column {
            //----- Modal header
            Rectangle {
                width: modalWrapper.width - 10
                height: rootItem.height / 16
                anchors.horizontalCenter: parent.horizontalCenter
                color: UIConstants.tableHeaderColor
                Row {
                    anchors.centerIn: parent
                    spacing: 10
                    Text {
                        text: "\uf055"
                        font.pixelSize: 20
                        font.family: "FontAwesome"
                        color: UIConstants.textFooterColor
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    Text {
                        text: "Thêm mới camera"
                        font.pixelSize: 18
                        font.family: UIConstants.customFont
                        color: UIConstants.textFooterColor
                        anchors.verticalCenter: parent.verticalCenter
                    }
                }
            }

            //----- Modal content
            Rectangle {
                id: modelContent
                width: modalWrapper.width
                height: childrenRect.height + 30 + childrenRect.y
                color: "#fff"

                //--- Camera Name
                CustomTextField {
                    id: textField1
                    x: 10
                    y: 20
                    width: parent.width - 30
                    height: 60
                    dynamicValidator: ipAddressValidate
                    titleTxt: "Tên camera"
                    inputColor: UIConstants.blackColor
                    textFieldPlaceholder: "Camera trước cửa."
                    anchors.horizontalCenter: parent.horizontalCenter
                    icon: "\uf6ff"
                }

                //--- IP address
                CustomTextField {
                    id: textField2
                    x: 10
                    anchors{ top: textField1.bottom; topMargin: 20 }
                    width: parent.width - 30
                    height: 60
                    dynamicValidator: ipAddressValidate
                    titleTxt: "Địa chỉ IP"
                    inputColor: UIConstants.blackColor
                    textFieldPlaceholder: "192.168.1.2"
                    anchors.horizontalCenter: parent.horizontalCenter
                    icon: "\uf6ff"
                }

                //--- Gateway
                CustomTextField {
                    id: textField3
                    anchors{ top: textField2.bottom; topMargin: 20 }
                    x: 10
                    width: parent.width - 30
                    height: 60
                    dynamicValidator: ipAddressValidate
                    titleTxt: "Đường truyền hình ảnh"
                    inputColor: UIConstants.blackColor
                    textFieldPlaceholder: "rtsp://127.0.0.1:xxxx/yyyy"
                    anchors.horizontalCenter: parent.horizontalCenter
                    icon: "\uf6ff"
                }
            }

            //----- Modal footer
            Rectangle {
                id: modalFooter
                width: modalWrapper.width
                height: rootItem.height / 16
                color: "#fff"
                RectBorder {
                    thick: 2
                    color: UIConstants.textFooterColor
                }
                RowLayout {
                    width: parent.width / 3
                    height: parent.height
                    spacing: 10
                    anchors { right: parent.right; rightMargin: 15 }
                    FlatButton {
                        Layout.preferredWidth: parent.width / 2 - 10
                        Layout.preferredHeight: parent.height / 2
                        anchors { verticalCenter: parent.verticalCenter }
                        btnText: "Lưu"
                        btnTextColor: UIConstants.textColor
                        btnBgColor: UIConstants.savingBtn
                        radius: 5
                        iconVisible: true
                        icon: UIConstants.iSave
                    }
                    FlatButton {
                        Layout.preferredWidth: parent.width / 2 - 10
                        Layout.preferredHeight: parent.height / 2
                        anchors { verticalCenter: parent.verticalCenter }
                        btnText: "Đóng"
                        btnTextColor: UIConstants.textColor
                        btnBgColor: UIConstants.borderGreen
                        radius: 5
                        iconVisible: true
                        onClicked: {
                            setModalDisappear();
                            rootItem.opacity = 0;
                        }
                    }
                }
            }
        }

        Behavior on anchors.topMargin {
            NumberAnimation {
                duration: 400
                easing.type: Easing.InCubic
            }
        }

    }


    //--------- Input form validator
    RegExpValidator {
        id: ipAddressValidate
        regExp: /^(([0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){0,3}([0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$/
    }

    //--------- Js functions

    function setModalAppear()
    {
        modalWrapper.anchors.topMargin = 100;
    }

    function setModalDisappear()
    {
        modalWrapper.anchors.topMargin = - modalContent.height
    }
}
