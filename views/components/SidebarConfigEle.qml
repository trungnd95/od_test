/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component:
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 19/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//---------------- Include custom libs ----------------------------------------
import UIConstants 1.0

//---------------- Component definition ----------------------------------------

Item {
    id: rootItem

    //--------- Properties
    property alias iconSide: iconSide.text
    property alias textSide: textSide.text
    property int eleId

    //--------- Signals
    signal activeSide(real eleId_)

    //--------- Element children
    Rectangle {
        id: sideEleWrapper
        anchors.fill: parent
        color: UIConstants.transparentColor
        RowLayout {
            opacity: 0.8
            anchors.fill: parent
            anchors.leftMargin: 20
            Text {
                id: iconSide
                text: UIConstants.iChecked
                font { pixelSize: 18 }
                color: textSide.color
                anchors.verticalCenter: parent.verticalCenter
            }
            Text {
                id: textSide
                text: "Lưu trữ"
                font {family: UIConstants.customFont; pixelSize: 13}
                color: UIConstants.textFooterColor
                anchors.verticalCenter: parent.verticalCenter
                anchors{ left: iconSide.right; leftMargin: 15 }
            }
            MouseArea {
                anchors.fill: parent
                enabled: true
                hoverEnabled: true
                onClicked: {
                    rootItem.activeSide(eleId);
                }
            }
        }
        RectBorder {
            thick: 1
            type: "bottom"
        }
    }

    //---- Js supported functions
    function setActive()
    {
        sideEleWrapper.color = UIConstants.sidebarActiveBg;
        textSide.color = UIConstants.textSideActive;
    }

    function setDeactive()
    {
        sideEleWrapper.color = UIConstants.transparentColor;
        textSide.color = UIConstants.textFooterColor;
    }
}
