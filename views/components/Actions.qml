/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Module: Device Mgmt view
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 04/03/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//----------------- Include Custom views --------------------------------------
import UIConstants 1.0
import "../components"
import "../partials"

Item {
    id: rootItem
    property var value
    property var row

    signal edited()
    signal deleted(real rowId)
    anchors.horizontalCenter: parent.horizontalCenter
    Text {
        id: text1
        text: value[0].text
        color: value[0].color
        font.pixelSize: 20
        font.family: UIConstants.customFont
        //anchors.verticalCenter: parent.verticalCenter
        MouseArea {
            anchors.fill: parent
            onClicked: {
                rootItem.edited();
                console.log(row.id_);
            }
        }
    }
    Text {
        id: text2
        anchors { left: text1.right; leftMargin: 10; /*verticalCenter: parent.verticalCenter*/ }
        text: value[1].text
        color: value[1].color
        font.pixelSize: 20
        font.family: UIConstants.customFont
        MouseArea {
            anchors.fill: parent
            onClicked: rootItem.deleted(row.id_);
        }
    }
}
