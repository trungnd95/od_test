/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component: List Data for control pane
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 27/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//-------------------- Include QT libs ---------------------------------------
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import UIConstants 1.0

//---------------------------------------------------------------------------
Item {
    id: rootItem
    visible: false

    property alias model_: listView.model
    property alias visible_: rootItem.visible
    property string type

    signal listViewClicked(string choosedItem)

    ListView {
        id: listView
        anchors.fill: parent
        model: ["HD", "VGA", "Mini-VGA"]
        delegate: Item {
            width: txt.width + 10
            height: 20
            Text {
                id: txt
                anchors.centerIn: parent
                text: modelData
                color: UIConstants.grayColor
                font { family: UIConstants.customFont; pixelSize: 12 }
            }
            MouseArea {
                anchors.fill: parent
                onClicked:  {
                    listView.currentIndex = index;
                    rootItem.listViewClicked(txt.text);
                }
            }
        }
        highlight: Rectangle { color: UIConstants.headerTxtShadowColor; radius: 5 }
        focus: true
    }
}
