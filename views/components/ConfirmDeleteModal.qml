/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component: Device Edit Modal
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 05/03/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//----------------- Include Custom views --------------------------------------
import UIConstants 1.0

//-----------------------------------------------------------------------------
Item {
    id: rootItem

    //----------- Properties
    property int rowId_
    //----------- Signals
    signal confirmDeleted( real rowId );


    //----------- Modals content
    Rectangle {
        id: modalContent
        anchors.fill: parent
        color: UIConstants.cfProcessingOverlayBg
        opacity: .8

        //--- Modal wrapper
        ColumnLayout {
            id: modalWrapper
            anchors{ top: parent.top; topMargin: - parent.height; horizontalCenter: parent.horizontalCenter }
            width: parent.width / 2.5
            //---Title
            Rectangle {
                id: titleWrapper
                Layout.preferredHeight:  60
                Layout.preferredWidth: parent.width
                Text {
                    id: titlteTxt
                    text: "Bạn có chắc chắn xóa? "
                    font{ family: UIConstants.customFont; pixelSize: 16; bold: true }
                    anchors { left: parent.left; leftMargin: 20; verticalCenter: parent.verticalCenter }
                }

                RectBorder {
                    type: "bottom"
                }
            }
            //--- Actions
            Rectangle {
                Layout.preferredWidth: parent.width
                Layout.preferredHeight: 100
                anchors.top: titleWrapper.bottom
                FlatButton {
                    id: deleteBtn
                    width: 100
                    height: parent.height / 2
                    anchors { verticalCenter: parent.verticalCenter;
                              right: cancelBtn.left; rightMargin: 10 }
                    btnText: "Xóa"
                    btnTextColor: UIConstants.textColor
                    btnBgColor: UIConstants.error
                    radius: 5
                    iconVisible: true
                    icon: UIConstants.iSave
                    onClicked: {
                         setModalDisappear();
                        rootItem.confirmDeleted(rowId_);
                    }
                }
                FlatButton {
                    id: cancelBtn
                    width: 100
                    height: parent.height / 2
                    anchors { verticalCenter: parent.verticalCenter;
                             right: parent.right; rightMargin: 20 }
                    btnText: "Hủy bỏ"
                    btnTextColor: UIConstants.textColor
                    btnBgColor: UIConstants.borderGreen
                    radius: 5
                    iconVisible: true
                    onClicked: {
                        setModalDisappear();
                    }
                }
            }
            //--- Behavior animation
            Behavior on anchors.topMargin {
                NumberAnimation {
                    duration: 400
                    easing.type: Easing.InCubic
                }
            }
        }
    }

    //----- JS supported funcs
    function setModalAppear()
    {
        modalWrapper.anchors.topMargin = modalContent.height / 9;
    }

    function setModalDisappear()
    {
        modalWrapper.anchors.topMargin = - modalContent.height;
        rootItem.opacity = 0;
    }
}
