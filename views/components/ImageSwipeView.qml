import QtQuick 2.0
import QtQuick.Controls 2.2
import UIConstants 1.0
import Qt.labs.folderlistmodel 2.1

Item {
    id: root
    focus: visible
    property string folderFilesPath: ""
    Rectangle {
        anchors.fill: parent
        color: UIConstants.bgColorOverlay
        opacity: .9
        MouseArea {
            anchors.fill: parent
            onClicked: root.destroy()
        }
    }

    Rectangle {
        width: parent.width * 2 / 3
        height: parent.height * 3 / 4
        anchors.centerIn: parent
        color: UIConstants.transparentColor
        SwipeView {
            id: view
            currentIndex: 1
            anchors.fill: parent
            Repeater {
                model: folderOfFiles
                Item {
                    Image {
                        source: folderFilesPath + model.fileName //"file://" + applicationDirPath + "/downloads/192.168.1.11/2019.12.18-09.00.00/previews/" + model.fileName
                        anchors.fill: parent
                    }
                }
            }
        }

//        PageIndicator {
//            id: indicator
//            count: view.count
//            currentIndex: view.currentIndex
//            anchors.bottom: view.bottom
//            anchors.horizontalCenter: parent.horizontalCenter
//        }
    }

    FolderListModel {
        id: folderOfFiles
        folder: folderFilesPath //"file://" + applicationDirPath + "/downloads/192.168.1.11/2019.12.18-09.00.00/previews"
        nameFilters: ["*.jpg"]
        showFiles: true
        showDirs: false
    }
    Keys.onPressed: if (event.key == Qt.Key_Escape) root.destroy()
}
