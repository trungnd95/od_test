/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component: Ip Address Form
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 02/03/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//----------------- Include Custom views --------------------------------------
import UIConstants 1.0

Item {
    id: ipAddressForm
    //--- Properties
    property int ids
    property alias titleText: title.text
    property alias ip1stTxt: ip1st.text
    property alias ip2stTxt: ip2st.text
    property alias ip3stTxt: ip3st.text

    //--- Signals
    signal typing(real stt, real columnNumber, string value)

    //--- Validators define
    RegExpValidator {
        id: ipElementValid
        regExp: /^[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]$/
    }

    //--- Content Layout
    RowLayout {
        anchors.fill: parent
        //---Title
        Text {
             id: title
             text: "Từ: "
             color: UIConstants.textFooterColor
             font { pixelSize: 12; family: UIConstants.customFont }
             anchors{ left: parent.left; verticalCenter: parent.verticalCenter }
        }
        //--- IP search field
        Rectangle {
            id: ipSearchField
            anchors { left: title.right; leftMargin: 10; right: parent.right; verticalCenter: parent.verticalCenter; rightMargin: 10 }
            Layout.preferredHeight: parent.height - 15
            color: UIConstants.cfProcessingOverlayBg
            opacity: .5
            radius: 5
            gradient: Gradient {
                GradientStop { position: 0.0; color: UIConstants.cfProcessingOverlayBg }
                GradientStop { position: 0.8; color: UIConstants.bgColorOverlay }
                GradientStop { position: 1.0; color: UIConstants.bgColorOverlay }
            }

            Row {
                anchors.fill: parent
                spacing: 1
                TextField {
                    id: ip1st
                    width: parent.width / 5
                    height: parent.height * 2 / 3
                    anchors.verticalCenter: parent.verticalCenter
                    color: UIConstants.textFooterColor
                    placeholderText: qsTr("172")
                    font.pixelSize: 11
                    font.family: UIConstants.customFont
                    background: Rectangle {
                        anchors.fill: parent
                        color: UIConstants.transparentColor
                    }
                    validator: ipElementValid
                    onTextChanged: {
                        ipAddressForm.typing(ids, 1, ip1st.text);
                    }
                }
                Text {
                    id: dot1
                    text: "."
                    font.pixelSize: 20
                    color: UIConstants.textFooterColor
                    anchors{ bottom: parent.bottom; bottomMargin: parent.height / 8 }
                }
                TextField {
                    id: ip2st
                    width: parent.width / 5
                    height: parent.height * 2 / 3
                    anchors.verticalCenter: parent.verticalCenter
                    color: UIConstants.textFooterColor
                    placeholderText: qsTr("20")
                    font.pixelSize: 11
                    font.family: UIConstants.customFont
                    background: Rectangle {
                        anchors.fill: parent
                        color: UIConstants.transparentColor
                    }
                    validator: ipElementValid
                    onTextChanged: {
                        ipAddressForm.typing(ids, 2, ip2st.text);
                    }
                }
                Text {
                    id: dot2
                    text: "."
                    font.pixelSize: 20
                    color: UIConstants.textFooterColor
                    anchors{ bottom: parent.bottom; bottomMargin: parent.height / 8 }
                }
                TextField {
                    id: ip3st
                    width: parent.width / 5
                    height: parent.height * 2 / 3
                    anchors.verticalCenter: parent.verticalCenter
                    color: UIConstants.textFooterColor
                    placeholderText: qsTr("200")
                    font.pixelSize: 11
                    font.family: UIConstants.customFont
                    background: Rectangle {
                        anchors.fill: parent
                        color: UIConstants.transparentColor
                    }
                    validator: ipElementValid
                    onTextChanged: {
                        ipAddressForm.typing(ids, 3, ip3st.text);
                    }
                }
                Text {
                    id: dot3
                    text: "."
                    font.pixelSize: 20
                    color: UIConstants.textFooterColor
                    anchors{ bottom: parent.bottom; bottomMargin: parent.height / 8 }
                }
                TextField {
                    id: ip4st
                    width: parent.width / 5
                    height: parent.height * 2 / 3
                    anchors.verticalCenter: parent.verticalCenter
                    color: UIConstants.textFooterColor
                    placeholderText: qsTr("1")
                    font.pixelSize: 11
                    font.family: UIConstants.customFont
                    background: Rectangle {
                        anchors.fill: parent
                        color: UIConstants.transparentColor
                    }
                    validator: ipElementValid
                }
            }
        }
    }
}
