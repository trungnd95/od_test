/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component: ListFilesItem
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 28/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//---------------- Include custom libs ----------------------------------------
import UIConstants 1.0

Item {
    id: rootItem

    property alias fileName_: fileNameTxt.text
    property alias sequence_: sttText.text
    signal itemClicked(string fileNameClicked)
    RowLayout {
        anchors.fill: parent

        //--------- Sequence Number
        Rectangle {
            id: sttCircle
            Layout.preferredWidth: parent.width / 15
            Layout.preferredHeight: width
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 15
            radius: width
            color: "#60a3bc"
            opacity: .4
            Text {
                id: sttText
                anchors.centerIn: parent
                text: "1"
                font.pixelSize: 11
                font.family: UIConstants.customFont
                color: UIConstants.textColor
            }
        }

        //--------- File name
        Rectangle {
            id: fileNameWrapper
            color: UIConstants.transparentColor
            Layout.preferredWidth: parent.width * 10 / 15
            Layout.preferredHeight: parent.height
            anchors { verticalCenter: parent.verticalCenter; left: sttCircle.right }
            clip: true
            Text {
                id: fileNameTxt
                text: "2019:02:28-15:40:30.h265"
                anchors.verticalCenter: parent.verticalCenter
                x: 15
                font.pixelSize: 12
                font.family: UIConstants.customFont
                color: UIConstants.textFooterColor
            }
        }

        //--------- Actions
        Rectangle {
            id: actionCops
            color: UIConstants.transparentColor
            Layout.preferredHeight: parent.height
            anchors { left: fileNameWrapper.right; right: parent.right }
            opacity: .7
            Text {
                id: iconPlay
                text: "\uf138"
                font.pixelSize: 17
                anchors.verticalCenter: parent.verticalCenter
                x: parent.width / 2
                color: UIConstants.textFooterColor
            }
        }

        //--------- Event handler
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            enabled: true
            onClicked: {
                rootItem.itemClicked(fileName_);
                iconPlay.text = UIConstants.iChoosedRight
                iconAnimation.start();
                fileNameTxt.font.bold = true;
            }
        }

        //-------- Animation
        PropertyAnimation {
            id: iconAnimation
            running: false
            loops: Animation.Infinite
            target: iconPlay
            property: "x"
            from: 15
            to: 30
            duration: 1000
            easing.type: Easing.InOutQuad
        }
    }
    //-------Border bottom
    Canvas {
        anchors.fill: parent
        onPaint: {
            var ctx = getContext("2d");
            ctx.strokeStyle = UIConstants.categoryCirColor;
            ctx.lineWidth = 1;
            ctx.beginPath();
            ctx.moveTo(10, height); ctx.lineTo(width - 10, height);
            ctx.closePath();
            ctx.stroke();
        }
    }
}
