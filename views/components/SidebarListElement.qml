/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component: Sidebar Element
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 19/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.8
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import UIConstants 1.0
import cam.attrs  1.0
import enums.notification_types 1.0
import enums.packet_keys 1.0

//---------------- Component definition ----------------------------------------
Item {
    id: sidebarListElement
    width: parent.width
    height: 50
    opacity: .8

    //-------------- Element property
    property alias iconForNavEl: iconForNavEle.text
    property alias navContent: navEleContent.text
    property alias camName: camName.text
    property var   camItem: new Object({})
    property alias xLeft: numberOfDevices.x
    property int   navLevel: 1
    property bool  dragEnable: false
    property alias iconRight: navIconRight.text

    //-------------- Signal
    signal clickedNavTrigger();

    Rectangle {
        id: sideNavEle
        anchors.fill: parent
        color: UIConstants.transparentColor
        //---- Left sidebar icon
        Rectangle {
            id: numberOfDevices
            width: 20
            height: 20
            color: "#3c6382"
            radius: width
            anchors.verticalCenter: parent.verticalCenter
            x: 0
            Text {
                id: iconForNavEle
                anchors.centerIn: parent
                text: "2"
                font.bold: true
                font.pixelSize: 11
                font.family: UIConstants.customFont
                color: UIConstants.textColor
            }
        }

        //---- Text content
        TextEdit {
            id: camName
            text: ""
            anchors.left: numberOfDevices.right
            anchors.leftMargin: 7
            anchors.verticalCenter: parent.verticalCenter
            wrapMode: TextEdit.NoWrap
            color: UIConstants.textColor
            font.family: UIConstants.customFont
            font.pixelSize: 13
            visible: navLevel == 2 ? true : false
            focus: false
            activeFocusOnPress: false
            Keys.onPressed: {
                if (event.key === Qt.Key_Return) {
                    event.accepted = true;
                    if (camName.text.length == 0) {
                        NotificationManger.addNoti(NotificationTypes.ERROR, "Tên không được để trống.!");
                    } else {
                        camName.focus = false;
                    }
                }
            }

            onEditingFinished: {
                //--- Update cam name to database
            }

            onFocusChanged: {
                if (!camName.focus) {
                    if( DevicesManager.updateCamAttrById(camItem.id, CameraAttrs.INFOS__NAME, camName.text.trim().replace("/\r\n/g", "")) ) {
                        var obj = {infos: {name: camName.text.trim()}};
                        SocketSender.sendData(PacketKeys.UPDATE_CONFIG, JSON.stringify(obj), camItem.ipAddress, camItem.comRecvPort);
                        NotificationManger.addNoti(NotificationTypes.SUCCESS, "Thay đổi được cập nhật.!");
                    } else {
                        NotificationManger.addNoti(NotificationTypes.ERROR, "Xảy ra lỗi khi cập nhật dữ liệu.!");
                        camItem.name = DevicesManager.getCamById(camItem.id).infos.name;
                    }
                }
            }

            onTextChanged: {
                if (camName.focus == true) {
                    console.log(camName.text, camName.text.length);
                    //--- Update camera name in model instance and validator input length. Max is 15.
                    if (camName.text.length < 10) {
                        camItem.name = camName.text;
                    } else {
                        camName.focus = false;
                        NotificationManger.addNoti(NotificationTypes.WARNING, "Chiều dài tối đa là 10 kí tự!");
                    }
                }
            }
        }

        Text {
            id: navEleContent
            z: 15
            anchors.left: camName.right
            anchors.verticalCenter: parent.verticalCenter
            text: ""
            color: UIConstants.textColor
            font.family: UIConstants.customFont
            font.pixelSize: 13
            Drag.active: dragArea.drag.active && dragEnable
            Drag.dragType: Drag.Automatic
            Drag.supportedActions: Qt.CopyAction
            Drag.mimeData: {
                "text/plain": "{ \"camId\": \"" + camItem.id + "\", \"camIp\": \"" + camItem.ipAddress + "\" }"
            }
            MouseArea {
                id: dragArea
                enabled: dragEnable
                hoverEnabled: true
                anchors.fill: parent
                drag.target: parent
                onPressed: {
                    parent.grabToImage(function(result) {
                        parent.Drag.imageSource = result.url
                    })
                }
                onEntered: {
                    if( navLevel == 2) {
                        editIcon.visible = true;
                    }
                }

                onExited: {
                    editIcon.visible = false;
                }
            }

        }

        Text {
            id: editIcon
            anchors { left: navEleContent.right; leftMargin: 15 }
            text: UIConstants.iEdit
            visible: false
            font.pixelSize: 15
            anchors.verticalCenter: parent.verticalCenter
            color: UIConstants.textFooterColor
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    camName.focus = true;
                }
            }
        }

        //---- Right sidebar items
        //--For nav level 2 only
        Text {
            id: streamingStateOff
            anchors { right: parent.right; rightMargin: 30; verticalCenter: parent.verticalCenter }
            text: "\uf4e2"
            font.pixelSize: 13
            color: UIConstants.iConOff
            visible: dragEnable ? ((navLevel == 2 && camItem.activePos === -1) ? true : false) : false
        }

        Rectangle {
            id: streamingStateOn
            anchors { right: parent.right; rightMargin: 30; verticalCenter: parent.verticalCenter }
            width: 12
            height: 12
            visible: dragEnable ? ((navLevel == 2 && camItem.activePos !== -1) ? true : false) : false
            color: "#eb4d4b"
            radius: 12
            Rectangle {
                id: deviceStreamingState
                anchors.fill: parent
                color: "#ff7979"
                opacity: 0
                radius: width
            }

            SequentialAnimation {
                id: mapActiveAnimation
                running: camItem.activePos !== -1 ? true : false
                loops: Animation.Infinite
                ParallelAnimation {
                    ScaleAnimator {
                        target: deviceStreamingState
                        from: 1
                        to: 1.1
                        duration: 300
                    }
                    NumberAnimation {
                        target: deviceStreamingState
                        properties: "opacity"
                        from: 0
                        to: 0.5
                        duration: 300
                    }
                }

                ParallelAnimation {
                    ScaleAnimator {
                        target: deviceStreamingState
                        from: 1.1
                        to: 1.2
                        duration: 400
                    }
                    NumberAnimation {
                        target: deviceStreamingState
                        properties: "opacity"
                        from: 0.5
                        to: 1
                        duration: 400
                    }
                }

                ParallelAnimation {
                    ScaleAnimator {
                        target: deviceStreamingState
                        from: 1.4
                        to: 1.5
                        duration: 500
                    }
                    NumberAnimation {
                        target: deviceStreamingState
                        properties: "opacity"
                        from: 1
                        to: 0
                        duration: 500
                    }
                }
            }
        }

        //--For both nav level 1 & 2 with same position but difference icon
        Text {
            id: navIconRight
            anchors { right: parent.right; rightMargin: 10; verticalCenter: parent.verticalCenter }
            text: ""
            color: navLevel == 1 ? UIConstants.textFooterColor : (dragEnable ? (camItem.status ? UIConstants.textSideActive : UIConstants.iConOff) : UIConstants.cateDescTextColor)
            font.pixelSize: 13
            font.family: "FontAwesome"
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                ListVideosOutput.hightLightPos = camItem.activePos || -1;

                sidebarListElement.clickedNavTrigger();
                if( navLevel == 1 )
                {
                    navIconRight.text = ( navIconRight.text === UIConstants.iCaretDown )
                            ? UIConstants.iCaretLeft : UIConstants.iCaretDown;
                }
            }

            onEntered: {
                if( navLevel == 2) {
                    editIcon.visible = true;
                }
            }

            onExited: {
                editIcon.visible = false;
            }
            z: -1
        }
    }
}
