import QtQuick 2.0
import UIConstants 1.0

Item {
    id: root

// public
    property double endTime: 0
    property double startTime:  0
    property double stepSize: -1
    property double backend: startTime
    property double value:  backend
    property double previewValue: 0
    property var    listWarningPositions:  []

    signal clicked(double percent);  // goto position where has "percent" in total range of slider
    signal showSwipeImages();

    opacity: 1 //enabled &&  !mouseArea.pressed? 1: 0.3 // disabled/pressed state

    Row {
        id: sliderPane
        anchors.fill: parent
        anchors.leftMargin: 10
        anchors.rightMargin: 10
        spacing: 10
        Text {
            id: timeLeft
            text: startTime == 0 ? "00:00:00" : convertIntToTime(startTime)
            font.pixelSize: 13
            font.family: UIConstants.customFont
            color: UIConstants.textFooterColor
            anchors.verticalCenter: parent.verticalCenter
        }

        Rectangle {
            id: sliderRoot
            anchors.verticalCenter: parent.verticalCenter
            color: UIConstants.transparentColor
            width: parent.width - timeLeft.width - timeRight.width - 20
            height: parent.height

            Repeater { // left and right trays (so tray doesn't shine through pill in disabled state)
                model: 2
                delegate: Rectangle {
                    x:     !index? 0: pill.x + pill.width - radius
                    width: !index? pill.x + radius: sliderRoot.width - x;  height: sliderRoot.height
                    radius: 2 //0.5 * height
                    color: index == 0 ? UIConstants.liveViewGridHighlight : UIConstants.cateCirTextColor
                    anchors.verticalCenter: parent.verticalCenter
                }
            }

            Rectangle { // pill
                id: pill
                anchors.verticalCenter: parent.verticalCenter
                x: (value - startTime) / (endTime - startTime) * (sliderRoot.width - pill.width) // pixels from value
                width: parent.height;  height: width + 4
                border.width: 1 //0.05 * sliderRoot.height
                border.color: UIConstants.textFooterColor
                radius: 1

                Text {
                    text: convertIntToTime(value)
                    y: -parent.height - 10
                    x: 0
                    font.pixelSize: 12
                    font.family: UIConstants.customFont
                    color: UIConstants.textFooterColor
                    anchors.horizontalCenter: parent.horizontalCenter
                    visible: endTime > 0
                }
            }

            MouseArea {
                id: mouseArea
                anchors.fill: parent
                hoverEnabled: true
                //        drag {
                //            target:   pill
                //            axis:     Drag.XAxis
                //            maximumX: root.width - pill.width
                //            minimumX: 0
                //        }
                //        onPositionChanged:{
                //            if(drag.active) {
                //                setPixels(pill.x + 0.5 * pill.width) // drag pill
                //            }
                //        }
                onClicked: {
                    setPixels(mouse.x) // tap tray
                    imagePreview.x = mouse.x - imagePreview.width / 2;
                }

                onPositionChanged: {
                    getPositionTime(mouse.x);
                    imagePreview.x = mouse.x - imagePreview.width / 2;
                }

                onExited: {
                    imagePreview.visible = false;
                }

                onEntered: {
                    if (endTime > 0)  imagePreview.visible = true;
                }
            }

            //--- Preview image
            Image {
                id: imagePreview
                visible: false
                width: 100
                height: 60
                anchors.bottom: parent.top
                anchors.bottomMargin: 5
                source: "file:///" + applicationDirPath + "/downloads/192.168.1.11/2019.12.18-09.00.00/previews/1.jpg"
                x: 50
                opacity: .8
                Text {
                    id: timeAtPreviewPoint
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: convertIntToTime(previewValue)
                    y: parent.height - 5
                    font.pixelSize: 12
                    font.bold: true
                    font.family: UIConstants.customFont
                    color: UIConstants.textFooterColor
                }
            }

        }

        Text {
            id: timeRight
            text: endTime == 0 ? "00:00:00" : convertIntToTime(endTime)
            font.pixelSize: 13
            font.family: UIConstants.customFont
            color: UIConstants.textFooterColor
            anchors.verticalCenter: parent.verticalCenter
        }

    }

    //---- Js supported funcs
    function setPixels(pixels) {
        backend = Math.min(Math.max(startTime, caculateTimeBasedOnPosition(pixels)), endTime);
        root.clicked((backend - startTime) / (endTime - startTime));
    }

    function getPositionTime(pixels) {
        previewValue = Math.min(Math.max(startTime, caculateTimeBasedOnPosition(pixels)), endTime);
    }

    function caculateTimeBasedOnPosition(pixels) {
        var value = (endTime - startTime) / (sliderRoot.width - pill.width) * (pixels - pill.width / 2) + startTime; // value from pixels
        //if (stepSize != -1) {
        //    value = Math.round(value / stepSize) * stepSize;
        //}
        return value;
    }

    function increase(step) {
        backend =  Math.min(Math.max(startTime, backend + step), endTime);
    }

    function decrease(step) {
        backend =  Math.min(Math.max(startTime, backend - stepSize), endTime);
    }

    function convertIntToTime(secs) {
        var time = new Date();
         // create Date object and set to today's date and time
         time.setHours(parseInt(secs/3600) % 24);
         time.setMinutes(parseInt(secs/60) % 60);
         time.setSeconds(parseInt(secs%60));
         time = time.toTimeString().split(" ")[0];
         // time.toString() = "HH:mm:ss GMT-0800 (PST)"
         // time.toString().split(" ") = ["HH:mm:ss", "GMT-0800", "(PST)"]
         // time.toTimeString().split(" ")[0]; = "HH:mm:ss"
         return time;
    }

    function createWarningsMark() {
        var component = Qt.createComponent("WarningMark.qml");
        for (var i = 0; i < listWarningPositions.length; i++) {
            var warningMark = component.createObject(sliderRoot, {x: listWarningPositions[i]});
            warningMark.clicked.connect(root.showSwipeImages);
            if (warningMark == null) {
                console.log("Error creating object");
            }
        }
    }

    Component.onCompleted: {
        createWarningsMark();
    }

    onListWarningPositionsChanged: {
        createWarningsMark();
    }
}
