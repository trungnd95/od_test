/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component: Fonts Loader Component
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 15/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//---------------------------Include QT LIBs ----------------------------------
import QtQuick 2.0

Item {

    FontLoader {
        id: fontAwesome
        source: "qrc:/assets/fonts/fontawesome-webfont.ttf"
    }

//    FontLoader {
//        id: fontAwesome1
//        source: "qrc:/assets/fonts/FontAwesomeRegular-400.otf"
//    }

//    FontLoader {
//        id: fontAwesome3
//        source: "qrc:/assets/fonts/FontAwesomeSolid-900.otf"
//    }
//    FontLoader {
//        id: fontAwesome4
//        source: "qrc:/assets/fonts/fa-brands-400.ttf"
//    }

//    FontLoader {
//        id: fontAwesome5
//        source: "qrc:/assets/fonts/fa-regular-400.ttf"
//    }

    FontLoader {
        id: fontAwesome6
        source: "qrc:/assets/fonts/fa-solid-900.ttf"
    }

    FontLoader {
        id: fontComicSans
        source: "qrc:/assets/fonts/ComicSansMS3.ttf"
    }

    FontLoader {
        id: nunitoFont
        source: "qrc:/assets/fonts/Nunito-Regular.ttf"
    }
}
