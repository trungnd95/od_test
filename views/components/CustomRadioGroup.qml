/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Partial:
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 13/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//---------------- Include custom libs ----------------------------------------
import UIConstants 1.0

Item {
    id: rootItem
    //-------- Properties
    property alias radioModel: radioEles.model
    property string labelColor: UIConstants.textFooterColor
    property string activeRadioColor: UIConstants.textSideActive
    property int radioBtnSize: 20
    ButtonGroup { id: radioGroup }
    Row {
        anchors.verticalCenter: parent.verticalCenter
        Repeater {
            id: radioEles
            model: ListModel {
                ListElement { id_: 1; checked_: true; label_: "A" }
                ListElement { id_: 2; checked_: false; label_: "B" }
            }
            RadioButton {
                id: control
                anchors.verticalCenter: parent.verticalCenter
                text: label_
                checked: checked_
                ButtonGroup.group: radioGroup
                indicator: Rectangle {
                    implicitWidth: radioBtnSize
                    implicitHeight: radioBtnSize
                    x: control.leftPadding
                    y: parent.height / 2 - height / 2
                    radius: radioBtnSize / 2
                    border.color: activeRadioColor
                    opacity: .7

                    Rectangle {
                        width: parent.width * 2 / 3
                        height: parent.height * 2 / 3
                        x: parent.width / 6
                        y: parent.height / 6
                        radius: width
                        color: activeRadioColor
                        visible: control.checked
                    }
                }

                contentItem: Text {
                    text: control.text
                    font: control.font
                    opacity: enabled ? 0.6 : 0.3
                    color: labelColor
                    verticalAlignment: Text.AlignVCenter
                    leftPadding: control.indicator.width + control.spacing
                }
            }
        }
    }
}
