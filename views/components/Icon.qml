/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component: Icon for mutual using
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 27/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import UIConstants 1.0

Item {
    id: root
    property alias type: icon.text
    property alias color: icon.color
    property alias size: icon.font.pixelSize
    property alias supText: textDes.text
    property alias needSupText: textDes.visible

    signal clicked();
    Text {
        id: icon
        font.pixelSize: 13
        text: "\uf028"
        font.family: "FontAwesome"
        color: UIConstants.grayColor
        transform: Rotation { id: iconRot; angle: 0 }
    }
    Text {
        id: textDes
        font.pixelSize: 9
        font.bold: true
        color: UIConstants.grayColor
        text: "VGA"
        font.family: "FontAwesome"
        anchors.left: icon.right
        y: 3
        visible: false
    }
    MouseArea {
        anchors.fill: parent
        enabled: true
        hoverEnabled: true
        onClicked: {
            root.clicked();
        }
    }
}
