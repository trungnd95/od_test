/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component: NavElement Component
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 13/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ==============================================================================
 */

//--------------------------- Include Qt Libs -----------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import UIConstants 1.0

//--------------------------- Component content --------------------------------
Item {
    id: navElement
    property alias text: txt.text
    property alias iconText: icon.text
    property string itemId
    property bool active: false
    property string color_: active ? UIConstants.borderGreen : UIConstants.grayColor
    width: icon.width + txt.width + 30
    height: 20
    signal clicked(real _itemId)
    signal hover()

    Text {
        id: icon
        text: "\uf015"
        anchors.right: txt.left
        anchors.rightMargin: 5
        anchors.verticalCenter: parent.verticalCenter
        color: color_
        z: 2
    }

    Text {
        id: txt
        text: "text"
        anchors.centerIn: parent
        color: color_
        font.family: UIConstants.customFont
        z: 2
    }

    //----------- Active Item Effect
    //    Rectangle {
    //        id: activeOverlay
    //        visible: active
    //        anchors.fill: parent
    //        color: UIConstants.activeNav
    //        radius: 7
    //        gradient: Gradient {
    //            GradientStop { position: 0.0; color: "#576574" }
    //            GradientStop { position: 0.2; color: "#576574" }
    //            GradientStop { position: 1.0; color: UIConstants.activeNav  }
    //        }
    //        z: 1
    //        Canvas {
    //            id: borderRectActiveOverlay
    //            anchors.fill: parent
    //            onPaint: {
    //                var ctx = getContext("2d");
    //                ctx.strokeStyle = "#2d3436";
    //                ctx.lineWidth = 5;
    //                ctx.beginPath();
    //                ctx.moveTo( 0, height );
    //                ctx.lineTo( width, height );
    //                ctx.closePath();
    //                ctx.stroke();
    //            }
    //        }
    //    }

    //----------- Item event handling
    MouseArea {
        id: itemMourseArea
        anchors.fill: parent
        enabled: navElement.enabled
        hoverEnabled: true
        cursorShape: Qt.OpenHandCursor
        onEntered: {
            navElement.color_ = UIConstants.borderGreen;
        }
        onExited: {
            if( !active ) { navElement.color_ = UIConstants.grayColor; }
        }
        onClicked: {
            //navElement.active = true;
            navElement.clicked(itemId);
        }
    }
}
