import QtQuick 2.5
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import UIConstants 1.0
import enums.notification_types 1.0


Item {
    id: root
    property var bgColorNoti: new Object({
                             "error": UIConstants.error,
                             "warning": UIConstants.warning,
                             "info": UIConstants.info,
                             "success": UIConstants.success
    })

    property var notiIcon: new Object({
                              "error": UIConstants.iError,
                              "warning": UIConstants.iWarning,
                              "info": UIConstants.iInfo,
                              "success": UIConstants.iSuccess
    })

    property int type_
    property string typeNoti: mapType(type_)
    property alias notiMes: mainNotiMes.text
    property real currentTime: new Date().getTime()
    property real timeHistoryData
    signal hidden();

    width: mainNoti.width
    opacity: currentTime - timeHistoryData > 1000 ? 1 : 0

    Rectangle {
        id: mainNoti
        width: childrenRect.width + 30
        height: parent.height
        //x: width + 20
        radius: 8
        color: bgColorNoti[typeNoti]
        Row {
            anchors.verticalCenter: parent.verticalCenter
            spacing: 5
            x: 10
            Text {
                text: notiIcon[typeNoti]
                color: UIConstants.textColor
                font.pixelSize: 17
                font.family: "FontAwesome"
            }

            Text {
                id: mainNotiMes
                color: UIConstants.textColor
                text: qsTr("Notification Message...")
                font.family: UIConstants.customFont
                anchors.verticalCenter: parent.verticalCenter
            }
        }
        //Text {
        //    text: "\uf057"
        //    font.pointSize: 18
        //    font.family: "fontawesome"
        //    color: "#fff"
        //    x: -10
        //    y: -10
        //    opacity: .8
        //    MouseArea {
        //        anchors.fill: parent
        //        cursorShape: Qt.OpenHandCursor
        //        onClicked: {
        //            notiShowReverse.running = true;
        //        }
        //   }
        //}
    }

    NumberAnimation {
        id: notiShow
        target: root
        property: "opacity"
        from: 0
        to: 1
        duration: 1000
        running: currentTime - timeHistoryData > 1000 ? false : true
        easing.type: Easing.Linear
        onStopped: {
            timeoutLeave.running = true;
        }
    }

    NumberAnimation {
        id: notiShowReverse
        target: root
        property: "opacity"
        from: 1
        to: 0
        duration: 1000
        running: currentTime - timeHistoryData > 4000 ? true : false
        easing.type: Easing.InCubic
        onStopped: {
            root.hidden();
        }
    }

    Timer {
        id: timeoutLeave
        interval: currentTime - timeHistoryData > 4000 ? 3000 : (3000 + timeHistoryData - currentTime)
        running: currentTime - timeHistoryData > 4000 ? false : true
        repeat: false
        onTriggered: {
            notiShowReverse.running = true;
        }
    }

    Component.onCompleted: {
        console.log(timeHistoryData, currentTime);
    }

    function mapType(enumType) {
        var result;
        switch(enumType) {
            case NotificationTypes.ERROR:
                    result = "error"; break;
            case NotificationTypes.WARNING:
                    result = "warning"; break;
            case NotificationTypes.INFO:
                    result = "info"; break;
            case NotificationTypes.SUCCESS:
                    result = "success"; break;
        }
        return result;
    }
}
