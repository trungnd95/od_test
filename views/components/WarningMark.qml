import QtQuick 2.0
import UIConstants 1.0

Item {
    id: root
    signal clicked()
    Column {
        spacing: 1
        Rectangle {
            width: 3
            height: 15
            color: "red"
        }

        Text {
            text: "\uf03e"
            font.pixelSize: 12
            font.family: "FontAwesome"
            color: UIConstants.textSidebarColor
            x: -6
            MouseArea {
                anchors.fill: parent
                onClicked: root.clicked();
            }
        }
    }
}
