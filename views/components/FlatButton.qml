/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component: Flat Button
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 18/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//----------------------- Include QT libs -------------------------------------
import QtQuick 2.6
import QtQuick.Controls 2.1
import UIConstants 1.0

//----------------------- Component definition- ------------------------------
Rectangle {
    id: flatBtn
    //---------- Element property
    property alias btnText: flatBtnText.text
    property alias btnTextColor: flatBtnText.color
    property alias btnBgColor: flatBtn.color
    property alias radius: flatBtn.radius
    property alias iconVisible: iconBtn.visible
    property alias icon: iconBtn.text

    signal clicked()

    state: "normal"
    z: 10
    radius: 1
    //---------- Icon
    Text {
        id: iconBtn
        visible: false
        text: UIConstants.iX
        font.pixelSize: 18
        anchors{ verticalCenter: parent.verticalCenter; right: flatBtnText.left;
                 rightMargin: 5 }
        color: flatBtnText.color
    }
    //---------- Text
    Text {
        id: flatBtnText
        text: "Button Text"
        font.pixelSize: 13
        font.family: UIConstants.customFont
        anchors.centerIn: parent
        color: UIConstants.textBlueColor
    }

    //--------- Background
    color: UIConstants.btnCateColor
    opacity: (flatBtn.state === "down") ? 0.3 : 0.7

   //---------- Event listener
    MouseArea {
        anchors.fill: parent
        onPressed: { flatBtn.state = "down"; flatBtn.clicked(); }
        onReleased: { flatBtn.state = "normal" }
    }
}
