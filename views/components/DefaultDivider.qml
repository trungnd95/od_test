/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component: Custom Table
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 04/03/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

import QtQuick 2.0
import UIConstants 1.0

Rectangle {
    height: 1
    color: UIConstants.sidebarBorderColor
    opacity: 0.12
}
