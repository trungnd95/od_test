/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Module: Liveview Page View
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 13/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import QtMultimedia 5.5

//---------------- Include custom libs ----------------------------------------
import "../components"
import "../partials"
import UIConstants 1.0
import enums.list 1.0
import enums.packet_keys 1.0
import "../../assets/javascripts/Helper.js" as Helper


//---------------- Window definition ------------------------------------------
Item {
    id: liveView

    //-------properties
    property int viewDevidedTo: 2
    property var camsIsLivingStream: []
    property bool fullScreen: false

    //------- Signals
    signal toggleFullScreen(bool fullScreenState)
    signal gotoDeviceMgmt()

    //------- Content --------
    Row {
        anchors.fill: parent

        //---------- Sidebar
        SidebarCamsList {
            id: sidebarCamsList
            width: liveView.fullScreen ? 20 : parent.width / 5
            sidebarTitle: liveView.fullScreen ? "" : "Danh sách thiết bị"
            opacity: 1
            height: parent.height
            liveViewPage: true
            onRequestToggleSidebar: {
                sidebarCamsList.width = (sidebarCamsList.width == 20) ? parent.width / 5 : 20 ;
                videoLiveBoard.width = (videoLiveBoard.width == parent.width * 4 / 5)
                                        ? (parent.width - 20) : parent.width * 4 / 5;
                sidebarCamsList.sidebarTitle = (sidebarCamsList.sidebarTitle == "") ? "Danh sách thiết bị" : "";
            }

            onGotoDeviceMgmt: liveView.gotoDeviceMgmt();

            Behavior on width {
                NumberAnimation {
                    duration: 300
                    easing.type: Easing.Linear
                }
            }
        }

        //---------- Video Live Board
        Rectangle {
            id: videoLiveBoard
            width: liveView.fullScreen ? (parent.width - 20) : parent.width * 4 / 5
            height: parent.height
            color: UIConstants.transparentColor
            //------- Sidebar border
            RectBorder {
                id: borderLeft
                type: "left"
                thick: 3
            }

            //-------- Liveview grid board
            //--- Model delegate
            Component {
                id: numberDelegate
                Rectangle {
                    id: wrapper
                    property var activeCam: ListVideosOutput.listVideoOutputs[index].camItem || null
                    Layout.rowSpan: 2
                    Layout.columnSpan: 2
                    Layout.preferredWidth: videoLiveBoard.width / 2
                    Layout.preferredHeight: videoLiveBoard.height / 2
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    border { width: ListVideosOutput.hightLightPos == index ? 2 : 1;
                             color: ListVideosOutput.hightLightPos == index ? UIConstants.textSideActive : UIConstants.sidebarBorderColor }
                    color: UIConstants.transparentColor
                    z: 1
                    Rectangle {
                        anchors.fill: parent
                        color: dropArea.containsDrag ? UIConstants.sidebarBorderColor : UIConstants.transparentColor
                        opacity: dropArea.containsDrag ? .2 : 1
                        //--------- Cell Title
                        VideoStatusPane {
                            id: videoStatusPane
                            z: 25
                            paneTitle: wrapper.activeCam ? wrapper.activeCam.name : ""
                            visible: wrapper.activeCam ? true : false
                            activeStreamingResol: wrapper.activeCam ? wrapper.activeCam.streamingResol - 2 : 0
                            //--- Remove active stream from grid
                            onRemoveStream: {
                                //--- Remove current stream item from grid
                                ListVideosOutput.remove(index);
                            }

                            //--- Stop current active stream
                            onToggleRunPauseStream: {
                                if (runningState) {
                                    ListVideosOutput.listVideoOutputs[index].restart();
                                    SocketSender.sendData(PacketKeys.PLAY_STREAM, "", wrapper.activeCam.ipAddress, wrapper.activeCam.comRecvPort);
                                } else {
                                    ListVideosOutput.listVideoOutputs[index].pause();
                                    SocketSender.sendData(PacketKeys.STOP_STREAM, "", wrapper.activeCam.ipAddress, wrapper.activeCam.comRecvPort);
                                }
                            }

                            //--- Remote reset server endpoint
                            onRemoteRestartServer: {
                                //-- Send command to request restart system.
                                SocketSender.sendData(PacketKeys.RESET_SYS, "", wrapper.activeCam.ipAddress, wrapper.activeCam.comRecvPort);
                            }

                            //--- Change resolution of particular stream
                            onChangeResol: {
                                var cfObj = {streaming: {resol: resol}};
                                if (wrapper.activeCam && wrapper.activeCam.streamingResol != resol) {
                                    //---1. Send command to server
                                    SocketSender.sendData(PacketKeys.UPDATE_CONFIG, JSON.stringify(cfObj),
                                                          wrapper.activeCam.ipAddress,
                                                          wrapper.activeCam.comRecvPort);
                                    //---2. Save new change to config file
                                    DevicesManager.updateCamById(wrapper.activeCam.id, JSON.stringify(cfObj));
                                }
                            }

                            //--- Timer to update bandwidth in second
                            Timer {
                                interval: 1000
                                running: wrapper.activeCam ? true : false
                                repeat: true
                                onTriggered: {
                                    //----------- Update bandwidth and packet size received ---------------------
                                    /**
                                      *  Bandwidth caculated as Kilobits per second. So that, packet received in byte ->
                                      *  we have to convert it into bit by multiple with 8.
                                      */
                                    if (ListVideosOutput.listVideoOutputs[index]) {
                                        var bandwidthCaculated = Math.ceil(parseFloat(ListVideosOutput.listVideoOutputs[index].totalBytesReceivedInSecond())  * 8 / 1024);
                                        videoStatusPane.bandWidth = bandwidthCaculated + "Kb/s";
                                        ListVideosOutput.listVideoOutputs[index].resetTotalBytesReceivedInSecond(0);
                                    }
                                }
                            }
                        }

                        //--------- Cell View
                        ProcessingOverlay {
                            id: processingOverlay
                            anchors.fill: parent
                            textOverlay: wrapper.activeCam ? wrapper.activeCam.name : "Chưa gán thiết bị"
                            loadingAnimation: wrapper.activeCam ? false : true
                            z: 10
                        }

                        //--------- Drop area
                        DropArea {
                            id: dropArea
                            anchors.fill: parent
                            keys: ["text/plain"]
                            onDropped: {
                                var passedData = JSON.parse(drop.text);
                                //--- Add new video streaming to list
                                ListVideosOutput.addToList(index, DevicesManager.listCamsModel.getCam(parseInt(passedData.camId)));
                            }
                        }

                        VideoOutput {
                            id: videoOutput
                            anchors.fill: parent
                            visible: wrapper.activeCam ? true : false
                            z: 20
                            source: ListVideosOutput.listVideoOutputs[index] || null
                        }
                    }
                }
            }

            //--- Gridview Videos Output
            GridLayout {
                id: liveViewGrid
                anchors.fill: parent
                rows: 4
                columns: 4
                rowSpacing: 0
                columnSpacing: 0
                Repeater {
                    id: videoOutputRepeater
                    model: 4
                    delegate: numberDelegate
                }

            }

            //------- Control Pane zone
            Rectangle {
                id: liveViewControlPane
                anchors { bottom: parent.bottom; horizontalCenter: parent.horizontalCenter }
                width: parent.width / 2
                height: 45
                radius: 10
                opacity: .7
                color: UIConstants.bgColorOverlay
                gradient: Gradient {
                    GradientStop { position: 0.0; color: UIConstants.cfProcessingOverlayBg }
                    GradientStop { position: 0.8; color: UIConstants.bgColorOverlay }
                    GradientStop { position: 1.0; color: UIConstants.bgColorOverlay }
                }

                //------ Control pane data wrapper
                Rectangle {
                    height: parent.height
                    width: parent.width * 3 / 4
                    anchors.centerIn: parent
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    color: UIConstants.transparentColor
                    RowLayout {
                        id: listControlItems
                        anchors.fill: parent
                        layoutDirection: Qt.LeftToRight
                        spacing: 15

                        //----------- Voice control
                        Icon {
                            id: voiceControlIcon
                            Layout.preferredWidth: parent.width / 6
                            Layout.preferredHeight: parent.height / 2
                            opacity: .7
                            size: 25
                            onClicked: {
                                if( !voiceControlIcon.needSupText )
                                {
                                    voiceControlIcon.needSupText = true;
                                    voiceControlIcon.supText = UIConstants.iX
                                } else {
                                    voiceControlIcon.needSupText = false;
                                }
                            }
                        }

                        //----------- Rocket warning control
                        Icon {
                            id: voiceWarning
                            anchors.verticalCenter: parent.verticalCenter
                            Layout.preferredWidth: parent.width / 6
                            Layout.preferredHeight: parent.height / 2
                            type: UIConstants.iRocketWarning
                            opacity: .7
                            size: 25
                            onClicked: {
                                if( !voiceWarning.needSupText )
                                {
                                    voiceWarning.needSupText = true;
                                    voiceWarning.supText = UIConstants.iX
                                } else {
                                    voiceWarning.needSupText = false;
                                }
                            }
                        }

                        //----------- Bandwidth control
                        Icon {
                            id: bandwidthControl
                            anchors.verticalCenter: parent.verticalCenter
                            Layout.preferredWidth: parent.width / 6
                            Layout.preferredHeight: parent.height / 2
                            type: UIConstants.iBandwidth
                            opacity: .7
                            size: 25
                            needSupText: true
                            supText: Helper.convertBw(JSON.parse(GeneralConfigsManager.generalSettingsInfos())['streaming']['bandwidth'])
                            onClicked: {
                                listDataControlPane.model_ = ["256Kb", "512Kb", "1Mb"];
                                listDataControlPane.type = "bwDatas";
                                listDataControlPane.visible_ = true;
                                listDataControlPane.anchors.bottom = listControlItems.top;
                                listDataControlPane.anchors.bottomMargin = -40;
                                listDataControlPane.x = bandwidthControl.x;
                            }
                        }

                        //----------- Resolution control
                        Icon {
                            id: resolutionControl
                            anchors.verticalCenter: parent.verticalCenter
                            Layout.preferredWidth: parent.width / 6
                            Layout.preferredHeight: parent.height / 2
                            type: UIConstants.iResolution
                            opacity: .7
                            size: 25
                            needSupText: true
                            supText: Helper.convertResol(JSON.parse(GeneralConfigsManager.generalSettingsInfos())['streaming']['resol'])
                            onClicked: {
                                listDataControlPane.model_ = ["HD", "VGA", "Mini-VGA"];
                                listDataControlPane.type = "resolDatas";
                                listDataControlPane.visible_ = true;
                                listDataControlPane.anchors.bottom = listControlItems.top;
                                listDataControlPane.anchors.bottomMargin = -40;
                                listDataControlPane.x = resolutionControl.x;
                            }
                        }

                        //----------- Gridview control
                        Icon {
                            id: gridViewControl
                            anchors.verticalCenter: parent.verticalCenter
                            Layout.preferredWidth: parent.width / 6
                            Layout.preferredHeight: parent.height / 2
                            type: UIConstants.iWindowGrid
                            opacity: .7
                            size: 25
                            needSupText: true
                            supText: "4:1:1"
                            onClicked: {
                                listDataControlPane.model_ = ["4:1:1", "8:9:1", "16:1:1"];
                                listDataControlPane.type = "viewTypeDatas";
                                listDataControlPane.visible_ = true;
                                listDataControlPane.anchors.bottom = listControlItems.top;
                                listDataControlPane.anchors.bottomMargin = -40;
                                listDataControlPane.x = gridViewControl.x;
                            }
                        }

                        //----------- Fullscreen icon
                        Icon {
                            id: fullScreenBtn
                            anchors.verticalCenter: parent.verticalCenter
                            Layout.preferredWidth: parent.width / 6
                            Layout.preferredHeight: parent.height / 2
                            type: liveView.fullScreen ? UIConstants.iReturnScreen : UIConstants.iOpenFullScreen
                            size: 25
                            opacity: .7
                            needSupText: false
                            onClicked: {
                                //--- Toggle full screen
                                liveView.fullScreen = !liveView.fullScreen;
                                liveView.toggleFullScreen(liveView.fullScreen);

                                sidebarCamsList.width = liveView.fullScreen ? 20 : liveView.width / 5;
                                sidebarCamsList.toggleSidebar.text = liveView.fullScreen ?  UIConstants.iRightHand : UIConstants.iLeftHand
                                sidebarCamsList.sidebarTitle = liveView.fullScreen ? "" : "Danh sách thiết bị";
                                videoLiveBoard.width = liveView.fullScreen ? (liveView.width - 20) : liveView.width * 4 / 5;
                            }
                        }
                    }
                    //----------- Data selection
                    ListDataControlPane {
                        id: listDataControlPane
                        width: 100
                        height: 100
                        anchors.bottom: parent.top
                        onListViewClicked: {
                            listDataControlPane.visible = false;
                            switch( type )
                            {
                                case "bwDatas":
                                    bandwidthControl.supText = choosedItem;
                                    var cfObj = {streaming: {bandwidth: Helper.convertData(choosedItem)}};
                                    for(var i = 0; i < Object.keys(DevicesManager.listCamsModel.listCams).length; i++) {
                                        SocketSender.sendData(PacketKeys.UPDATE_CONFIG, JSON.stringify(cfObj),
                                                              DevicesManager.listCamsModel.listCams[i].ipAddress,
                                                              DevicesManager.listCamsModel.listCams[i].comRecvPort);
                                    }
                                    break;
                                case "resolDatas":
                                    resolutionControl.supText = choosedItem;
                                    cfObj = {streaming: {resol: Helper.convertBackResol(choosedItem)}};
                                    for(var i = 0; i < Object.keys(DevicesManager.listCamsModel.listCams).length; i++) {
                                        SocketSender.sendData(PacketKeys.UPDATE_CONFIG, JSON.stringify(cfObj),
                                                              DevicesManager.listCamsModel.listCams[i].ipAddress,
                                                              DevicesManager.listCamsModel.listCams[i].comRecvPort);
                                    }
                                    break;
                                case "viewTypeDatas":
                                    gridViewControl.supText = choosedItem;
                                    switch( choosedItem )
                                    {
                                        case "4:1:1":
                                            videoOutputRepeater.model = 4;
                                            for( var i  = 0; i < videoOutputRepeater.count; i++)
                                            {
                                                videoOutputRepeater.itemAt(i).Layout.rowSpan = 2;
                                                videoOutputRepeater.itemAt(i).Layout.columnSpan = 2;
                                                videoOutputRepeater.itemAt(i).Layout.preferredWidth = videoLiveBoard.width / 2;
                                                videoOutputRepeater.itemAt(i).Layout.preferredHeight = videoLiveBoard.height / 2;
                                                videoOutputRepeater.itemAt(i).Layout.fillWidth = true;
                                                videoOutputRepeater.itemAt(i).Layout.fillHeight = true;
                                            }
                                            break;
                                        case "8:9:1":
                                            videoOutputRepeater.model = 8;
                                            videoOutputRepeater.itemAt(0).Layout.rowSpan = 3;
                                            videoOutputRepeater.itemAt(0).Layout.columnSpan = 3;
                                            videoOutputRepeater.itemAt(0).Layout.preferredWidth = videoLiveBoard.width * 3 / 4;
                                            videoOutputRepeater.itemAt(0).Layout.preferredHeight = videoLiveBoard.height * 3 / 4;
                                            videoOutputRepeater.itemAt(0).Layout.fillWidth = true;
                                            videoOutputRepeater.itemAt(0).Layout.fillHeight = true;
                                            for( var j  = 1; j < videoOutputRepeater.count; j++)
                                            {
                                                videoOutputRepeater.itemAt(j).Layout.rowSpan = 1;
                                                videoOutputRepeater.itemAt(j).Layout.columnSpan = 1;
                                                videoOutputRepeater.itemAt(j).Layout.preferredWidth = videoLiveBoard.width / 4;
                                                videoOutputRepeater.itemAt(j).Layout.preferredHeight = videoLiveBoard.height / 4
                                                videoOutputRepeater.itemAt(j).Layout.fillWidth = true;
                                                videoOutputRepeater.itemAt(j).Layout.fillHeight = true;
                                            }
                                            break;
                                        case "16:1:1":
                                            videoOutputRepeater.model = 16;
                                            for( var k  = 0; k < videoOutputRepeater.count; k++)
                                            {
                                                videoOutputRepeater.itemAt(k).Layout.rowSpan = 1;
                                                videoOutputRepeater.itemAt(k).Layout.columnSpan = 1;
                                                videoOutputRepeater.itemAt(k).Layout.preferredWidth = videoLiveBoard.width / 4;
                                                videoOutputRepeater.itemAt(k).Layout.preferredHeight = videoLiveBoard.height / 4
                                                videoOutputRepeater.itemAt(k).Layout.fillWidth = true;
                                                videoOutputRepeater.itemAt(k).Layout.fillHeight = true;
                                            }
                                            break;
                                    }
                                    break;
                            }
                        }
                        Behavior on visible {
                            NumberAnimation {
                                duration: 200
                                easing.type: Easing.InBack
                            }
                        }
                    }
                }
            }
        }
    }

    //------------ JS supported functions
    function videoLiveviewRequestLeftBorder()
    {
        borderLeft.requestPaint();
    }
}
