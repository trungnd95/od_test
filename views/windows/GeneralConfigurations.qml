/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Module: General Configuration view
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 13/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//---------------- Include custom libs ----------------------------------------
import "../components"
import "../partials"
import UIConstants 1.0

//---------------- Window definition ------------------------------------------
Item {
    id: generalConfigurations
    RowLayout {
        anchors.fill: parent
        //---------- Sidebar
        SidebarConfigs {
            id: sidebarGeneralConfigs
            Layout.preferredWidth: parent.width / 5
            Layout.preferredHeight: parent.height
            onDisplayActiveConfigBoard: {
                configBoard.currentIndex = boardId_;
            }
            RectBorder {
                type: "right"
            }
        }
        //---------- Content
        StackLayout {
            id: configBoard
            Layout.preferredWidth: parent.width * 4 / 5
            Layout.preferredHeight: parent.height
            anchors.left: sidebarGeneralConfigs.right
            currentIndex: 0

            //--- Storage Config Board
            StorageConfigBoard {
                id: storageConfigBoard
                Behavior on opacity {
                    NumberAnimation {
                        duration: 1000
                         easing {type: Easing.Linear; overshoot: 1000}
                    }
                }
            }

            //--- Ip Params Config Board
            IpParamsConfigBoard {
                id: ipParamsConfigBoard
                Behavior on opacity {
                    NumberAnimation {
                        duration: 1000
                         easing {type: Easing.Linear; overshoot: 1000}
                    }
                }
            }

            //--- Sequence Task Config Board
            SequenceTasksConfigBoard {
                id: sequenceTasksConfigBoard
                Behavior on opacity {
                    NumberAnimation {
                        duration: 1000
                         easing {type: Easing.Linear; overshoot: 1000}
                    }
                }
            }

            //--- Streaming Config Board
            StreamingParamsConfigBoard {
                id: streamingParamsConfigBoard
                Behavior on opacity {
                    NumberAnimation {
                        duration: 1000
                         easing {type: Easing.Linear; overshoot: 1000}
                    }
                }
            }

            //--- Users management
            Oops {
                id: userManagement
                Behavior on opacity {
                    NumberAnimation {
                        duration: 1000
                         easing {type: Easing.Linear; overshoot: 1000}
                    }
                }
            }

            onCurrentIndexChanged: {
                switch( currentIndex )
                {
                    case 0:
                        setPageToActive(storageConfigBoard);
                        setPageToInactive(ipParamsConfigBoard);
                        setPageToInactive(sequenceTasksConfigBoard);
                        setPageToInactive(streamingParamsConfigBoard);
                        setPageToInactive(userManagement);
                        break;
                    case 1:
                        setPageToInactive(storageConfigBoard);
                        setPageToActive(ipParamsConfigBoard);
                        setPageToInactive(sequenceTasksConfigBoard);
                        setPageToInactive(streamingParamsConfigBoard);
                        setPageToInactive(userManagement);
                        break;
                    case 2:
                        setPageToInactive(storageConfigBoard);
                        setPageToInactive(ipParamsConfigBoard);
                        setPageToActive(sequenceTasksConfigBoard);
                        setPageToInactive(streamingParamsConfigBoard);
                        setPageToInactive(userManagement);
                        break;
                    case 3:
                        setPageToInactive(storageConfigBoard);
                        setPageToInactive(ipParamsConfigBoard);
                        setPageToInactive(sequenceTasksConfigBoard);
                        setPageToActive(streamingParamsConfigBoard);
                        setPageToInactive(userManagement);
                        break;
                    case 4:
                        setPageToInactive(storageConfigBoard);
                        setPageToInactive(ipParamsConfigBoard);
                        setPageToInactive(sequenceTasksConfigBoard);
                        setPageToInactive(streamingParamsConfigBoard);
                        setPageToActive(userManagement);
                        break;
                }
            }
        }
    }

    //------- JS supported functions
    function setPageToInactive( pageId )
    {
        pageId.opacity = 0;
        pageId.visible = false;
    }

    function setPageToActive( pageId )
    {
        pageId.opacity = 1;
        pageId.visible = true;
    }
}
