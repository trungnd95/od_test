/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Module: Playback Page View
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 13/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import QtQml 2.0
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls.Private 1.0

//---------------- Include custom libs ----------------------------------------
import "../components"
import "../partials"
import UIConstants 1.0

//---------------- Window definition ------------------------------------------
Item {
    id: playback

    signal gotoDeviceMgmt()

    RowLayout {
        anchors.fill: parent
        spacing: 0
        //---------- Sidebar
        SidebarCamsList {
            id: sidebarPlayback
            Layout.preferredWidth: parent.width / 5
            Layout.preferredHeight: parent.height
            onRequestOpenFile: {
                listFileManip.opacity = 1;
                listFileManip.Layout.preferredWidth = parent.width / 5;
                playbackDisplay.Layout.preferredWidth = parent.width * 3 / 5;
            }
            onRequestCloseFile: {
                listFileManip.opacity = 0;
                listFileManip.Layout.preferredWidth = 0;
                playbackDisplay.Layout.preferredWidth = parent.width * 4 / 5;
            }
            onGotoDeviceMgmt: playback.gotoDeviceMgmt();
        }

        //----------List files
        Rectangle {
            id: listFileManip
            opacity: 0
            //---LeftBorder
            RectBorder {
                type: "left"
                thick: 3
            }
            anchors { top: parent.top; left: sidebarPlayback.right; right: playbackDisplay.left;}
            color: UIConstants.transparentColor
            Layout.preferredWidth: 0
            Layout.preferredHeight: parent.height
            Layout.minimumWidth: 0
            ColumnLayout {
                anchors.fill: parent
                //------------- Title
                SidebarTitle {
                    id: listFileColTitle
                    Layout.preferredWidth: parent.width
                    Layout.preferredHeight: parent.height / 14
                    anchors { top: parent.top; left: parent.left; right: parent.right }
                    title: "Danh sách tệp"
                    iconType: UIConstants.iFiles
                }

                //------------- Top search bar
                SearchBar {
                    id: topSearchBar
                    anchors { top: listFileColTitle.bottom; left: parent.left; right: parent.right }
                    Layout.preferredHeight: parent.height / 14
                }
                //------------- Advance Search

                //------------- Search Results
                Rectangle {
                    id: searchResults
                    color: UIConstants.transparentColor
                    anchors { top: topSearchBar.bottom; topMargin: 20; left: parent.left; right: parent.right; bottom: parent.bottom }
                    ColumnLayout {
                        opacity: .6
                        anchors.fill: parent
                        //--------- Infos and Filter (Tilte)
                        RowLayout {
                            id: infosAndFilter
                            anchors { top: parent.top; left: parent.left; right: parent.right }
                            Layout.preferredHeight: parent.height / 30
                            Layout.preferredWidth: parent.width
                            //---- Info
                            Text {
                                id: searchResultInfo
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.left: parent.left
                                anchors.leftMargin: 20
                                Layout.preferredWidth: parent.width / 2
                                text: "30 kết quả"
                                color: UIConstants.textFooterColor
                                font.pixelSize: 12
                                font.family: UIConstants.customFont
                            }

                            Rectangle {
                                Layout.preferredWidth: parent.width / 2
                                height: 40
                                color: UIConstants.transparentColor
                                border.width: 1
                                border.color: UIConstants.bgAppColor
                                Text {
                                    id: currentDate
                                    anchors.centerIn: parent
                                    text: Qt.formatDateTime(new Date(), "dd-MM-yyyy")
                                    color: UIConstants.textFooterColor
                                    font.family: UIConstants.customFont
                                    font.pixelSize: 13
                                }

                                Text {
                                    id: arrowDown
                                    text: UIConstants.iCaretDown
                                    font.family: "FontAwesome"
                                    font.pixelSize: 16
                                    anchors.left: currentDate.right
                                    anchors.leftMargin: 10
                                    anchors.verticalCenter: parent.verticalCenter
                                    color: UIConstants.textFooterColor
                                }

                                MouseArea {
                                    anchors.fill: parent
                                    onClicked: {
                                        calendarWrapper.opacity = 1;
                                    }
                                }
                            }

                            //---- Filter
                            /*ComboBox {
                                id: fitlerFiles
                                anchors{ verticalCenter: parent.verticalCenter; right: parent.right; rightMargin: 5 }
                                Layout.preferredWidth: parent.width / 2
                                Layout.preferredHeight: parent.height
                                model: ["Tất cả", "Có cảnh báo"]
                                delegate: ItemDelegate {
                                    width: fitlerFiles.width
                                    contentItem: Text {
                                        text: modelData
                                        color: UIConstants.textFooterColor
                                        font.family: UIConstants.customFont
                                        verticalAlignment: Text.AlignVCenter
                                        opacity: .4
                                        font.pixelSize: 12
                                    }
                                }
                                indicator: Canvas {
                                    id: canvas
                                    x: fitlerFiles.width - width - fitlerFiles.rightPadding
                                    y: fitlerFiles.topPadding + (fitlerFiles.availableHeight - height) / 2
                                    width: 12
                                    height: 8
                                    contextType: "2d"
                                    Connections {
                                        target: fitlerFiles
                                        onPressedChanged: canvas.requestPaint()
                                    }

                                    onPaint: {
                                        context.reset();
                                        context.moveTo(0, 0);
                                        context.lineTo(width, 0);
                                        context.lineTo(width / 2, height);
                                        context.closePath();
                                        context.fillStyle = UIConstants.textFooterColor
                                        context.fill();
                                    }
                                }
                                contentItem: Text {
                                    leftPadding: 20
                                    text: fitlerFiles.displayText
                                    font.family: UIConstants.customFont
                                    color: UIConstants.textFooterColor
                                    verticalAlignment: Text.AlignVCenter
                                    font.pixelSize: 13
                                }
                                background: Rectangle {
                                    anchors.fill: parent
                                    border.width: 1
                                    border.color: UIConstants.sidebarBorderColor
                                    color: UIConstants.transparentColor
                                }
                                popup: Popup {
                                    y: fitlerFiles.height
                                    width: fitlerFiles.width
                                    implicitHeight: contentItem.implicitHeight
                                    padding: 5
                                    contentItem: ListView {
                                        clip: true
                                        implicitHeight: contentHeight
                                        width: fitlerFiles.width
                                        model: fitlerFiles.popup.visible ? fitlerFiles.delegateModel : null
                                        currentIndex: fitlerFiles.highlightedIndex
                                        ScrollIndicator.vertical: ScrollIndicator { }
                                    }
                                    background: Rectangle {
                                        color: UIConstants.bgColorOverlay
                                        border { color: UIConstants.sidebarBorderColor; width: 1 }
                                        radius: 1
                                        opacity: .5
                                    }
                                }
                            }*/

                            //---- Border bottom
                            RectBorder {
                                type: "bottom"
                                color: UIConstants.categoryCirColor
                                thick: 3
                            }
                        }

                        //---------- List of files (Content)
                        //-----Files Display
                        ListView {
                            id: listOfFiles
                            anchors { top: infosAndFilter.bottom; left: parent.left; right: parent.right }
                            Layout.preferredHeight: filesModel.count * 40
                            model: ListModel {
                                id: filesModel
                                Component.onCompleted: {
                                    append({ _id: "1", _fileName: "2019:02:28-15:40:30"});
                                    append({ _id: "2", _fileName: "2019:02:28-15:40:31"});
                                }
                            }
                            delegate: ListFilesItem {
                                width: parent.width
                                height: 40
                                fileName_: _fileName
                                sequence_: index + 1
                                onItemClicked: {
                                    fileName.text = fileNameClicked;
                                }
                            }
                        }
                        //-----Loading Files Overlay
                        ProcessingOverlay {
                            id: filesLoadingOverlay
                            anchors.fill: listOfFiles
                            z: 10
                            opacity: 0
                            textOverlay: ""
                            background: UIConstants.cfProcessingOverlayBg
                            transparentLevel: .5
                            MouseArea {
                                anchors.fill: parent
                                hoverEnabled: true
                            }
                        }

                        //---------- Pagination (Footer)
                        Rectangle {
                            anchors { top: listOfFiles.bottom; topMargin: 5; left: parent.left; right: parent.right }
                            Layout.preferredHeight: parent.height / 14
                            color: UIConstants.transparentColor
                            RowLayout {
                                width: parent.width / 4
                                height: parent.height
                                anchors.right: parent.right
                                FlatButton {
                                    id: btnBackward
                                    anchors.right: btnForward.left
                                    anchors.rightMargin: 5
                                    Layout.preferredWidth: parent.width / 4
                                    Layout.preferredHeight: parent.height * 2 / 3
                                    anchors.verticalCenter: parent.verticalCenter
                                    btnText: UIConstants.iDoubleLeft
                                    btnTextColor: UIConstants.textFooterColor
                                    btnBgColor: UIConstants.cfProcessingOverlayBg
                                    gradient: Gradient {
                                        GradientStop { position: 0.0; color: UIConstants.dropshadowColor }
                                        GradientStop { position: 0.8; color: UIConstants.categoryEleBgColor }
                                        GradientStop { position: 1.0; color: UIConstants.categoryEleBgColor }
                                    }
                                    onClicked: {
                                        filesLoadingOverlay.opacity = 1;
                                    }
                                }

                                FlatButton {
                                    id: btnForward
                                    Layout.preferredWidth: parent.width / 4
                                    Layout.preferredHeight: parent.height * 2 / 3
                                    anchors.verticalCenter: parent.verticalCenter
                                    btnText: UIConstants.iDoubleRight
                                    btnTextColor: UIConstants.textFooterColor
                                    btnBgColor: UIConstants.cfProcessingOverlayBg
                                    gradient: Gradient {
                                        GradientStop { position: 0.0; color: UIConstants.dropshadowColor }
                                        GradientStop { position: 0.8; color: UIConstants.categoryEleBgColor }
                                        GradientStop { position: 1.0; color: UIConstants.categoryEleBgColor }
                                    }
                                    onClicked: {
                                        filesLoadingOverlay.opacity = 1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Behavior on opacity {
                NumberAnimation {
                    duration: 500
                    easing.type: Easing.InCubic
                }
            }
            Behavior on Layout.preferredWidth {
                NumberAnimation {
                    duration: 600
                    easing.type: Easing.InCubic
                }
            }

            Rectangle {
                id: calendarWrapper
                anchors.top: parent.top
                anchors.topMargin: parent.height / 7 + parent.height / 14
                anchors.left: parent.left
                anchors.leftMargin: 2
                width: parent.width - 2
                height: 300
                z: 100
                opacity: 0
                MouseArea {
                    anchors.fill: parent
                    visible: parent.opacity == 0 ? false : true
                    hoverEnabled: true
                }
                Calendar{
                    id: calendar
                    anchors.fill: parent
                    minimumDate: new Date(2019, 0, 1)
                    maximumDate: new Date()
                    frameVisible: false
                    style: CalendarStyle {
                        gridVisible: false
                        background: Rectangle {
                            color: UIConstants.sidebarConfigBg
                        }
                        navigationBar: Rectangle {
                            height: 50
                            width: parent.width
                            color: UIConstants.sidebarBgItemLevel2
                            opacity: 0.5

                            Text {
                                anchors.left: parent.left
                                anchors.leftMargin: 10
                                anchors.verticalCenter: parent.verticalCenter
                                text: UIConstants.iCaretLeft
                                font.pixelSize: 20
                                font.family: "FontAwesome"
                                color: UIConstants.textColor
                                MouseArea {
                                    anchors.fill: parent
                                    onClicked: control.showPreviousMonth()
                                }
                            }

                            Label {
                                id: dateText
                                anchors.centerIn: parent
                                text: styleData.title
                                font.pixelSize: 14
                                font.family: UIConstants.customFont
                                color: UIConstants.textColor
                                font.bold: true
                            }

                            Text {
                                anchors.right: parent.right
                                anchors.rightMargin: 10
                                anchors.verticalCenter: parent.verticalCenter
                                text: UIConstants.iCaretRight
                                font.pixelSize: 20
                                font.family: "FontAwesome"
                                color: UIConstants.textColor
                                MouseArea {
                                    anchors.fill: parent
                                    onClicked: control.showNextMonth()
                                }
                            }
                        }

                        dayOfWeekDelegate: Rectangle {
                            visible: false
                        }

                        dayDelegate: Rectangle {
                            color: styleData.date !== undefined && styleData.selected ? "#0984e3" : "transparent"
                            RectBorder {
                                type: "right"
                                color: UIConstants.dropshadowColor
                                thick: 1
                            }
                            RectBorder {
                                type: "bottom"
                                color: UIConstants.dropshadowColor
                                thick: 1
                            }
                            Label {
                               text: styleData.date.getDate()
                               anchors.centerIn: parent
                               color: styleData.valid ? UIConstants.textFooterColor : UIConstants.iConOff
                               font.pixelSize: 12
                               font.family: UIConstants.customFont
                            }

                        }

                    }
                    onClicked: {
                        calendarWrapper.opacity = 0;
                        currentDate.text = Qt.formatDateTime(date, "dd-MM-yyyy");
                        filesLoadingOverlay.opacity = 1;
                    }
                }
            }
        }

        //---------- Display video area
        Rectangle {
            id: playbackDisplay
            color: UIConstants.transparentColor
            Layout.preferredWidth: parent.width * 4 / 5
            Layout.preferredHeight: parent.height
            //---Border
            RectBorder {
                id: playbackDisplayBorder
                type: "left"
                thick: 3
                color: UIConstants.sidebarBorderColor
            }

            //---Content
            ColumnLayout {
                anchors.fill: parent

                //----- Video Output
                Rectangle {
                    Layout.preferredWidth: parent.width
                    Layout.preferredHeight: parent.height * 9 / 10
                    anchors.top: parent.top
                    color: UIConstants.cfProcessingOverlayBg
                    opacity: .7
                    Text {
                        id: noVideo
                        text: "\uf4e2"
                        font{ pixelSize: 35; bold: true; family: UIConstants.customFont }
                        anchors.centerIn: parent
                        color: UIConstants.textFooterColor
                    }
                }

                //----- Control pane
                Rectangle {
                    id: controlPane
                    Layout.preferredWidth: parent.width
                    Layout.preferredHeight: parent.height / 10
                    opacity: .6
                    color: UIConstants.transparentColor
                    // --- Slider
                    PlaybackSlider {
                        id: customSlider
                        width: parent.width
                        height: 7
                        anchors { left: parent.left; top: parent.top; topMargin: 5 }
                        startTime: 3000
                        endTime: 3500
                        stepSize: 2
                    }

                    // Pause/Playing + Speed + Filename
                    Rectangle {
                        id: controlPaneAdjust
                        anchors {top: customSlider.bottom; left: parent.left; bottom: parent.bottom}
                        width: parent.width
                        color: UIConstants.transparentColor

                        Row {
                            id: controlLeft
                            width: parent.width / 3
                            height: parent.height
                            x: 15
                            spacing: 20
                            // Pause/ Playing
                            Row {
                                height: parent.height
                                width: childrenRect.width
                                spacing: 7

                                Rectangle {
                                    id: decreaseBtn
                                    height: parent.height / 3
                                    width: height
                                    anchors.verticalCenter: parent.verticalCenter
                                    border.width: 1
                                    border.color: UIConstants.dropshadowColor
                                    radius: 2
                                    color: UIConstants.transparentColor
                                    Text {
                                        text: UIConstants.iChevronLeft
                                        font.pixelSize: 12
                                        font.family: "FontAwesome"
                                        color: UIConstants.textFooterColor
                                        anchors.centerIn: parent

                                    }
                                    MouseArea {
                                        anchors.fill: parent
                                        visible: customSlider.endTime > 0
                                        onPressed: {
                                            parent.opacity = 0.6;
                                            controlPane.focus = true;
                                            customSlider.decrease();
                                        }
                                        onReleased: parent.opacity = 1
                                    }
                                }

                                Rectangle {
                                    height: parent.height / 3
                                    width: height
                                    anchors.verticalCenter: parent.verticalCenter
                                    border.width: 1
                                    border.color: UIConstants.dropshadowColor
                                    radius: 2
                                    color: UIConstants.transparentColor
                                    Text {
                                        id: pausePlaying
                                        anchors.left: parent.left
                                        text: UIConstants.iPlayState
                                        font.pixelSize: 12
                                        color: UIConstants.textFooterColor
                                        anchors.centerIn: parent
                                        MouseArea {
                                            anchors.fill: parent
                                            enabled: true
                                            hoverEnabled: true
                                            onClicked: {
                                                parent.text = (parent.text == UIConstants.iPlayState)
                                                            ? UIConstants.iStopState : UIConstants.iPlayState
                                            }
                                        }
                                    }
                                }

                                Rectangle {
                                    id: increaseBtn
                                    height: parent.height / 3
                                    width: height
                                    anchors.verticalCenter: parent.verticalCenter
                                    border.width: 1
                                    border.color: UIConstants.dropshadowColor
                                    radius: 2
                                    color: UIConstants.transparentColor
                                    Text {
                                        text: UIConstants.iChevronRight
                                        font.pixelSize: 12
                                        font.family: "FontAwesome"
                                        color: UIConstants.textFooterColor
                                        anchors.centerIn: parent

                                    }
                                    MouseArea {
                                        anchors.fill: parent
                                        visible: customSlider.endTime > 0
                                        onPressed: {
                                            parent.opacity = 0.6;
                                            controlPane.focus = true;
                                            customSlider.increase();
                                        }
                                        onReleased: parent.opacity = 1
                                    }
                                }
                            }

                            // Speed adjusting
                            CustomComboBox {
                                id: speedAdjusting
                                width: 50
                                height: parent.height / 3
                                anchors.verticalCenter: parent.verticalCenter
                                model_: ["0.1x", "0.3x", "0.6x", "1.0x"]
                            }

                        }

                        // FileName
                        Rectangle {
                            anchors.left: controlLeft.right
                            width: parent.width / 3
                            height: parent.height
                            color: UIConstants.transparentColor
                            Text {
                                id: previousFile
                                text: UIConstants.iDoubleLeft
                                font.pixelSize: 15
                                font.family: "FontAwesome"
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.right: fileName.left
                                anchors.rightMargin: 10
                                color: UIConstants.textFooterColor
                                visible: fileName.text != "N/A"
                                MouseArea {
                                    anchors.fill: parent
                                    onPressed: parent.opacity = 0.6
                                    onReleased: parent.opacity = 1
                                }
                            }

                            Text {
                                id: fileName
                                text: "N/A"
                                color: UIConstants.textFooterColor
                                font { family: UIConstants.customFont; bold: true; pixelSize: 14 }
                                anchors.centerIn: parent
                            }

                            Text {
                                id: nextFile
                                text: UIConstants.iDoubleRight
                                font.pixelSize: 15
                                font.family: "FontAwesome"
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.left: fileName.right
                                anchors.leftMargin: 10
                                color: UIConstants.textFooterColor
                                visible: fileName.text != "N/A"
                                MouseArea {
                                    anchors.fill: parent
                                    onPressed: parent.opacity = 0.6
                                    onReleased: parent.opacity = 1
                                }
                            }
                        }

                        Rectangle {
                            id: controlRight
                            anchors.right: parent.right
                            width: parent.width / 3
                            height: parent.height
                            color: UIConstants.transparentColor
                            Row {
                                height: parent.height
                                width: childrenRect.width
                                anchors.right: parent.right
                                anchors.rightMargin: 10
                                spacing: 10

                                Rectangle {
                                    id: videoExport
                                    height: parent.height / 3
                                    width: height
                                    anchors.verticalCenter: parent.verticalCenter
                                    border.width: 1
                                    border.color: UIConstants.dropshadowColor
                                    radius: 2
                                    color: UIConstants.transparentColor
                                    Text {
                                        text: UIConstants.iFileDownload
                                        font.pixelSize: 12
                                        font.family: "FontAwesome"
                                        color: UIConstants.textFooterColor
                                        anchors.centerIn: parent

                                    }
                                }

                                Rectangle {
                                    id: fullScreen
                                    height: parent.height / 3
                                    width: height
                                    anchors.verticalCenter: parent.verticalCenter
                                    border.width: 1
                                    border.color: UIConstants.dropshadowColor
                                    radius: 2
                                    color: UIConstants.transparentColor
                                    Text {
                                        text: UIConstants.iOpenFullScreen
                                        font.pixelSize: 12
                                        font.family: "FontAwesome"
                                        color: UIConstants.textFooterColor
                                        anchors.centerIn: parent

                                    }
                                }

                                Rectangle {
                                    height: parent.height / 3
                                    width: height
                                    anchors.verticalCenter: parent.verticalCenter
                                    border.width: 1
                                    border.color: UIConstants.dropshadowColor
                                    radius: 2
                                    color: UIConstants.transparentColor
                                    Text {
                                        id: voice
                                        anchors.left: parent.left
                                        text: UIConstants.iVoiceControl
                                        font.pixelSize: 12
                                        color: UIConstants.textFooterColor
                                        anchors.centerIn: parent
                                    }
                                    MouseArea {
                                        anchors.fill: parent
                                        onPressed: parent.opacity = 0.6;
                                        onReleased: parent.opacity = 1;
                                    }
                                }
                            }
                        }
                    }

                    Keys.onPressed:  {
                        if (event.key == Qt.Key_Left) {
                            decreaseBtn.opacity = 0.6;
                            customSlider.decrease();
                        }

                        if (event.key == Qt.Key_Right) {
                            increaseBtn.opacity = 0.6;
                            customSlider.increase();
                        }
                    }

                    Keys.onReleased: {
                            decreaseBtn.opacity = 1;
                            increaseBtn.opacity = 1;
                    }
                }
            }

            Behavior on Layout.preferredWidth {
                NumberAnimation {
                    duration: 500;
                    easing.type: Easing.InOutQuad
                }
            }
        }
    }
}
