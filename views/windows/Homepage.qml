/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component: Content partial for homepage
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 16/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//----------------- Include Custom views --------------------------------------
import UIConstants 1.0
import "../components"
import "../partials"

Item {
    id: homepage
    signal navigatedTo(real _id)
    Row {
        anchors.fill: parent
        anchors.leftMargin: parent.width / 20
        spacing: parent.width / 25
        Repeater {
            model: ListModel {
                //---- Solution 1
                // ListElement { id_: 1; name_: "Xem trực tuyến"; iconTxt_: UIConstants.iLiveView }
                // ListElement { id_: 2; name_: "Xem lại"; iconTxt_: UIConstants.iPlayback }
                // ListElement { id_: 3; name_: "Quản lý thiết bị"; iconTxt_: UIConstants.iDeviceMgmt }
                // ListElement { id_: 4; name_: "Cấu hình chung"; iconTxt_: UIConstants.iGeneralConfigs }
                //---- Solution 2
                Component.onCompleted: {
                    append({ id_: 1, name_: "Xem trực tuyến", iconTxt_: UIConstants.iLiveView,
                             cateDesc_: "Quan sát trực tiếp quá trình truyền nhận hình ảnh từ tàu bia về sở chỉ huy", loadingDuration_: 1000 });
                    append({ id_: 2, name_: "Xem lại", iconTxt_: UIConstants.iPlayback,
                             cateDesc_: "Xem lại các video được lưu trữ tại các cameras ở tàu bia", loadingDuration_: 1600 });
                    append({id_: 3, name_: "Quản lý thiết bị", iconTxt_: UIConstants.iDeviceMgmt,
                             cateDesc_: "Tìm kiếm, kết nối, quản lý các cameras tại tàu bia", loadingDuration_: 2200 });
                    append({id_: 4, name_: "Cấu hình chung", iconTxt_: UIConstants.iGeneralConfigs,
                             cateDesc_: "Một số cấu hình chung cho toàn bộ hệ thống từ phần mềm SCH và các cameras tại tàu bia", loadingDuration_: 3000 });
                }
            }
            CategoryElement {
                iconTxt: iconTxt_
                cateTitle: name_
                cateDesc: cateDesc_
                loadingDuration: loadingDuration_
                cateId: id_
                onChoosed: {
                    homepage.navigatedTo(_id);
                }
            }
        }
    }
}

