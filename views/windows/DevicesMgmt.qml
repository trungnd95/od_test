/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Module: Device Mgmt view
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 13/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//----------------- Include Custom views --------------------------------------
import UIConstants 1.0
import "../components"
import "../partials"

Item {
    id: deviceMgmt
    //---- Properties
    property var sortedColumn
    property bool inverted: false
    property int devices: 2

    RowLayout {
        id: deviceMgmtLayout
        anchors.fill: parent

        //--------- Sidebar
        SidebarDeviceMgmt {
            id: sidebarDeviceMgmt
            Layout.preferredWidth: parent.width / 5
            Layout.preferredHeight: parent.height
            onRequestSearchDevices: {
                findingCamsOverlay.visible = true;
            }
        }

        //--------- Devices
        Rectangle {
            id: devices
            Layout.preferredHeight: parent.height
            anchors {left: sidebarDeviceMgmt.right; right: parent.right; top: parent.top }
            color: UIConstants.transparentColor
            //---Border
            RectBorder {
                id: playbackDisplayBorder
                type: "left"
                thick: 3
                color: UIConstants.sidebarBorderColor
            }

            //--- Title
            SidebarTitle {
                id: title
                anchors { top: parent.top; left: parent.left; right: parent.right }
                height: parent.height / 14
                visible: true
                title: "Danh sách thiết bị"
                xPosition: 20
            }

            //---Table List Devices
            Rectangle {
                id: tableListDevices
                anchors { left: parent.left; top: title.bottom; right: parent.right; bottom: parent.bottom }
                color: UIConstants.transparentColor
                z: 1
                CustomTable {
                    id: tableId
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width - 60

                    columns: [
                        DataColumn {field:"id_"; label: "ID"; type: "number"},
                        DataColumn {field:"name_"; label: "Tên Camera"},
                        DataColumn {field:"camIp_"; label: "Địa chỉ IP"},
                        DataColumn {field:"rtspLink_"; label: "Đường truyền hình ảnh"},
//                        DataColumn {field:"status_"; label: "Trạng thái"; type: "number"},
//                        DataColumn {field:"input_"; label: "Nguồn"},
//                        DataColumn {field:"infos_"; label: "Thông số"},
                        DataColumn {field:"action_"; label: "Hành động";
                                    cell: Actions{
                                              onEdited: {
                                                  deviceEditingModal.opacity = 1;
                                                  deviceEditingModal.setModalAppear();
                                              }

                                              onDeleted: {
                                                  deviceDeletingModal.opacity = 1;
                                                  deviceDeletingModal.setModalAppear();
                                                  deviceDeletingModal.rowId_ = rowId;
                                              }
                                          }
                                    }
                    ]
                    rows: getTableContent()
                    onCellClicked: print("clicked cell:" + column.field + "|" + row.name)
                    columnHeader: Row {
                        property var column
                        Text {
                            text: sortedColumn !== column.field ? " " : (inverted ? "\u25B2" : "\u25BC")
                        }
                        Label {
                            text: column.label || column.field
                            opacity: .8
                            color: UIConstants.tableHeaderColor
                            font.bold: true
                        }
                    }
                }
            }

            ProcessingOverlay {
                id: findingCamsOverlay
                anchors.fill: parent
                textOverlay: "Đang tìm kiếm thiết bị"
                //opacity: 0
                visible: false
                z: 100
                Behavior on opacity {
                    NumberAnimation {
                        duration: 500
                        easing.type: Easing.InExpo
                    }
                }
                MouseArea {
                    anchors.fill: parent
                }
            }
        }
    }

    //----- Add new button
    Rectangle {
        width: 45
        height: 45
        radius: 45
        color: UIConstants.liveViewGridHighlight
        anchors { bottom: parent.bottom; right: parent.right; rightMargin: 13; bottomMargin: 13 }
        Text {
            text: "\uf067"
            font.pixelSize: 20
            font.family: "FontAwesome"
            anchors.centerIn: parent
            color: "#fff"
        }

        MouseArea {
            anchors.fill: parent
            onPressed: parent.opacity = 0.7
            onClicked: {
                deviceAddNewModal.opacity = 1;
                deviceAddNewModal.setModalAppear();
            }

            onExited: parent.opacity = 1
        }
    }

    //----- Modals
    DeviceEditingModal {
        id: deviceEditingModal
        width: parent.width
        height: parent.height + 100
        parent: ApplicationWindow.contentItem
        opacity: 0
        Behavior on opacity {
            NumberAnimation {
                duration: 200
                easing.type: Easing.InOutBack
            }
        }
    }

    ConfirmDeleteModal {
        id: deviceDeletingModal
        width: parent.width
        height: parent.height + 100
        parent: ApplicationWindow.contentItem
        opacity: 0
        Behavior on opacity {
            NumberAnimation {
                duration: 200
                easing.type: Easing.InOutBack
            }
        }

        onConfirmDeleted: {
            tableId.rows.splice( tableId.rows.indexOf(tableId.rows.find(function(row) {return Number(row.id_) === rowId; })), 1 );
            tableId.deleteRow(rowId);
        }
    }

    DeviceAddNew {
        id: deviceAddNewModal
        width: parent.width
        height: parent.height + 100
        parent: ApplicationWindow.contentItem
        opacity: 0
        Behavior on opacity {
            NumberAnimation {
                duration: 200
                easing.type: Easing.InOutBack
            }
        }
    }

    function getTableContent() {
        var rows = [];
        for (var i = 0; i < deviceMgmt.devices; i ++) {
            rows.push({id_: "1", name_: "Cam Trước", camIp_: "192.168.1.112", rtspLink_: "rtsp://172.20.100.93:8554/stream",
                          action_: [{text: "\uf044", color: "#fdcb6e" }, {text: "\uf1f8", color: "#fab1a0" }]});
        }
        return rows;
    }
}
