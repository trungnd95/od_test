/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Partial: Sidebar of Device Management page
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 02/03/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//----------------- Include Custom views --------------------------------------
import UIConstants 1.0
import "../components"

Item {
    id: sidebarDeviceMgmt

    //------------- Signals
    signal requestSearchDevices()

    //-------------- Sidebar title
    SidebarTitle {
        id: sidebarTitle
        anchors { top: parent.top; left: parent.left; right: parent.right }
        height: parent.height / 14
        visible: true
        title: "Tìm kiếm thiết bị"
        iconType: UIConstants.iSearch
    }

    //------------- Search area
    Column {
        id: columnSearchArea
        anchors {top: sidebarTitle.bottom; topMargin: 20; left: parent.left; right: parent.right; leftMargin: 20; rightMargin: 5 }
        height: 130
        Repeater {
            id: ipsForm
            model: ListModel {
                Component.onCompleted: {
                    append({ids_: 0, title_: "Từ"});
                    append({ids_: 1, title_: "Đến"});
                }
            }
            IpAddressForm {
                width: parent.width
                height: 50
                titleText: title_
                ids: ids_
                onTyping: {
                    var targetItem = (stt == 0) ? ++stt : --stt;
                    switch( columnNumber )
                    {
                        case 1:
                            ipsForm.itemAt(targetItem).ip1stTxt = value;break;
                        case 2:
                            ipsForm.itemAt(targetItem).ip2stTxt = value;break;
                        case 3:
                            ipsForm.itemAt(targetItem).ip3stTxt = value;break;
                    }
                }
            }
        }
    }

    //------------ Bottom: search button
    Rectangle {
        anchors { top: columnSearchArea.bottom; topMargin: 15; horizontalCenter: parent.horizontalCenter }
        width: parent.width * 2 / 3
        height: 60
        color: UIConstants.transparentColor
        RectBorder {}
        FlatButton {
            anchors { left: parent.left; right: parent.right; bottom: parent.bottom }
            height: 40
            btnText: "Gửi"
            btnBgColor: UIConstants.bgColorOverlay
            btnTextColor: UIConstants.textFooterColor
            radius: 20
            onClicked: {
                sidebarDeviceMgmt.requestSearchDevices();
            }
        }
    }
}
