/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Partial:
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 13/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//---------------- Include custom libs ----------------------------------------
import "../components"
import UIConstants 1.0

Item {
    id: rootItem

    //--- Title
    SidebarTitle {
        id: title
        anchors { top: parent.top; left: parent.left; right: parent.right }
        height: parent.height / 14
        visible: true
        title: "Cấu hình tính năng lưu trữ cho hệ thống"
        iconType: "\uf197"
        xPosition: 20
    }

    //--- Config board
    Rectangle {
        id: configBoard
        anchors { left: parent.left; top: title.bottom; right: parent.right; bottom: parent.bottom }
        color: UIConstants.transparentColor
        RowLayout {
            anchors.fill: parent
            anchors.margins: 40
            spacing: 10
            //--- Device Side
            Rectangle {
                id: storageDeviceSide
                Layout.preferredWidth: parent.width / 2
                Layout.preferredHeight: parent.height
                color: UIConstants.transparentColor

                //---- Figure Label
                Text {
                    id: figureText
                    anchors { left: parent.left; leftMargin: 30;
                              bottom: parent.top; bottomMargin: -7 }
                    text: "Trên thiết bị"
                    font { family: UIConstants.customFont; pixelSize: 16 }
                    color: UIConstants.textFooterColor
                }

                //---- Figure Border
                Canvas {
                    anchors.fill: parent
                    onPaint: {
                        var ctx = getContext("2d");
                        ctx.lineWidth = 2;
                        ctx.strokeStyle = "#57606f"
                        ctx.beginPath();
                        ctx.moveTo(25, 0);
                        ctx.lineTo(0, 0);
                        ctx.moveTo(0, 0);
                        ctx.lineTo(0, height);
                        ctx.moveTo(0, height);
                        ctx.lineTo(width, height);
                        ctx.moveTo(width, height);
                        ctx.lineTo(width, 0);
                        ctx.moveTo(width, 0);
                        ctx.lineTo(160, 0 );
                        ctx.closePath();
                        ctx.stroke();
                    }
                }

                //---- Configs Board
                GridLayout {
                    id: gridDeviceStorageConfigs
                    rows: 3
                    columns: 2
                    rowSpacing: 25
                    width: parent.width - 50
                    height: childrenRect.height
                    anchors{ horizontalCenter: parent.horizontalCenter; top: parent.top;
                            topMargin: 50 }
                    //--- Compress standard
                    CustomComboBox {
                        Layout.rowSpan: 1
                        Layout.columnSpan: 1
                        Layout.preferredHeight: 60
                        Layout.preferredWidth: parent.width / 2 - 5
                        titleTxt: "Chuẩn nén"
                        textColor_: UIConstants.textFooterColor
                        labelTextColor: UIConstants.tableHeaderColor
                        icon: "\uf1c6"
                        popupBackground: UIConstants.sidebarActiveBg
                        model_: ["H264", "H265"]
                    }

                    //--- Type files
                    CustomComboBox {
                        Layout.rowSpan: 1
                        Layout.columnSpan: 1
                        Layout.preferredHeight: 60
                        Layout.preferredWidth: parent.width / 2 - 5
                        titleTxt: "Định dạng file"
                        textColor_: UIConstants.textFooterColor
                        labelTextColor: UIConstants.tableHeaderColor
                        icon: "\uf15b"
                        popupBackground: UIConstants.sidebarActiveBg
                        model_: ["mp4", "mkv", "ts"]
                    }

                    //--- Saving types
                    Rectangle {
                        Layout.rowSpan: 1
                        Layout.columnSpan: 2
                        Layout.preferredHeight: 60
                        Layout.preferredWidth: parent.width - 5
                        color: UIConstants.transparentColor
                        RowLayout {
                            anchors.fill: parent
                            spacing: 10
                            Text {
                                id: savingTypeLabel
                                text: "Kiểu lưu: "
                                font{ pixelSize: 13; family: UIConstants.customFont;
                                    bold: true }
                                color: UIConstants.tableHeaderColor
                                anchors.verticalCenter: parent.verticalCenter
                            }
                            CustomRadioGroup {
                                anchors{ left: savingTypeLabel.right; leftMargin: 10; verticalCenter: parent.verticalCenter }
                                radioModel: ListModel {
                                    ListElement{ id_: 1; checked_: true; label_: "Gộp file" }
                                    ListElement{ id_: 2; checked_: false; label_: "Tách file" }
                                }
                                activeRadioColor: UIConstants.tableHeaderColor
                                radioBtnSize: 18
                            }
                        }
                    }

                    //--- Compress standard
                    CustomComboBox {
                        Layout.rowSpan: 1
                        Layout.columnSpan: 1
                        Layout.preferredHeight: 60
                        Layout.preferredWidth: parent.width / 2 - 5
                        titleTxt: "Tách theo: "
                        textColor_: UIConstants.textFooterColor
                        labelTextColor: UIConstants.tableHeaderColor
                        icon: "\uf529"
                        popupBackground: UIConstants.sidebarActiveBg
                        model_: ["Thời gian", "Dung lượng"]
                    }

                    //--- Streaming Host
                    CustomTextField {
                        id: streamingHost
                        Layout.rowSpan: 1
                        Layout.columnSpan: 1
                        Layout.preferredHeight: 60
                        Layout.preferredWidth: parent.width / 2 - 5
                        dynamicValidator: IntValidator{bottom: 1; top: 1000}
                        titleTxt: "Cỡ"
                        inputColor: UIConstants.textFooterColor
                        labelColor: UIConstants.tableHeaderColor
                        textFieldPlaceholder: "..."
                        icon: "\uf6ff"
                    }
                }

                //---- Divider
                Rectangle {
                    id: divider
                    anchors{ top: gridDeviceStorageConfigs.bottom; horizontalCenter: parent.horizontalCenter
                             topMargin: 50 }
                    width: parent.width / 2
                    height: 1
                    color: UIConstants.sidebarBorderColor
                }

                //---- Buttons Action
                Rectangle {
                    anchors{ top: divider.bottom;
                        horizontalCenter: parent.horizontalCenter }
                    width: parent.width / 2
                    height: 100
                    color: UIConstants.transparentColor
                    FlatButton {
                        id: saveBtn
                        width: parent.width / 2
                        height: parent.height / 2
                        anchors { verticalCenter: parent.verticalCenter;
                                  right: resetBtn.left; rightMargin: 10 }
                        btnText: "Lưu lại"
                        btnTextColor: UIConstants.textColor
                        btnBgColor: UIConstants.borderGreen
                        radius: 5
                        iconVisible: true
                        icon: UIConstants.iSave
                    }
                    FlatButton {
                        id: resetBtn
                        width: parent.width / 2
                        height: parent.height / 2
                        anchors { verticalCenter: parent.verticalCenter;
                                 right: parent.right; }
                        btnText: "Đặt lại"
                        btnTextColor: UIConstants.textColor
                        btnBgColor: "#57606f"
                        radius: 5
                        iconVisible: true
                    }
                }
            }

            //--- Local side
            Rectangle {
                id: storageLocalSide
                Layout.preferredWidth: parent.width / 2
                Layout.preferredHeight: parent.height
                color: UIConstants.transparentColor

                //---- Figure label
                Text {
                    id: figureText1
                    anchors { left: parent.left; leftMargin: 30;
                              bottom: parent.top; bottomMargin: -7 }
                    text: "Tại phần mềm quan sát"
                    font { family: UIConstants.customFont; pixelSize: 16 }
                    color: UIConstants.textFooterColor
                }

                //---- Figure border
                Canvas {
                    anchors.fill: parent
                    onPaint: {
                        var ctx = getContext("2d");
                        ctx.lineWidth = 2;
                        ctx.strokeStyle = "#57606f"
                        ctx.beginPath();
                        ctx.moveTo(25, 0);
                        ctx.lineTo(0, 0);
                        ctx.moveTo(0, 0);
                        ctx.lineTo(0, height);
                        ctx.moveTo(0, height);
                        ctx.lineTo(width, height);
                        ctx.moveTo(width, height);
                        ctx.lineTo(width, 0);
                        ctx.moveTo(width, 0);
                        ctx.lineTo(230, 0 );
                        ctx.closePath();
                        ctx.stroke();
                    }
                }

                //---- Config Board
                GridLayout {
                    id: gridLocalStorageConfigs
                    rows: 1
                    columns: 2
                    rowSpacing: 25
                    width: parent.width - 50
                    height: childrenRect.height
                    anchors{ horizontalCenter: parent.horizontalCenter; top: parent.top;
                            topMargin: 50 }
                    //--- Compress standard
                    CustomComboBox {
                        Layout.rowSpan: 1
                        Layout.columnSpan: 1
                        Layout.preferredHeight: 60
                        Layout.preferredWidth: parent.width / 2 - 5
                        titleTxt: "Chuẩn nén"
                        textColor_: UIConstants.textFooterColor
                        labelTextColor: UIConstants.tableHeaderColor
                        icon: "\uf1c6"
                        popupBackground: UIConstants.sidebarActiveBg
                        model_: ["H264", "H265"]
                    }

                    //--- Type files
                    CustomComboBox {
                        Layout.rowSpan: 1
                        Layout.columnSpan: 1
                        Layout.preferredHeight: 60
                        Layout.preferredWidth: parent.width / 2 - 5
                        titleTxt: "Định dạng file"
                        textColor_: UIConstants.textFooterColor
                        labelTextColor: UIConstants.tableHeaderColor
                        icon: "\uf15b"
                        popupBackground: UIConstants.sidebarActiveBg
                        model_: ["mp4", "mkv", "ts"]
                    }
                }

                //---- Divider
                Rectangle {
                    id: divider1
                    anchors{ top: gridLocalStorageConfigs.bottom; horizontalCenter: parent.horizontalCenter
                             topMargin: 50 }
                    width: parent.width / 2
                    height: 1
                    color: UIConstants.sidebarBorderColor
                }

                //---- Buttons Action
                Rectangle {
                    anchors{ top: divider1.bottom;
                        horizontalCenter: parent.horizontalCenter }
                    width: parent.width / 2
                    height: 100
                    color: UIConstants.transparentColor
                    FlatButton {
                        id: saveBtn1
                        width: parent.width / 2
                        height: parent.height / 2
                        anchors { verticalCenter: parent.verticalCenter;
                                  right: resetBtn1.left; rightMargin: 10 }
                        btnText: "Lưu lại"
                        btnTextColor: UIConstants.textColor
                        btnBgColor: UIConstants.borderGreen
                        radius: 5
                        iconVisible: true
                        icon: UIConstants.iSave
                    }
                    FlatButton {
                        id: resetBtn1
                        width: parent.width / 2
                        height: parent.height / 2
                        anchors { verticalCenter: parent.verticalCenter;
                                 right: parent.right; }
                        btnText: "Đặt lại"
                        btnTextColor: UIConstants.textColor
                        btnBgColor: "#57606f"
                        radius: 5
                        iconVisible: true
                    }
                }
            }
        }
    }
}
