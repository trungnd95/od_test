/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component: Sidebar Element
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 25/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import UIConstants 1.0

//----------------- Include custom component ---------------------------------
import "../components"

//---------------- Component definition ----------------------------------------
Item {
    id: sidebarNav
    width: parent.width
    height: navLevel1.height + navLevel2.height
    z: 10
    clip: true
    //------------- Element property
    property string  _iconForNavEl
    property string  _navContent
    property var     _navLevel2Content
    property int     _id
    property int     _countTrigger: 0
    property bool    _liveViewPage: false
    property string  _network

    //------------- Signal
    signal triggered()
    signal openFile()
    signal closeFile()

    //------------- Sidebar Nav level 1
    SidebarListElement {
        id: navLevel1
        //navColor: UIConstants.sidebarBgItemLevel1
        iconForNavEl: _iconForNavEl
        navContent: _navContent
        xLeft: 5
        iconRight: UIConstants.iCaretDown
        onClickedNavTrigger: {
            _countTrigger ++;
            if( _countTrigger % 2 != 0 )
            {
                navLevel2.opacity = 0;
                navLevel2.anchors.leftMargin = -navLevel2.width
            }else {
                navLevel2.opacity = 1;
                navLevel2.anchors.leftMargin = 0
            }
            sidebarNav.triggered();
        }
    }

    //----------- Sidebar nav level 1 trigger animation
    Behavior on height {
        NumberAnimation {
            duration: 500
             easing {type: Easing.Linear; overshoot: 500}
        }
    }

    //------------- Sidebar nav level 2
    Column {
        id: navLevel2
        anchors { top: navLevel1.bottom; left: parent.left; right: parent.right; }
        z: -10
        Repeater {
            id: navLevel2Repeater
            model: _navLevel2Content //--- Array of CameraItem objects
            SidebarListElement {
                id: sidebarLevel2
                //navColor: UIConstants.sidebarBgItemLevel2
                iconForNavEl: UIConstants.iDevice
                navContent: ": " + _navLevel2Content[model.index].ipAddress
                camName: _navLevel2Content[model.index].name
                navLevel: 2
                camItem: _navLevel2Content[model.index]
                xLeft: 20
                dragEnable: _liveViewPage ? true : false
                iconRight: _liveViewPage ? UIConstants.iSignal : ""
                onClickedNavTrigger: {
                    if( sidebarLevel2.iconRight == UIConstants.iChoosedRight )
                    {
                        sidebarLevel2.iconRight = "";
                        sidebarNav.closeFile();
                    }
                    else
                    {
                        sidebarLevel2.iconRight = UIConstants.iChoosedRight;
                        sidebarNav.openFile();
                    }

                    if( _liveViewPage )
                    {
                        sidebarLevel2.iconRight = UIConstants.iSignal;
                    }
                }
                Rectangle {
                    anchors.fill: parent
                    color: UIConstants.sidebarBgItemLevel2
                    opacity: .6
                }
            }
        }

        //----------- Animations
        Behavior on opacity {
            NumberAnimation {
                duration: 300
                easing { type: Easing.Linear; overshoot: 300 }
            }
        }
        Behavior on anchors.leftMargin {
            NumberAnimation {
                duration: 400
                easing {type: Easing.Linear; overshoot: 400}
            }
        }
    }
}
