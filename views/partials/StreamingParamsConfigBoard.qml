/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Partial:
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 13/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//---------------- Include custom libs ----------------------------------------
import "../components"
import UIConstants 1.0

Item {
    //--- Title
    SidebarTitle {
        id: title
        anchors { top: parent.top; left: parent.left; right: parent.right }
        height: parent.height / 14
        visible: true
        title: "Cấu hình chung các thông số streaming hình ảnh"
        iconType: "\uf197"
        xPosition: 20
    }

    //--- Streaming params config board
    Rectangle {
        id: configBoard
        anchors { left: parent.left; top: title.bottom; right: parent.right; bottom: parent.bottom }
        color: UIConstants.transparentColor
        GridLayout {
            id: gridConfigBoard
            rows: 4
            columns: 3
            width: parent.width * 2 / 3
            height: childrenRect.height
            x: parent.width / 12
            y: 50
            Rectangle {
                id: label1
                Layout.rowSpan: 1
                Layout.columnSpan: 1
                Layout.preferredWidth: parent.width / 4
                Layout.preferredHeight: 60
                color: UIConstants.transparentColor
                Label {
                    text: "Giao thức truyền thông: "
                    anchors.centerIn: parent
                    color: UIConstants.textFooterColor
                    opacity: .6
                    font.bold: true
                    font.pixelSize: 13
                }

                RectBorder {
                    type: "bottom"
                }
            }

            Rectangle {
                Layout.rowSpan: 1
                Layout.columnSpan: 2
                Layout.preferredWidth: parent.width * 3 / 4
                Layout.preferredHeight: 60
                color: UIConstants.transparentColor
                CustomRadioGroup {
                    anchors{ left: parent.left; leftMargin: 10; verticalCenter: parent.verticalCenter }
                    radioModel: ListModel {
                        ListElement{ id_: 1; checked_: true; label_: "Unicast" }
                        ListElement{ id_: 2; checked_: false; label_: "Multicast" }
                    }
                    activeRadioColor: UIConstants.tableHeaderColor
                    radioBtnSize: 18
                }
                RectBorder {
                    type: "bottom"
                }
            }

            Rectangle {
                id: label2
                Layout.rowSpan: 1
                Layout.columnSpan: 1
                Layout.preferredWidth: parent.width / 4
                Layout.preferredHeight: 60
                color: UIConstants.transparentColor
                Label {
                    text: "Băng thông: "
                    anchors.centerIn: parent
                    color: UIConstants.textFooterColor
                    opacity: .6
                    font.bold: true
                    font.pixelSize: 13
                }

                RectBorder {
                    type: "bottom"
                }
            }

            Rectangle {
                Layout.rowSpan: 1
                Layout.columnSpan: 2
                Layout.preferredWidth: parent.width * 3 / 4
                Layout.preferredHeight: 60
                color: UIConstants.transparentColor
                CustomComboBox {
                    height: parent.height
                    width: parent.width / 2 - 5
                    anchors { bottom: parent.bottom; bottomMargin: 5 }
                    textColor_: UIConstants.textFooterColor
                    popupBackground: UIConstants.sidebarActiveBg
                    model_: ["256Kb", "512Kb", "1Mb"]
                }
                RectBorder {
                    type: "bottom"
                }
            }

            Rectangle {
                id: label3
                Layout.rowSpan: 1
                Layout.columnSpan: 1
                Layout.preferredWidth: parent.width / 4
                Layout.preferredHeight: 60
                color: UIConstants.transparentColor
                Label {
                    text: "Chất lượng hình ảnh "
                    anchors.centerIn: parent
                    color: UIConstants.textFooterColor
                    opacity: .6
                    font.bold: true
                    font.pixelSize: 13
                }

                RectBorder {
                    type: "bottom"
                }
            }

            Rectangle {
                Layout.rowSpan: 1
                Layout.columnSpan: 2
                Layout.preferredWidth: parent.width * 3 / 4
                Layout.preferredHeight: 60
                color: UIConstants.transparentColor
                CustomComboBox {
                    height: parent.height
                    width: parent.width / 2 - 5
                    anchors { bottom: parent.bottom; bottomMargin: 5 }
                    textColor_: UIConstants.textFooterColor
                    popupBackground: UIConstants.sidebarActiveBg
                    model_: ["Mini-VGA", "VGA", "HD"]
                }
                RectBorder {
                    type: "bottom"
                }
            }

            Rectangle {
                id: label4
                Layout.rowSpan: 1
                Layout.columnSpan: 1
                Layout.preferredWidth: parent.width / 4
                Layout.preferredHeight: 60
                color: UIConstants.transparentColor
                Label {
                    text: "Tốc độ hình"
                    anchors.centerIn: parent
                    color: UIConstants.textFooterColor
                    opacity: .6
                    font.bold: true
                    font.pixelSize: 13
                }

                RectBorder {
                    type: "bottom"
                }
            }

            Rectangle {
                Layout.rowSpan: 1
                Layout.columnSpan: 2
                Layout.preferredWidth: parent.width * 3 / 4
                Layout.preferredHeight: 60
                color: UIConstants.transparentColor
                CustomComboBox {
                    height: parent.height
                    width: parent.width / 2 - 5
                    anchors { bottom: parent.bottom; bottomMargin: 5 }
                    textColor_: UIConstants.textFooterColor
                    popupBackground: UIConstants.sidebarActiveBg
                    model_: ["20FPS", "25FPS", "30FPS", "60FPS"]
                }
                RectBorder {
                    type: "bottom"
                }
            }
        }

        //---- Divider
        Rectangle {
            id: divider
            anchors{ top: gridConfigBoard.bottom; topMargin: 50; left: gridConfigBoard.left; right: gridConfigBoard.right  }
            width: parent.width / 2
            height: 1
            x: parent.width / 4
            color: UIConstants.sidebarBorderColor
        }

        //---- Buttons Action
        Rectangle {
            anchors{ top: divider.bottom; }
            width: parent.width / 3
            height: 100
            x: parent.width / 6
            color: UIConstants.transparentColor
            FlatButton {
                id: saveBtn1
                width: parent.width / 2
                height: parent.height / 2
                anchors { verticalCenter: parent.verticalCenter;
                          right: resetBtn1.left; rightMargin: 10 }
                btnText: "Lưu lại"
                btnTextColor: UIConstants.textColor
                btnBgColor: UIConstants.borderGreen
                radius: 5
                iconVisible: true
                icon: UIConstants.iSave
            }
            FlatButton {
                id: resetBtn1
                width: parent.width / 2
                height: parent.height / 2
                anchors { verticalCenter: parent.verticalCenter;
                         right: parent.right; }
                btnText: "Đặt lại"
                btnTextColor: UIConstants.textColor
                btnBgColor: "#57606f"
                radius: 5
                iconVisible: true
            }
        }
    }
}
