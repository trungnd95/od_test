/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Module: Footer Partial
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 13/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */


//-------------- Include Libs ----------------
import QtQuick 2.0

//-------------- Include Custom libs ---------
import UIConstants 1.0
import "../components"

Item {
    id: header
    width: parent.width
    height: parent.height / 20
    Rectangle {
        anchors.fill: parent
        color: UIConstants.bgColorOverlay
        opacity: .8
        Text {
            text: "(c) TT Uav - Viện HKVT Viettel"
            anchors.centerIn: parent
            font.bold: true
            font.family: UIConstants.customFont
            color: UIConstants.textFooterColor
        }

        Canvas {
            anchors.fill: parent
            onPaint: {
                var ctx = getContext("2d");
                ctx.strokeStyle = "#576574"
                ctx.lineWidth = 3
                ctx.beginPath();
                ctx.moveTo( 0, 0 );
                ctx.lineTo( width, 0 );
                ctx.closePath();
                ctx.stroke();
            }
        }
    }
}
