/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Partial:
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 13/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//---------------- Include custom libs ----------------------------------------
import "../components"
import UIConstants 1.0

Item {
    id: rootItem

    //--- Title
    SidebarTitle {
        id: title
        anchors { top: parent.top; left: parent.left; right: parent.right }
        height: parent.height / 14
        visible: true
        title: "Cấu hình các tham số xử lý ảnh cho tính năng phát hiện quả"
        iconType: "\uf197"
        xPosition: 20
    }

    Oops {
        id: pageError
        anchors { top: title.bottom; left: parent.left;
            right: parent.right; bottom: parent.bottom }
    }
}
