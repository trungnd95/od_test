/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Module: Header Partial
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 13/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0


//--------------Include custom libs -------------------------------------------
import UIConstants 1.0
import "../components"

//-------------- Element definition -------------------------------------------
Item {
    id: header
    //------------------------------- properties
    property bool navbarVisible: true

    //-------------------------------Signals
    signal newActiveItem( real _id )

    //------------------------------- Set Dimension
    width: parent.width
    height: parent.height / 10

    //------------------------------- Content
    Rectangle {
        id: wrapper
        z: 0
        anchors.fill: parent
        //------- Set element color
        color: UIConstants.bgColorOverlay
        opacity: .7
        gradient: Gradient {
            GradientStop { position: 0.0; color: UIConstants.bgColorOverlay }
            GradientStop { position: 0.45; color: UIConstants.sidebarBgColor }
            GradientStop { position: 1.0; color: UIConstants.sidebarBorderColor }
        }


        RowLayout {
            anchors.fill: parent
            //-------------------------------- Logo
            Rectangle {
                id: imgLogo
                width: parent.width / 8
                height: parent.height
                color: wrapper.color
                Image {
                    source: "qrc:/assets/images/VTX_logo.png"
                    width: parent.width / 2
                    height: parent.height / 2
                    anchors.centerIn: parent
                }
            }

            //-------------------------------- Header Title and Navbar
            Rectangle {
                z: 1
                anchors { left: imgLogo.right; right: parent.right; top: parent.top; bottom: parent.bottom }
                color: wrapper.color
                Text {
                    id: txtTitleHeader
                    font.capitalization: Font.AllUppercase
                    font.family: UIConstants.customFont
                    font.pixelSize: 25
                    color: UIConstants.textColor
                    text: "VTX-VTCC Object Detection Test"
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    y: parent.height / 4
                }
                DropShadow {
                    anchors.fill: txtTitleHeader
                    source: txtTitleHeader
                    verticalOffset: 2
                    color: UIConstants.headerTxtShadowColor
                    radius: 4
                    samples: 3
                }
            }
        }

        //------------------------------------ Border bottom
        Canvas {
            id: drawALine
            width: parent.width
            height: parent.height
            onPaint: {
                var ctx = getContext("2d");
                ctx.lineWidth = 1;
                ctx.strokeStyle = "#d2dae2";
                ctx.shadowColor = "#2ed5fa";
                ctx.shadowOffsetX = 2;
                ctx.shadowOffsetY = 2;
                ctx.shadowBlur = 20;
                ctx.beginPath();
                ctx.moveTo(0, parent.height);
                ctx.lineTo(parent.width, parent.height);
                ctx.closePath();
                ctx.stroke();
            }
        }
    }
}
