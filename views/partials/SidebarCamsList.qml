/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component:
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 19/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//---------------- Include custom libs ----------------------------------------
import "../components"
import "../partials"
import UIConstants 1.0

//---------------- Component definition ----------------------------------------
Item {
    id: sidebarCamsList

    //------------- Properties
    property var camsList_: feedUpSidebarCams()
    property var toggleSidebar: toggleSidebarIcon
    property alias sidebarTitle: sidebarTitle.title
    property bool liveViewPage: false

    //-------------- Signals
    signal requestOpenFile()
    signal requestCloseFile()
    signal requestToggleSidebar()
    signal gotoDeviceMgmt()

    //-------------- Sidebar title
    SidebarTitle {
        id: sidebarTitle
        anchors { top: parent.top; left: parent.left; right: parent.right }
        height: parent.height / 14
        visible: true
    }

    //--------------- Sidebar list / Sidebar content
    Column {
        anchors { top: sidebarTitle.bottom; left: parent.left; right: parent.right; bottom: parent.bottom }
        Repeater {
            id: navEles
            anchors.fill: parent
            visible: camsList_.length > 0
            model: camsList_
            SidebarNav {
                id: sidebarNav
                _iconForNavEl: camsList_[model.index].numOfDevice_
                _navContent: camsList_[model.index].network_
                _navLevel2Content: camsList_[model.index].navLevel2Content_
                _id: camsList_[model.index].id_
                _liveViewPage: liveViewPage
                height: camsList_[model.index].open_ ? ((camsList_[model.index].numOfDevice_ + 1) * 50) : 50
                onTriggered: {
                    if( camsList_[model.index].id_ < camsList_.length - 1 )
                    {
                        if( _countTrigger % 2 != 0 )
                        {
                            camsList_[model.index].open_ = false;
                            navEles.itemAt(model.index).height =  50;
                        }else {
                            camsList_[model.index].open_ = true;
                            navEles.itemAt(model.index).height = (camsList_[model.index].numOfDevice_ + 1) * 50;
                        }
                    }
                }

                onOpenFile: {
                    sidebarCamsList.requestOpenFile();
                }

                onCloseFile: {
                    sidebarCamsList.requestCloseFile();
                }
            }
        }

        Rectangle {
            color:  UIConstants.sidebarBgItemLevel2
            height: 100
            width: parent.width
            visible: camsList_.length === 0
            opacity: .7
            Column {
                width: parent.width
                height: parent.height - 20
                y: 20
                spacing: 10
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Trống ..."
                    font.pixelSize: 13
                    font.family: UIConstants.customFont
                    color: UIConstants.textFooterColor
                }

                Rectangle {
                    id: goToDeviceMgmtBtn
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width / 2
                    height: 30
                    color: "#37474f"
                    radius: 3
                    Row {
                        anchors.centerIn: parent
                        spacing: 10
                        Text {
                            id: btnText
                            text: "Tìm kiếm"
                            font.pixelSize: 13
                            font.family: UIConstants.customFont
                            color: UIConstants.textFooterColor
                        }

                        Text {
                            text: UIConstants.iRightHand
                            font.pixelSize: 13
                            font.family: "FontAwesome"
                            color: btnText.color
                            anchors.verticalCenter: parent.verticalCenter
                        }
                    }

                    Timer {
                        interval: 500
                        repeat: true
                        running: camsList_.length === 0
                        onTriggered: {
                            btnText.color = btnText.color == UIConstants.textFooterColor ? "#37474f" : UIConstants.textFooterColor
                        }
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: sidebarCamsList.gotoDeviceMgmt()
                    }
                }
            }
        }
    }


    //--------------- Toggle Icon
    Text {
        id: toggleSidebarIcon
        anchors { bottom: parent.bottom; right: parent.right }
        text: sidebarCamsList.width == 20 ? UIConstants.iRightHand : UIConstants.iLeftHand
        font.pixelSize: 25
        color: UIConstants.textFooterColor
        visible: liveViewPage ? true : false
        MouseArea {
            anchors.fill: parent
            enabled: true
            hoverEnabled: true
            onClicked: {
                parent.text = (parent.text === UIConstants.iLeftHand) ?
                             UIConstants.iRightHand : UIConstants.iLeftHand ;
                sidebarCamsList.requestToggleSidebar();
            }
        }
    }

    //--------------- Js supported funcs
    //--- Create sidebar model: groups of cams.
    function feedUpSidebarCams() {
        var listCams = DevicesManager.listCamsModel.listCams || {};
        var listCamsGroup = [];
        var listSidebarObj = {};
        for(var i = 0; i < Object.keys(listCams).length; i++) {
            var camIpCluster = listCams[i].ipAddress.split(".").slice(0, -1).join(".");
            if( listCams[i].ipAddress.includes(camIpCluster) ) {
                if(!listSidebarObj[camIpCluster]) {
                    listSidebarObj[camIpCluster] = [];
                }
                listSidebarObj[camIpCluster].push(listCams[i]);
            }
        }

        for( var i = 0; i < Object.keys(listSidebarObj).length; i++ )
        {
            listCamsGroup.push({ id_: i, network_: Object.keys(listSidebarObj)[i], numOfDevice_: listSidebarObj[Object.keys(listSidebarObj)[i]].length, navLevel2Content_: listSidebarObj[Object.keys(listSidebarObj)[i]], open_: true });
        }
        return listCamsGroup;
    }
}
