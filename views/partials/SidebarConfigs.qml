/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Component:
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 19/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------ Include QT libs ------------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

//---------------- Include custom libs ----------------------------------------
import "../components"
import UIConstants 1.0

//---------------- Component definition ----------------------------------------
Item {
    id: rootItem
    //-------------- signal
    signal displayActiveConfigBoard( real boardId_ )

    //-------------- Sidebar title
    SidebarTitle {
        id: sidebarTitle
        anchors { top: parent.top; left: parent.left; right: parent.right }
        height: parent.height / 14
        visible: true
        title: "Menu"
    }

    //--------------- Sidebar list / Sidebar content
    Column {
        anchors { top: sidebarTitle.bottom; left: parent.left; right: parent.right; bottom: parent.bottom }
        //--- Sidebar Elements
        Repeater {
            id: sidebarElements
            anchors.fill: parent
            model: ListModel {
                id: listElesData
                Component.onCompleted: {
                    append({id_: 1, icon_: UIConstants.iStorage, text_: "Lưu trữ" });
                    append({id_: 2, icon_: UIConstants.iDetectParams, text_: "Thông số phát hiện quả" });
                    append({id_: 3, icon_: UIConstants.iSequenceTasks, text_: "Tác vụ" });
                    append({id_: 4, icon_: UIConstants.iLiveView, text_: "Tham số truyền hình ảnh" });
                    append({id_: 5, icon_: UIConstants.iUsers, text_: "Người dùng" });
                }
            }
            SidebarConfigEle {
                width: parent.width
                height: 60
                iconSide: icon_
                textSide: text_
                eleId: id_
                onActiveSide: {
                    rootItem.displayActiveConfigBoard( eleId_ - 1);
                    for( var i = 0; i < listElesData.count; i++ )
                    {
                        if( i == eleId_ - 1 )
                        {
                            sidebarElements.itemAt(i).setActive();
                        }
                        else
                        {
                            sidebarElements.itemAt(i).setDeactive();
                        }
                    }
                }
                //--- Set default active
                Component.onCompleted: {
                    sidebarElements.itemAt(0).setActive();
                }
            }
        }
    }
}
