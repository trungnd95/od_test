/**
 * ==============================================================================
 * @Project: VCM01TargetViewer
 * @Module: Navbar Partial
 * @Breif:
 * @Author: Trung Nguyen
 * @Date: 15/02/2019
 * @Language: QML
 * @License: (c) Viettel Aerospace Institude - Viettel Group
 * ============================================================================
 */

//------------------------ Include QT libs ------------------------------------
import QtQuick.Window 2.2
import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import UIConstants 1.0

//------------------------ Include Custom Component ---------------------------
import "../components"
import "../windows"

//------------------------ Partial content ------------------------------------
Row {
    id: rowNavbar
    //---------- Signal
    signal newActiveItem( real _id )

    //--------- Nav Elements
    Repeater {
        id: repeatNavElements
        model: ListModel {
            ListElement { text_: "Trang chủ"; iconText_: "\uf015"; id_: 1 }
            ListElement { text_: "Xem trực tuyến"; iconText_: "\uf03d"; id_: 2 }
            ListElement { text_: "Xem lại"; iconText_: "\uf144"; id_: 3  }
            ListElement { text_: "Quản lý thiết bị"; iconText_: "\uf109"; id_: 4 }
            ListElement { text_: "Cấu hình chung"; iconText_: "\uf085"; id_: 5 }
        }

        NavElement {
            id: navEle
            text: text_
            iconText: iconText_
            anchors.leftMargin: 20
            itemId: id_
            onClicked: {
                rowNavbar.newActiveItem(_itemId);
                for( var i = 0; i < 5; i++ )
                {
                    if( repeatNavElements.itemAt(i).itemId != _itemId )
                    {
                        repeatNavElements.itemAt(i).active = false;
                        repeatNavElements.itemAt(i).color_ = UIConstants.grayColor;
                    } else {
                        repeatNavElements.itemAt(i).active =  true;
                    }
                }
            }
        }
    }

    //------------ Js suported functions
    function changeActiveItem( _activeId )
    {
        for( var i = 0; i < 5; i++ )
        {
            if( repeatNavElements.itemAt(i).itemId != (_activeId + 1))
            {
                repeatNavElements.itemAt(i).active = false;
                repeatNavElements.itemAt(i).color_ = UIConstants.grayColor;
            }else {
                repeatNavElements.itemAt(i).active = true;
                repeatNavElements.itemAt(i).color_ = UIConstants.borderGreen;
            }
        }
    }
}

